function [DV_debug_params] = CHK_q_SetPoint(DV_debug_params,DV_FDS_CMD_LIST)

[m,~] = size(DV_debug_params.time);
n=DV_FDS_CMD_LIST.index;
j=1;
tolerance = 5;
DV_debug_params.quatCHK.q_SetPoint_CHK = zeros(n,4);
DV_debug_params.quatCHK.q_SetPoint_err = zeros(n,4);

for i=1:m
    
    if(abs(DV_debug_params.Tick_time(i) - DV_FDS_CMD_LIST.OBC_clock{j,1}) < tolerance)
        DV_debug_params.quatCHK.q_SetPoint_CHK(j,:) = DV_debug_params.GNC.GUIDANCE_CMD.q_SetPoint(i,:);
        DV_debug_params.quatCHK.q_SetPoint_err(j,:) = quatmultiply(DV_debug_params.quatCHK.q_SetPoint_CHK(j,:),quatconj(DV_FDS_CMD_LIST.Quat_list{j,:}));
        if j == n
            break
        end
        i=i+100;
        j = j+1;
    end
end


figure('name','q SetPoint CHK')
subplot 221
plot(DV_debug_params.Tick_time,DV_debug_params.GNC.GUIDANCE_CMD.q_SetPoint(:,1))
hold on
plot(DV_FDS_CMD_LIST.OBC_clock{:,1},DV_FDS_CMD_LIST.Quat_list{:,4},'.','MarkerSize',15)
grid on
xlabel('obc [ticks]')
ylim([-1,1])
legend('q SetPoint','q from chart')
title('q_1')
subplot 222
plot(DV_debug_params.Tick_time,DV_debug_params.GNC.GUIDANCE_CMD.q_SetPoint(:,2))
hold on
plot(DV_FDS_CMD_LIST.OBC_clock{:,1},DV_FDS_CMD_LIST.Quat_list{:,1},'.','MarkerSize',15)
grid on
xlabel('obc [ticks]')
ylim([-1,1])
title('q_2')
subplot 223
plot(DV_debug_params.Tick_time,DV_debug_params.GNC.GUIDANCE_CMD.q_SetPoint(:,3))
hold on
plot(DV_FDS_CMD_LIST.OBC_clock{:,1},DV_FDS_CMD_LIST.Quat_list{:,2},'.','MarkerSize',15)
grid on
xlabel('obc [ticks]')
ylim([-1,1])
title('q_3')
subplot 224
plot(DV_debug_params.Tick_time,DV_debug_params.GNC.GUIDANCE_CMD.q_SetPoint(:,4))
hold on
plot(DV_FDS_CMD_LIST.OBC_clock{:,1},DV_FDS_CMD_LIST.Quat_list{:,3},'.','MarkerSize',15)
grid on
xlabel('obc [ticks]')
ylim([-1,1])
title('q_4')