function [DV_case] = choose_DV()

prompt = 'choose maneuver:\n 1 = AM1\n 2 = AM2\n 3 = PM1\n 4 = PM2\n 5 = PM3\n 6 = LOI1\n 7 = LOI2\n 8 = DM1\n 9 = Control Room\n';
User_input = input(prompt);

switch User_input
    case 1
        DV_case = 'AM1';
    case 2
        DV_case = 'AM2';
    case 3
        DV_case = 'PM1';
    case 4
        DV_case = 'PM2';
    case 5
        DV_case = 'PM3';
    case 6
        DV_case = 'LOI1';
    case 7
        DV_case = 'LOI2';
    case 8
        DV_case = 'DM1';
    case 9
        DV_case = 'Control Room';
end
