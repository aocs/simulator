function DV_err = get_DV_error(Kp_before,Kp_after,MNVR_DATA)


hp_before_err = abs(MNVR_DATA.BEFORE.Kp.hp - Kp_before.hp);
hp_after_err = abs(MNVR_DATA.AFTER.Kp.hp - Kp_after.hp);
ra_before_err = abs(MNVR_DATA.BEFORE.Kp.ra - Kp_before.ra);
ra_after_err = abs(MNVR_DATA.AFTER.Kp.ra - Kp_after.ra);

DV_err.before.hp_err_before = hp_before_err;
DV_err.after.hp_err_after = hp_after_err;
DV_err.before.ra_err_before = ra_before_err;
DV_err.after.ra_err_after = ra_after_err;
