function DV_FDS_CMD_LIST = get_FDS_data(DV_case)

switch DV_case
    case 'AM1'
                addpath('C:\Users\orell\Desktop\v1.2\Environment\Trajectory\Burn0_AM1')
        filename = 'CMD_20181216_0412.txt';
    case 'AM2'
                addpath('C:\Users\orell\Desktop\v1.2\Environment\Trajectory\Burn1_AM2')
        filename = 'CMD_20181216_2329.txt';
    case 'PM1'
                addpath('C:\Users\orell\Desktop\v1.2\Environment\Trajectory\Burn2_PM1')
        filename = 'CMD_20181218_2359.txt';
    case 'PM2'
                addpath('C:\Users\orell\Desktop\v1.2\Environment\Trajectory\Burn3_PM2')
        filename = 'CMD_20181230_1821.txt';
    case 'PM3'
                addpath('C:\Users\orell\Desktop\v1.2\Environment\Trajectory\Burn4_PM3')
        filename = 'CMD_20190111_0358.txt';
    case 'LOI1'
                addpath('C:\Users\orell\Desktop\v1.2\Environment\Trajectory\Burn7_LOI1')
        filename = 'CMD_20190125_1046.txt';
    case 'LOI2'
                addpath('C:\Users\orell\Desktop\v1.2\Environment\Trajectory\Burn8_LOI2')
        filename = 'CMD_20190127_1936.txt';
    case 'DM1'
                addpath('C:\Users\orell\Desktop\v1.2\Environment\Trajectory\Burn9_DM1')
        filename = 'CMD_20190211_0022.txt';
    case 'Control Room'
        addpath('C:\Users\orell\Desktop\v1.2\Environment\Trajectory\control room CMD file')
        filename = 'CMD_20181221_2311.txt';
end

CMD_list = get_CMD_list(filename);
index = table2array(CMD_list(1,1));
DV_FDS_CMD_LIST.index = index;
DV_FDS_CMD_LIST.OBC_clock = CMD_list(6:5+index,1);
DV_FDS_CMD_LIST.Duration = CMD_list(6:5+index,2);
DV_FDS_CMD_LIST.Quat_list = CMD_list(6:5+index,3:6);
DV_FDS_CMD_LIST.Coarse_Fine_list = CMD_list(6:5+index,7);


