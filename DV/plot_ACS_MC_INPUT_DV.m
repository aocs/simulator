function plot_ACS_MC_INPUT_DV(mission_simulator_results)

figure('name','MC INPUT - ACS')
subplot 311
plot(mission_simulator_results.Time,mission_simulator_results.MC_INPUT.MC_ACS_LOGIC_CMD.ACS_PERMIT,'LineWidth',1.5)
grid on
xlabel('Time [sec]')
ylim([0,4])
title('ACS Permit')
subplot 312
plot(mission_simulator_results.Time,mission_simulator_results.MC_INPUT.MC_ACS_LOGIC_CMD.MHT_AVILABLE,'LineWidth',1.5)
grid on
xlabel('Time [sec]')
ylim([0,4])
title('MHT Available')
subplot 313
plot(mission_simulator_results.Time,mission_simulator_results.MC_INPUT.MC_ACS_LOGIC_CMD.PROCEED_WITH_HT_ONLY,'LineWidth',1.5)
grid on
xlabel('Time [sec]')
ylim([0,4])
title('Proceed with HT only')