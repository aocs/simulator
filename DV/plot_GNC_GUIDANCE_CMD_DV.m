function [] = plot_GNC_GUIDANCE_CMD_DV(DV_debug_params)


figure('name','GNC Guidance CMD')
subplot 311
plot(DV_debug_params.time,DV_debug_params.GNC.GUIDANCE_CMD.apply_DV_enable,'LineWidth',2)
grid on
xlabel('time [sec]')
title ('apply DV enable')
hold on
subplot 312
plot(DV_debug_params.time,DV_debug_params.GNC.GUIDANCE_CMD.ang_vel_band,'LineWidth',2)
grid on
xlabel('time [sec]')
title ('ang vel band')
hold on
subplot 313
plot(DV_debug_params.time,DV_debug_params.GNC.GUIDANCE_CMD.attitude_band,'LineWidth',2)
grid on
xlabel('time [sec]')
title ('attitude band')