function [] = plot_engines_CMD_DV(mission_simulator_results)

[m,~] = size(mission_simulator_results.GNC.ENG_CMD.MHT.CMD_to_MC1{:,1});
plot_time = linspace(mission_simulator_results.Time(1),mission_simulator_results.Time(end),m);

%%%% plot MHT off command
figure('name','MHT')
stem(plot_time, mission_simulator_results.GNC.ENG_CMD.MHT.CMD_to_MC1{:,1},'.')
grid on
xlabel('time [sec]')
ylabel('engine activation')
title('MHT command to MC 2')

%%%% plot first set of ACS off command
figure('name','ACS 1')
subplot 221
stem(plot_time, mission_simulator_results.GNC.ENG_CMD.ACS.ACS(1).CMD_to_MC1{:,1},'.')
grid on
xlabel('time [sec]')
ylabel('engine activation')
title('ACS 1 command to MC 2')
subplot 222
stem(plot_time, mission_simulator_results.GNC.ENG_CMD.ACS.ACS(2).CMD_to_MC1{:,1},'.')
grid on
xlabel('time [sec]')
ylabel('engine activation')
title('ACS 2 command to MC 2')
subplot 223
stem(plot_time, mission_simulator_results.GNC.ENG_CMD.ACS.ACS(3).CMD_to_MC1{:,1},'.')
grid on
xlabel('time [sec]')
ylabel('engine activation')
title('ACS 3 command to MC 2')
subplot 224
stem(plot_time, mission_simulator_results.GNC.ENG_CMD.ACS.ACS(4).CMD_to_MC1{:,1},'.')
grid on
xlabel('time [sec]')
ylabel('engine activation')
title('ACS 4 command to MC 2')

%%%% plot second set of ACS off command
figure('name','ACS 2')
subplot 221
stem(plot_time, mission_simulator_results.GNC.ENG_CMD.ACS.ACS(5).CMD_to_MC1{:,1},'.')
grid on
xlabel('time [sec]')
ylabel('engine activation')
title('ACS 5 command to MC 2')
subplot 222
stem(plot_time, mission_simulator_results.GNC.ENG_CMD.ACS.ACS(6).CMD_to_MC1{:,1},'.')
grid on
xlabel('time [sec]')
ylabel('engine activation')
title('ACS 6 command to MC 2')
subplot 223
stem(plot_time, mission_simulator_results.GNC.ENG_CMD.ACS.ACS(7).CMD_to_MC1{:,1},'.')
grid on
xlabel('time [sec]')
ylabel('engine activation')
title('ACS 7 command to MC 2')
subplot 224
stem(plot_time, mission_simulator_results.GNC.ENG_CMD.ACS.ACS(8).CMD_to_MC1{:,1},'.')
grid on
xlabel('time [sec]')
ylabel('engine activation')
title('ACS 8 command to MC 2')
