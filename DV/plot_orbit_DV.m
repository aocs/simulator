function [] = plot_orbit(aocs_spacecraft_data,MNVR_DATA,DV_case)

%% kepler calculation - can only be done after running project with the same maneuver

if DV_case =='LOI1'
    
    frame = 'Moon';
    date_from_simulator = datetime([aocs_spacecraft_data.Year,aocs_spacecraft_data.Month,aocs_spacecraft_data.Day...
        ,aocs_spacecraft_data.Hour,aocs_spacecraft_data.Minute,aocs_spacecraft_data.Second]);
    JD_LOI1 = juliandate(date_from_simulator);
    
    [position_Moon,velocity_Moon] = planetEphemeris(JD_LOI1,'Earth','Moon','421');
    
    X_rel = [aocs_spacecraft_data.X,aocs_spacecraft_data.Y,aocs_spacecraft_data.Z] - position_Moon*1000;
    V_rel = [aocs_spacecraft_data.Vx,aocs_spacecraft_data.Vy,aocs_spacecraft_data.Vz] - velocity_Moon*1000;
    
    X_input_before = [X_rel(1,1),X_rel(1,2),X_rel(1,3)];
    V_input_before = [V_rel(1,1),V_rel(1,2),V_rel(1,3)];
    
    X_input_after = [X_rel(end-1,1),X_rel(end-1,2),X_rel(end-1,3)];
    V_input_after = [V_rel(end-1,1),V_rel(end-1,2),V_rel(end-1,3)];
    
    [X_before,Y_before,Z_before,Kp_before] = plot_trajectory(X_input_before,V_input_before,[-90 90],frame);
    [X_after,Y_after,Z_after,Kp_after] = plot_trajectory(X_input_after,V_input_after,[0 360],frame);
    C.RGB_grey = [128,128,128]/255;
    
else

frame = 'earth';

X_input_before = [aocs_spacecraft_data.X(1),aocs_spacecraft_data.Y(1),aocs_spacecraft_data.Z(1)];
V_input_before = [aocs_spacecraft_data.Vx(1),aocs_spacecraft_data.Vy(1),aocs_spacecraft_data.Vz(1)];

X_input_after = [aocs_spacecraft_data.X(end-1),aocs_spacecraft_data.Y(end-1),aocs_spacecraft_data.Z(end-1)];
V_input_after = [aocs_spacecraft_data.Vx(end-1),aocs_spacecraft_data.Vy(end-1),aocs_spacecraft_data.Vz(end-1)];

[X_before,Y_before,Z_before,Kp_before] = plot_trajectory(X_input_before,V_input_before,[0 360],frame);
[X_after,Y_after,Z_after,Kp_after] = plot_trajectory(X_input_after,V_input_after,[0 360],frame);

end

%% plot kepler trajectory

[Xs,Ys,Zs] = sphere;
figure('name','SC trajectory')
switch frame
    case 'earth'
        R = 6371000;
        surf(Xs*R/1000,Ys*R/1000,Zs*R/1000,'FaceColor','c','FaceLighting','gouraud','DisplayName','Earth')
        
    case 'Moon'
        R = 1738000;
        surf(Xs*R/1000,Ys*R/1000,Zs*R/1000,'FaceColor',C.RGB_grey,'FaceLighting','gouraud','DisplayName','Moon')
end
hold on
axis equal
plot3(X_before/1000,Y_before/1000,Z_before/1000,'Color','b','LineWidth',1.5)

plot3(X_after/1000,Y_after/1000,Z_after/1000,'m','LineWidth',1.5)
legend('Earth','Before DV','After DV')
% legend('Moon','Before DV','After DV')

%% Kp error vec
DV_err = get_DV_error(Kp_before,Kp_after,MNVR_DATA);

disp(DV_err.before);
disp(DV_err.after);
disp('KP from mission simulator:')
hp_before_disp = sprintf('hp before = %s \n',num2str(Kp_before.hp));
fprintf(hp_before_disp);
hp_after_disp = sprintf('hp after = %s \n',num2str(Kp_after.hp));
fprintf(hp_after_disp);
ra_before_disp = sprintf('ra before = %s \n',num2str(Kp_before.ra));
fprintf(ra_before_disp);
ra_after_disp = sprintf('ra after = %s \n',num2str(Kp_after.ra));
fprintf(ra_after_disp);
