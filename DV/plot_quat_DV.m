function [] = plot_quat_DV(mission_simulator_results,DV_debug_params)

[m,~] = size(mission_simulator_results.Inertial_attitude.q);
plot_time = linspace(mission_simulator_results.Time(1),mission_simulator_results.Time(end),m);

figure('name','quat I2b est-real comparison')
subplot 221
plot(DV_debug_params.time,DV_debug_params.NAV.inertial_attitude.q_I2B_est(:,1),'r','LineWidth',2)
hold on
plot(plot_time,mission_simulator_results.Inertial_attitude.q(:,1),'b','LineWidth',2)
grid on
title('q_1')
xlabel('time [sec]')
ylim([-1,1])
legend('q I2B est','Quat from simulator')
subplot 222
plot(DV_debug_params.time,DV_debug_params.NAV.inertial_attitude.q_I2B_est(:,2),'r','LineWidth',2)
hold on
plot(plot_time,mission_simulator_results.Inertial_attitude.q(:,2),'b','LineWidth',2)
grid on
title('q_2')
xlabel('time [sec]')
ylim([-1,1])
subplot 223
plot(DV_debug_params.time,DV_debug_params.NAV.inertial_attitude.q_I2B_est(:,3),'r','LineWidth',2)
hold on
plot(plot_time,mission_simulator_results.Inertial_attitude.q(:,3),'b','LineWidth',2)
grid on
title('q_3')
xlabel('time [sec]')
ylim([-1,1])
subplot 224
plot(DV_debug_params.time,DV_debug_params.NAV.inertial_attitude.q_I2B_est(:,4),'r','LineWidth',2)
hold on
plot(plot_time,mission_simulator_results.Inertial_attitude.q(:,4),'b','LineWidth',2)
grid on
title('q_4')
xlabel('time [sec]')
ylim([-1,1])

figure('name','quat SetPoint-req comparison')
subplot 221
plot(DV_debug_params.time,DV_debug_params.GNC.GUIDANCE_CMD.q_SetPoint(:,1),'r','LineWidth',2)
hold on
plot(DV_debug_params.time,DV_debug_params.GNC.GUIDANCE_CMD.q_I2B_req(:,1),'b','LineWidth',2)
grid on
title('q_1')
xlabel('time [sec]')
ylim([-1,1])
legend('q Set Point','q I2B req')
subplot 222
plot(DV_debug_params.time,DV_debug_params.GNC.GUIDANCE_CMD.q_SetPoint(:,2),'r','LineWidth',2)
hold on
plot(DV_debug_params.time,DV_debug_params.GNC.GUIDANCE_CMD.q_I2B_req(:,2),'b','LineWidth',2)
grid on
title('q_2')
xlabel('time [sec]')
ylim([-1,1])
subplot 223
plot(DV_debug_params.time,DV_debug_params.GNC.GUIDANCE_CMD.q_SetPoint(:,3),'r','LineWidth',2)
hold on
plot(DV_debug_params.time,DV_debug_params.GNC.GUIDANCE_CMD.q_I2B_req(:,3),'b','LineWidth',2)
grid on
title('q_3')
xlabel('time [sec]')
ylim([-1,1])
subplot 224
plot(DV_debug_params.time,DV_debug_params.GNC.GUIDANCE_CMD.q_SetPoint(:,4),'r','LineWidth',2)
hold on
plot(DV_debug_params.time,DV_debug_params.GNC.GUIDANCE_CMD.q_I2B_req(:,4),'b','LineWidth',2)
grid on
title('q_4')
xlabel('time [sec]')
ylim([-1,1])

figure('name','quat req-est')
subplot 221
plot(DV_debug_params.time,DV_debug_params.NAV.inertial_attitude.q_I2B_est(:,1),'r','LineWidth',2)
hold on
plot(DV_debug_params.time,DV_debug_params.GNC.GUIDANCE_CMD.q_I2B_req(:,1),'b','LineWidth',2)
hold on
plot(DV_debug_params.time,DV_debug_params.GNC.GUIDANCE_CMD.q_SetPoint(:,1),'g','LineWidth',2)
grid on
title('q_1')
xlabel('time [sec]')
ylim([-1,1])
legend('q I2B est','q req','q SetPoint')
subplot 222
plot(DV_debug_params.time,DV_debug_params.NAV.inertial_attitude.q_I2B_est(:,2),'r','LineWidth',2)
hold on
plot(DV_debug_params.time,DV_debug_params.GNC.GUIDANCE_CMD.q_I2B_req(:,2),'b','LineWidth',2)
hold on
plot(DV_debug_params.time,DV_debug_params.GNC.GUIDANCE_CMD.q_SetPoint(:,2),'g','LineWidth',2)
grid on
title('q_2')
xlabel('time [sec]')
ylim([-1,1])
subplot 223
plot(DV_debug_params.time,DV_debug_params.NAV.inertial_attitude.q_I2B_est(:,3),'r','LineWidth',2)
hold on
plot(DV_debug_params.time,DV_debug_params.GNC.GUIDANCE_CMD.q_I2B_req(:,3),'b','LineWidth',2)
hold on
plot(DV_debug_params.time,DV_debug_params.GNC.GUIDANCE_CMD.q_SetPoint(:,3),'g','LineWidth',2)
grid on
title('q_3')
xlabel('time [sec]')
ylim([-1,1])
subplot 224
plot(DV_debug_params.time,DV_debug_params.NAV.inertial_attitude.q_I2B_est(:,4),'r','LineWidth',2)
hold on
plot(DV_debug_params.time,DV_debug_params.GNC.GUIDANCE_CMD.q_I2B_req(:,4),'b','LineWidth',2)
hold on
plot(DV_debug_params.time,DV_debug_params.GNC.GUIDANCE_CMD.q_SetPoint(:,4),'g','LineWidth',2)
grid on
title('q_4')
xlabel('time [sec]')
ylim([-1,1])

