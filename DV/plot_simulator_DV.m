function plot_simulator_DV(DV_debug_params)

DV_debug_fields = get_all_struct_fields([],'DV_debug_params',DV_debug_params);
t = eval(DV_debug_fields{1});
plots_per_figure = [7,1,2,8,8,7,8,1,4,2,2,2,5];
f = 3;
time_ind = 1;


for fig = 1:numel(plots_per_figure)
    figure(fig)
    if plots_per_figure(fig)<=4
        n1 = plots_per_figure(fig);n2 =1;
    else
        n1 = 4; n2 =2;
    end    
    for p = 1:plots_per_figure(fig)
        subplot(n1,n2,p)
        plot(t,eval(DV_debug_fields{f}),'LineWidth',2)
        xlabel (DV_debug_fields{f}(17:end),'Interpreter','none')
        f = f+1;
    end
    
end
