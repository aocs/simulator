function [] = plot_spacecraft_orientation_DV(mission_simulator_results)

[m,~] = size(mission_simulator_results.Inertial_attitude.location.X);
plot_time = linspace(mission_simulator_results.Time(1),mission_simulator_results.Time(end),m);

% plot_time = linspace(mission_simulator_results.Time{1,1},mission_simulator_results.Time{end,1},m);

figure('name','Position')
subplot 311
plot(plot_time,mission_simulator_results.Inertial_attitude.location.X{:,1})
grid on
xlabel('time [sec]')
ylabel('[m]')
title('X position')
subplot 312
plot(plot_time,mission_simulator_results.Inertial_attitude.location.Y{:,1})
grid on
xlabel('time [sec]')
ylabel('[m]')
title('Y position')
subplot 313
plot(plot_time,mission_simulator_results.Inertial_attitude.location.Z{:,1})
grid on
xlabel('time [sec]')
ylabel('[m]')
title('Z position')

figure('name','Velocity')
subplot 311
plot(plot_time,mission_simulator_results.Inertial_attitude.velocity.Vx{:,1})
grid on
xlabel('time [sec]')
ylabel('[m/sec]')
title('Vx')
subplot 312
plot(plot_time,mission_simulator_results.Inertial_attitude.velocity.Vy{:,1})
grid on
xlabel('time [sec]')
ylabel('[m/sec]')
title('Vy')
subplot 313
plot(plot_time,mission_simulator_results.Inertial_attitude.velocity.Vz{:,1})
grid on
xlabel('time [sec]')
ylabel('[m/sec]')
title('Vz')

figure('name','Angular Velocity')
subplot 311
plot(plot_time,rad2deg(mission_simulator_results.Inertial_attitude.omega.omega_x{:,1}))
grid on
xlabel('time [sec]')
ylabel('[deg/sec]')
title('Wx')
subplot 312
plot(plot_time,rad2deg(mission_simulator_results.Inertial_attitude.omega.omega_y{:,1}))
grid on
xlabel('time [sec]')
ylabel('[deg/sec]')
title('Wy')
subplot 313
plot(plot_time,rad2deg(mission_simulator_results.Inertial_attitude.omega.omega_z{:,1}))
grid on
xlabel('time [sec]')
ylabel('[deg/sec]')
title('Wz')

[m,~] = size(mission_simulator_results.AOCS_Inputs.MC_Inputs.daylight);
plot_time = linspace(mission_simulator_results.Time(1),mission_simulator_results.Time(end),m);

figure('name','Daylight')
plot(plot_time,mission_simulator_results.AOCS_Inputs.MC_Inputs.daylight(:,1),'LineWidth',2)
grid on
xlabel('time [sec]')
ylim([-0.5,1.5])
title('Daylight')