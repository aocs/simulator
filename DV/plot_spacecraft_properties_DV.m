function [] = plot_spacecraft_properties_DV(mission_simulator_results)

[m,~] = size(mission_simulator_results.Inertial_attitude.mass);
plot_time = linspace(mission_simulator_results.Time(1),mission_simulator_results.Time(end),m);
% plot_time = linspace(mission_simulator_results.Time{1,1},mission_simulator_results.Time{end,1},m);

%%%% Spacecraft mass
figure('name','Mass')
plot(plot_time,mission_simulator_results.Inertial_attitude.mass{:,1})
grid on
title('Spacecraft mass')
xlabel('time [sec]')
ylabel('mass [Kg]')

%%%% Sun direction
figure('name','Sun Angle')
sun_col = mission_simulator_results.Inertial_attitude.sun_col{:,1};
plot(plot_time, sun_col)
grid on
xlabel('time [sec]')
ylabel('angle [deg]')
title('sun z angle colatitude')
