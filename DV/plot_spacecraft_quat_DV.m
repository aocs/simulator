function [] = plot_spacecraft_quat_DV(mission_simulator_results)

[m,~] = size(mission_simulator_results.Inertial_attitude.q);
plot_time = linspace(mission_simulator_results.Time(1),mission_simulator_results.Time(end),m);

figure('name','q I2B')
subplot 221
grid on
plot(plot_time,mission_simulator_results.Inertial_attitude.q(:,1))
grid on
title('q I2B - q_0')
xlabel('time [sec]')
ylim([-1,1])
subplot 222
plot(plot_time,mission_simulator_results.Inertial_attitude.q(:,2))
grid on
title('q I2B - q_1')
xlabel('time [sec]')
ylim([-1,1])
subplot 223
plot(plot_time,mission_simulator_results.Inertial_attitude.q(:,3))
grid on
title('q I2B - q_2')
xlabel('time [sec]')
ylim([-1,1])
subplot 224
plot(plot_time,mission_simulator_results.Inertial_attitude.q(:,4))
grid on
title('q I2B - q_3')
xlabel('time [sec]')
ylim([-1,1])
