function [X,Y,Z,Kp] = plot_trajectory(X_input,V_input,ni_interval,frame)

addpath('C:\Users\orell\Desktop\Space101\Environment\Utilities');

switch frame
    case 'earth'
        mu = 3.9860e+14;
        R = 6371000;
    case 'Moon'
        mu = 4.9030e+12;
        R = 1738000;
end

ORBIT_PARAMS.r_orbit = X_input;
ORBIT_PARAMS.v_orbit = V_input;
Kp = get_kepler_params(ORBIT_PARAMS,mu,R);

a_m = Kp.a;
e = Kp.e;
i_rad = Kp.i;
OMEGA_rad = Kp.OMEGA;
omega_rad = Kp.omega;
Np = 1000;

[X,Y,Z]=Generate_Orbit_Cart(a_m,e,i_rad,OMEGA_rad,omega_rad,mu,ni_interval,Np);
