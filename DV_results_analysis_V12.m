% clc;close all;
%%%%%%%%%----- run project first -----%%%%%%%%%

addpath(genpath('C:\Users\orell\Desktop\v1.2\AOCS\CodeGeneration\Simulator_log_files\DV')) %data's directory
rmpath('C:\Users\orell\Desktop\v1.2\AOCS\CodeGeneration\Simulator_log_files\cruise')
rmpath('C:\Users\orell\Desktop\v1.2\AOCS\CodeGeneration\Simulator_log_files\import data from simulator')
rmpath('C:\Users\orell\Desktop\v1.2\AOCS\CodeGeneration\Simulator_log_files\Landing')

[DV_case] = choose_DV(); %choose DV for results analysis        

%% extract the data from the files
aocs_spacecraft_data = readtable('aocs_spacecraft_data.csv'); %reads data from aocs_spacecraft_data
aocs_outputs = readtable('aocs_outputs.csv'); %reads data from aocs_outputs
aocs_inputs = readtable('aocs_inputs.csv'); %reads data from aocs_inputs
aocs_debug = readtable('aocs_debug.csv'); %reads data from aocs_debug

%% create the data structs
DV_FDS_CMD_LIST = get_FDS_data(DV_case); %get FDS data
mission_simulator_results = get_simulator_data(aocs_spacecraft_data,aocs_outputs,aocs_inputs,aocs_debug);
DV_debug_params = get_DV_debug_params(aocs_debug); %creates a struct of DV debug params

%% check q SetPoint
[DV_debug_params] = CHK_q_SetPoint(DV_debug_params,DV_FDS_CMD_LIST); % compare q SetPoint with the quat list chart

%% plots
% plot_engines_CMD_DV(mission_simulator_results) %engines CMD
% plot_GNC_CONTROL_CMD_DV(DV_debug_params) %GNC Control CMD
% plot_GNC_CONTROL_status_DV(DV_debug_params) %GNC Control Status
% plot_GNC_GUIDANCE_CMD_DV(DV_debug_params) %GNC Guidance CMD
% plot_GNC_GUIDANCE_status_DV(DV_debug_params) %GNC Guidance Status
% plot_LOGIC_events_DV(mission_simulator_results) %Logic events
% plot_LOGIC_modes_DV(mission_simulator_results) %Logic Modes
% plot_NAV_DV(DV_debug_params) %NAV
% plot_NAV_InertialAttitude_DV(DV_debug_params) %NAV Inertial Attitude
% plot_quat_DV(mission_simulator_results,DV_debug_params) %quaternion
% plot_spacecraft_orientation_DV(mission_simulator_results) %SC Orientation
% plot_spacecraft_properties_DV(mission_simulator_results) %SC Mass & Sun angle
% plot_spacecraft_quat_DV(mission_simulator_results) %SC quaternion
% plot_ACS_MC_INPUT_DV(mission_simulator_results)
% plot_TLM_events(mission_simulator_results)

%% sensors plots
% plot_CSS_DV(mission_simulator_results) %CSS 
% plot_IMU1_DV(mission_simulator_results) %IMU 1
% plot_IMU2_DV(mission_simulator_results) %IMU 2
% plot_STR_DV(mission_simulator_results) %STRs

%% kepler calculation - can only be done after running project with the same maneuver

X_input_before = [aocs_spacecraft_data.X(1),aocs_spacecraft_data.Y(1),aocs_spacecraft_data.Z(1)];
V_input_before = [aocs_spacecraft_data.Vx(1),aocs_spacecraft_data.Vy(1),aocs_spacecraft_data.Vz(1)];

X_input_after = [aocs_spacecraft_data.X(end-1),aocs_spacecraft_data.Y(end-1),aocs_spacecraft_data.Z(end-1)];
V_input_after = [aocs_spacecraft_data.Vx(end-1),aocs_spacecraft_data.Vy(end-1),aocs_spacecraft_data.Vz(end-1)];

if  strcmp(DV_case,'LOI1')
    frame = 'Moon';
    C.RGB_grey = [128,128,128]/255;
    [X_before,Y_before,Z_before,Kp_before] = plot_trajectory(X_input_before,V_input_before,[-90 90],frame);
    [X_after,Y_after,Z_after,Kp_after] = plot_trajectory(X_input_after,V_input_after,[0 360],frame);
elseif strcmp(DV_case,'LOI2') || strcmp(DV_case,'DM1')
    C.RGB_grey = [128,128,128]/255;
    frame = 'Moon';
    [X_before,Y_before,Z_before,Kp_before] = plot_trajectory(X_input_before,V_input_before,[0 360],frame);
    [X_after,Y_after,Z_after,Kp_after] = plot_trajectory(X_input_after,V_input_after,[0 360],frame);
else
    frame = 'earth';
    [X_before,Y_before,Z_before,Kp_before] = plot_trajectory(X_input_before,V_input_before,[0 360],frame);
    [X_after,Y_after,Z_after,Kp_after] = plot_trajectory(X_input_after,V_input_after,[0 360],frame);
end
%% plot kepler trajectory

[Xs,Ys,Zs] = sphere;
figure('name','SC trajectory')
switch frame
    case 'earth'
        R = 6371000;
        surf(Xs*R/1000,Ys*R/1000,Zs*R/1000,'FaceColor','c','FaceLighting','gouraud','DisplayName','Earth')
        
    case 'Moon'
        R = 1738000;
        surf(Xs*R/1000,Ys*R/1000,Zs*R/1000,'FaceColor',C.RGB_grey,'FaceLighting','gouraud','DisplayName','Moon')
end
hold on
axis equal
plot3(X_before/1000,Y_before/1000,Z_before/1000,'Color','b','LineWidth',1.5)

plot3(X_after/1000,Y_after/1000,Z_after/1000,'m','LineWidth',1.5)
xlabel('X')
ylabel('Y')
zlabel('Z')
switch frame
    case 'earth'
legend('Earth','Before DV','After DV')
case 'Moon'
legend('Moon','Before DV','After DV')
end
%% Kp error vec
DV_err = get_DV_error(Kp_before,Kp_after,MNVR_DATA);

disp(DV_err.before);
disp(DV_err.after);
disp('KP from mission simulator:')
hp_before_disp = sprintf('hp before = %s \n',num2str(Kp_before.hp));
fprintf(hp_before_disp);
hp_after_disp = sprintf('hp after = %s \n',num2str(Kp_after.hp));
fprintf(hp_after_disp);
ra_before_disp = sprintf('ra before = %s \n',num2str(Kp_before.ra));
fprintf(ra_before_disp);
ra_after_disp = sprintf('ra after = %s \n',num2str(Kp_after.ra));
fprintf(ra_after_disp);
