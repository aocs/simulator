
%%%%% torque from simulator units - [N/m] in geometric frame
%%%%% thrust from simulator units - [N] in geometric frame

simulator_recordings = readtable('Landing comparison.csv');
tilt_deg = calc_tilt_landing(Landing_debug_params.Local_Level.q_L2B);

[m,~] = size(simulator_recordings);
SIM_times = linspace(0,Landing_debug_params.time(end),m);

figure('name','LLA Comparison')
subplot 311
plot(Landing_debug_params.time,rad2deg(Landing_debug_params.Local_Level.LLA.lat),'LineWidth',1.5)
hold on
plot(SIM_times,simulator_recordings.lat)
legend('AOCS','Simulator')
grid on
xlabel('Time [sec]')
ylabel('Lat [rad]')
title('Lat')
subplot 312
plot(Landing_debug_params.time,rad2deg(Landing_debug_params.Local_Level.LLA.lon),'LineWidth',1.5)
hold on
plot(SIM_times,simulator_recordings.lon)
grid on
xlabel('Time [sec]')
ylabel('Lon [rad]')
title('Lon')
subplot 313
plot(Landing_debug_params.time,Landing_debug_params.Local_Level.LLA.alt,'LineWidth',1.5)
hold on
plot(SIM_times,simulator_recordings.altitude)
grid on
xlabel('Time [sec]')
ylabel('alt [m]')
title('alt')


figure('name','V NED Comparison')
subplot 311
plot(Landing_debug_params.time,Landing_debug_params.Local_Level.vNED(:,1),'LineWidth',2)
hold on
plot(SIM_times,simulator_recordings.VNED_north)
legend('AOCS','Simulator')
grid on
xlabel('Time [sec]')
title('vNED - X')
subplot 312
plot(Landing_debug_params.time,Landing_debug_params.Local_Level.vNED(:,2),'LineWidth',2)
hold on
plot(SIM_times,simulator_recordings.VNED_east)
grid on
xlabel('Time [sec]')
title('vNED - Y')
subplot 313
plot(Landing_debug_params.time,Landing_debug_params.Local_Level.vNED(:,3),'LineWidth',2)
hold on
plot(SIM_times,simulator_recordings.VNED_down)
grid on
xlabel('Time [sec]')
title('vNED - Z')

% figure('name','g NED comparison')
% subplot 311
% plot(Landing_debug_params.time,Landing_debug_params.Local_Level.gNED(:,1),'LineWidth',2)
% hold on
% plot(SIM_times,simulator_recordings.gned_1)
% legend('AOCS','Simulator')
% grid on
% xlabel('Time [sec]')
% title('gNED - X')
% subplot 312
% plot(Landing_debug_params.time,Landing_debug_params.Local_Level.gNED(:,2),'LineWidth',2)
% hold on
% plot(SIM_times,simulator_recordings.gned_2)
% grid on
% xlabel('Time [sec]')
% title('gNED - Y')
% subplot 313
% plot(Landing_debug_params.time,Landing_debug_params.Local_Level.gNED(:,3),'LineWidth',2)
% hold on
% plot(SIM_times,simulator_recordings.gned_3)
% grid on
% xlabel('Time [sec]')
% title('gNED - Z')

figure('name','torque comparison')
subplot 311
plot(Landing_debug_params.time,Landing_debug_params.NAV.engines.Torque_est(:,1),'LineWidth',2)
hold on
plot(SIM_times,simulator_recordings.tot_torque_Body_frame_1_)
legend('AOCS','Simulator')
grid on
xlabel('Time [sec]')
title('torque X')
subplot 312
plot(Landing_debug_params.time,Landing_debug_params.NAV.engines.Torque_est(:,2),'LineWidth',2)
hold on
plot(SIM_times,simulator_recordings.tot_torque_Body_frame_2_)
grid on
xlabel('Time [sec]')
title('torque Y')
subplot 313
plot(Landing_debug_params.time,Landing_debug_params.NAV.engines.Torque_est(:,3),'LineWidth',2)
hold on
plot(SIM_times,simulator_recordings.tot_torque_Body_frame_3_)
grid on
xlabel('Time [sec]')
title('torque Z')

figure('name','thrust comparison')
subplot 311
plot(Landing_debug_params.time,Landing_debug_params.NAV.engines.Force_est(:,1),'LineWidth',2)
hold on
plot(SIM_times,simulator_recordings.tot_thrust_Body_frame_1_)
legend('AOCS','Simulator')
grid on
xlabel('Time [sec]')
title('thrust X')
subplot 312
plot(Landing_debug_params.time,Landing_debug_params.NAV.engines.Force_est(:,2),'LineWidth',2)
hold on
plot(SIM_times,simulator_recordings.tot_thrust_Body_frame_2_)
grid on
xlabel('Time [sec]')
title('thrust Y')
subplot 313
plot(Landing_debug_params.time,Landing_debug_params.NAV.engines.Force_est(:,3),'LineWidth',2)
hold on
plot(SIM_times,simulator_recordings.tot_thrust_Body_frame_3_)
grid on
xlabel('Time [sec]')
title('thrust Z')

figure('name','tilt comparison')
subplot 211
plot(Landing_debug_params.time,rad2deg(Landing_debug_params.GNC.GUIDANCE_status.LANDING_GUIDANCE_VARS.tilt_cmd))
hold on
plot(Landing_debug_params.time,tilt_deg)
grid on
xlabel('Time [sec]')
legend('tilt cmd from simulator','tilt cmd calc')
title('GNC Comparison')
subplot 212
plot(Landing_debug_params.time,rad2deg(Landing_debug_params.GNC.GUIDANCE_status.LANDING_GUIDANCE_VARS.tilt_cmd))
hold on
plot(SIM_times,90 - simulator_recordings.tilt_angle)
grid on
xlabel('Time [sec]')
legend('tilt cmd - AOCS','tilt angle - simulator')
title('tilt angle comparison with simulator')

figure('name','V Down comparison')
plot(Landing_debug_params.time,Landing_debug_params.GNC.GUIDANCE_status.LANDING_GUIDANCE_VARS.V_down_goal)
hold on
plot(SIM_times,simulator_recordings.rateOfDescent)
grid on
xlabel('Time [sec]')
legend('V Down goal','V NED')
