clc;close all;%clear all

%% add relevant folders to path
addpath(genpath('C:\Users\orell\Desktop\v1.1\AOCS\CodeGeneration\Simulator_log_files\Landing'))
%% extract the data from the files
aocs_spacecraft_data = readtable('aocs_spacecraft_data.csv'); %reads data from aocs_spacecraft_data
aocs_outputs = readtable('aocs_outputs.csv'); %reads data from aocs_outputs
aocs_inputs = readtable('aocs_inputs.csv'); %reads data from aocs_inputs
aocs_debug = readtable('aocs_debug.csv'); %reads data from aocs_debug

%% create the data structs
mission_simulator_results = get_simulator_data(aocs_spacecraft_data,aocs_outputs,aocs_inputs);
Landing_debug_params = get_Landing_debug_params(aocs_debug); %creates a struct of DV debug params

%% plots
plot_engines_CMD_landing(mission_simulator_results) %engines CMD 
plot_GNC_CONTROL_CMD_landing(Landing_debug_params) %GNC control CMD
plot_GNC_CONTROL_status_landing(Landing_debug_params) %GNC control status
plot_GNC_GUIDANCE_CMD_landing(Landing_debug_params) %GNC guidance CMD
plot_GNC_GUIDANCE_status_landing(Landing_debug_params) %GNC guidance status
plot_LOGIC_events_landing(mission_simulator_results) %Logic events
plot_LOGIC_modes_landing(mission_simulator_results) %Logic modes
plot_NAV_InertialAttitude_landing(Landing_debug_params) %NAV inertial attitude
plot_NAV_landing(Landing_debug_params) %NAV
plot_spacecraft_orientation_landing(mission_simulator_results) %orientation and daylight
plot_spacecraft_properties_landing(mission_simulator_results) %mass and sun angle
plot_spacecraft_quat_landing(mission_simulator_results) %quaternion

%% Sensors plots
% plot_CSS_landing(mission_simulator_results); %CSS results 
% plot_IMU1_landing(mission_simulator_results) %IMU 1
% plot_IMU2_landing(mission_simulator_results) %IMU 2
% plot_STR_landing(mission_simulator_results) %STR
