function tilt_deg = calc_tilt_landing(q_L2B)

u_up_L = [0;0;-1];

u_up_B = quatrotate(q_L2B,u_up_L');

tilt_deg = 90-acosd(u_up_B(:,3));
