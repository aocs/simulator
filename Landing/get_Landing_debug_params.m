function Landing_debug_params = get_Landing_debug_params(aocs_debug)

%% get sim's time and tick count
[m,~] = size(aocs_debug.SimTime);
Landing_debug_params.time = zeros(m,1);
%%%%% sort simulator times
for i=1:m
    Landing_debug_params.time(i) = i*0.02;
    Landing_debug_params.Tick_time(i) = aocs_debug.TickNumber(end) - 2*(m-i);
end
Landing_debug_params.Tick_time = Landing_debug_params.Tick_time';
    
%% GNC guidance CMD
Landing_debug_params.GNC.GUIDANCE_CMD.attitude_band = aocs_debug.attitude_band;
Landing_debug_params.GNC.GUIDANCE_CMD.ang_vel_band = aocs_debug.ang_vel_band;
%%%% q I2B req
Landing_debug_params.GNC.GUIDANCE_CMD.q_I2B_req(:,1) = aocs_debug.q_i2b_required_0_;
Landing_debug_params.GNC.GUIDANCE_CMD.q_I2B_req(:,2) = aocs_debug.q_i2b_required_1_;
Landing_debug_params.GNC.GUIDANCE_CMD.q_I2B_req(:,3) = aocs_debug.q_i2b_required_2_;
Landing_debug_params.GNC.GUIDANCE_CMD.q_I2B_req(:,4) = aocs_debug.q_i2b_required_3_;

%% GNC guidance status
Landing_debug_params.GNC.GUIDANCE_status.braking_phase_status = aocs_debug.BrakingPhase_status;
Landing_debug_params.GNC.GUIDANCE_status.apply_DV_status = aocs_debug.ApplyDV_status;
Landing_debug_params.GNC.GUIDANCE_status.set_Attitude_status = aocs_debug.Set_Attitude_status;
Landing_debug_params.GNC.GUIDANCE_status.LANDING_GUIDANCE_VARS.tilt_cmd = aocs_debug.tilt_cmd;
Landing_debug_params.GNC.GUIDANCE_status.LANDING_GUIDANCE_VARS.ct_cmd = aocs_debug.ct_cmd;
Landing_debug_params.GNC.GUIDANCE_status.LANDING_GUIDANCE_VARS.V_down_goal = aocs_debug.V_down_goal;
Landing_debug_params.GNC.GUIDANCE_status.MoonDescent_status = aocs_debug.MoonDescent_status;
Landing_debug_params.GNC.GUIDANCE_status.Landing_Active_Descent_status = aocs_debug.Landing_Active_Descent_status;
Landing_debug_params.GNC.GUIDANCE_status.Landing_Vertical_descent_status = aocs_debug.Landing_Vertical_descent_status;

%% GNC control status
Landing_debug_params.GNC.CONTROL_status.control_mode = aocs_debug.CONTROL_MODE;
Landing_debug_params.GNC.CONTROL_status.PWM_logic_mode = aocs_debug.PWM_LOGIC_MODE;

%% GNC control CMD
Landing_debug_params.GNC.CONTROL_CMD.rate_limit = aocs_debug.rate_limit;
Landing_debug_params.GNC.CONTROL_CMD.att_err = aocs_debug.att_err;
Landing_debug_params.GNC.CONTROL_CMD.vel_err = aocs_debug.vel_err;
Landing_debug_params.GNC.CONTROL_CMD.PID_reset = aocs_debug.PID_RESET;
Landing_debug_params.GNC.CONTROL_CMD.ACS_fine = aocs_debug.ACS_FINE;
Landing_debug_params.GNC.CONTROL_CMD.ACS_full = aocs_debug.ACS_FULL;
Landing_debug_params.GNC.CONTROL_CMD.OFFMODE = aocs_debug.OFFMOD;
Landing_debug_params.GNC.CONTROL_CMD.PD_limiter = aocs_debug.PD_LIMITER;
Landing_debug_params.GNC.CONTROL_CMD.fill_ratio = aocs_debug.fill_ratio;
%%% int vec
Landing_debug_params.GNC.CONTROL_CMD.int_vec(:,1) = aocs_debug.int_vec_0_;
Landing_debug_params.GNC.CONTROL_CMD.int_vec(:,2) = aocs_debug.int_vec_1_;
Landing_debug_params.GNC.CONTROL_CMD.int_vec(:,3) = aocs_debug.int_vec_2_;
%%% int2 vec
Landing_debug_params.GNC.CONTROL_CMD.int2_vec(:,1) = aocs_debug.int2_vec_0_;
Landing_debug_params.GNC.CONTROL_CMD.int2_vec(:,2) = aocs_debug.int2_vec_1_;
Landing_debug_params.GNC.CONTROL_CMD.int2_vec(:,3) = aocs_debug.int2_vec_2_;
%%%% fine control error band
Landing_debug_params.GNC.CONTROL_CMD.fine_control_err_band(:,1) = aocs_debug.fine_cotrol_err_band_0_;
Landing_debug_params.GNC.CONTROL_CMD.fine_control_err_band(:,2) = aocs_debug.fine_cotrol_err_band_1_;
Landing_debug_params.GNC.CONTROL_CMD.fine_control_err_band(:,3) = aocs_debug.fine_cotrol_err_band_2_;
%%%% fine control omega band
Landing_debug_params.GNC.CONTROL_CMD.fine_control_omega_band(:,1) = aocs_debug.fine_cotrol_omega_band_0_;
Landing_debug_params.GNC.CONTROL_CMD.fine_control_omega_band(:,2) = aocs_debug.fine_cotrol_omega_band_1_;
Landing_debug_params.GNC.CONTROL_CMD.fine_control_omega_band(:,3) = aocs_debug.fine_cotrol_omega_band_2_;
%%%% U com
Landing_debug_params.GNC.CONTROL_CMD.U_com(:,1) = aocs_debug.u_com_0_;
Landing_debug_params.GNC.CONTROL_CMD.U_com(:,2) = aocs_debug.u_com_1_;
Landing_debug_params.GNC.CONTROL_CMD.U_com(:,3) = aocs_debug.u_com_2_;
Landing_debug_params.GNC.CONTROL_CMD.U_com(:,4) = aocs_debug.u_com_3_;
Landing_debug_params.GNC.CONTROL_CMD.U_com(:,5) = aocs_debug.u_com_4_;
Landing_debug_params.GNC.CONTROL_CMD.U_com(:,6) = aocs_debug.u_com_5_;
Landing_debug_params.GNC.CONTROL_CMD.U_com(:,7) = aocs_debug.u_com_6_;
Landing_debug_params.GNC.CONTROL_CMD.U_com(:,8) = aocs_debug.u_com_7_;

%%%% Ubit acs req
Landing_debug_params.GNC.CONTROL_CMD.Ubit_acs_req(:,1) = aocs_debug.ubit_acs_req_0_;
Landing_debug_params.GNC.CONTROL_CMD.Ubit_acs_req(:,2) = aocs_debug.ubit_acs_req_1_;
Landing_debug_params.GNC.CONTROL_CMD.Ubit_acs_req(:,3) = aocs_debug.ubit_acs_req_2_;
Landing_debug_params.GNC.CONTROL_CMD.Ubit_acs_req(:,4) = aocs_debug.ubit_acs_req_3_;
Landing_debug_params.GNC.CONTROL_CMD.Ubit_acs_req(:,5) = aocs_debug.ubit_acs_req_4_;
Landing_debug_params.GNC.CONTROL_CMD.Ubit_acs_req(:,6) = aocs_debug.ubit_acs_req_5_;
Landing_debug_params.GNC.CONTROL_CMD.Ubit_acs_req(:,7) = aocs_debug.ubit_acs_req_6_;
Landing_debug_params.GNC.CONTROL_CMD.Ubit_acs_req(:,8) = aocs_debug.ubit_acs_req_7_;

%%%% AHT com
Landing_debug_params.GNC.CONTROL_CMD.AHT_COM(:,1) = aocs_debug.AHT_COM_0_;
Landing_debug_params.GNC.CONTROL_CMD.AHT_COM(:,2) = aocs_debug.AHT_COM_1_;
Landing_debug_params.GNC.CONTROL_CMD.AHT_COM(:,3) = aocs_debug.AHT_COM_2_;
Landing_debug_params.GNC.CONTROL_CMD.AHT_COM(:,4) = aocs_debug.AHT_COM_3_;
%%%%pd limiter gains
Landing_debug_params.GNC.CONTROL_CMD.PD_limiter_gains(:,1) = aocs_debug.PD_limiter_gains_0_;
Landing_debug_params.GNC.CONTROL_CMD.PD_limiter_gains(:,2) = aocs_debug.PD_limiter_gains_1_;
Landing_debug_params.GNC.CONTROL_CMD.PD_limiter_gains(:,3) = aocs_debug.PD_limiter_gains_2_;
Landing_debug_params.GNC.CONTROL_CMD.PD_limiter_gains(:,4) = aocs_debug.PD_limiter_gains_3_;
Landing_debug_params.GNC.CONTROL_CMD.PD_limiter_gains(:,5) = aocs_debug.PD_limiter_gains_4_;
Landing_debug_params.GNC.CONTROL_CMD.PD_limiter_gains(:,6) = aocs_debug.PD_limiter_gains_5_;
%%%% w vec
Landing_debug_params.GNC.CONTROL_CMD.w_vec(:,1) = aocs_debug.w_vec_0_;
Landing_debug_params.GNC.CONTROL_CMD.w_vec(:,2) = aocs_debug.w_vec_1_;
Landing_debug_params.GNC.CONTROL_CMD.w_vec(:,3) = aocs_debug.w_vec_2_;
%%%% req torque
Landing_debug_params.GNC.CONTROL_CMD.req_torque(:,1) = aocs_debug.req_torque_0_;
Landing_debug_params.GNC.CONTROL_CMD.req_torque(:,2) = aocs_debug.req_torque_1_;
Landing_debug_params.GNC.CONTROL_CMD.req_torque(:,3) = aocs_debug.req_torque_2_;
%%%% err vec
Landing_debug_params.GNC.CONTROL_CMD.err_vec(:,1) = aocs_debug.err_vec_0_;
Landing_debug_params.GNC.CONTROL_CMD.err_vec(:,2) = aocs_debug.err_vec_1_;
Landing_debug_params.GNC.CONTROL_CMD.err_vec(:,3) = aocs_debug.err_vec_2_;
%%%% PID gains alpha
Landing_debug_params.GNC.CONTROL_CMD.PID_gains_alpha(:,1) = aocs_debug.PID_gains_alpha_0_;
Landing_debug_params.GNC.CONTROL_CMD.PID_gains_alpha(:,2) = aocs_debug.PID_gains_alpha_1_;
Landing_debug_params.GNC.CONTROL_CMD.PID_gains_alpha(:,3) = aocs_debug.PID_gains_alpha_2_;
Landing_debug_params.GNC.CONTROL_CMD.PID_gains_alpha(:,4) = aocs_debug.PID_gains_alpha_3_;
Landing_debug_params.GNC.CONTROL_CMD.PID_gains_alpha(:,5) = aocs_debug.PID_gains_alpha_4_;
Landing_debug_params.GNC.CONTROL_CMD.PID_gains_alpha(:,6) = aocs_debug.PID_gains_alpha_5_;
Landing_debug_params.GNC.CONTROL_CMD.PID_gains_alpha(:,7) = aocs_debug.PID_gains_alpha_6_;
Landing_debug_params.GNC.CONTROL_CMD.PID_gains_alpha(:,8) = aocs_debug.PID_gains_alpha_7_;
Landing_debug_params.GNC.CONTROL_CMD.PID_gains_alpha(:,9) = aocs_debug.PID_gains_alpha_8_;
Landing_debug_params.GNC.CONTROL_CMD.PID_gains_alpha(:,10) = aocs_debug.PID_gains_alpha_9_;
Landing_debug_params.GNC.CONTROL_CMD.PID_gains_alpha(:,11) = aocs_debug.PID_gains_alpha_10_;
Landing_debug_params.GNC.CONTROL_CMD.PID_gains_alpha(:,12) = aocs_debug.PID_gains_alpha_11_;
%%%% PID gains 0
Landing_debug_params.GNC.CONTROL_CMD.PID_gains_0(:,1) = aocs_debug.PID_gains_0_0_;
Landing_debug_params.GNC.CONTROL_CMD.PID_gains_0(:,2) = aocs_debug.PID_gains_0_1_;
Landing_debug_params.GNC.CONTROL_CMD.PID_gains_0(:,3) = aocs_debug.PID_gains_0_2_;
Landing_debug_params.GNC.CONTROL_CMD.PID_gains_0(:,4) = aocs_debug.PID_gains_0_3_;
Landing_debug_params.GNC.CONTROL_CMD.PID_gains_0(:,5) = aocs_debug.PID_gains_0_4_;
Landing_debug_params.GNC.CONTROL_CMD.PID_gains_0(:,6) = aocs_debug.PID_gains_0_5_;
Landing_debug_params.GNC.CONTROL_CMD.PID_gains_0(:,7) = aocs_debug.PID_gains_0_6_;
Landing_debug_params.GNC.CONTROL_CMD.PID_gains_0(:,8) = aocs_debug.PID_gains_0_7_;
Landing_debug_params.GNC.CONTROL_CMD.PID_gains_0(:,9) = aocs_debug.PID_gains_0_8_;
Landing_debug_params.GNC.CONTROL_CMD.PID_gains_0(:,10) = aocs_debug.PID_gains_0_9_;
Landing_debug_params.GNC.CONTROL_CMD.PID_gains_0(:,11) = aocs_debug.PID_gains_0_10_;
Landing_debug_params.GNC.CONTROL_CMD.PID_gains_0(:,12) = aocs_debug.PID_gains_0_11_;
%%%% PID gains 
Landing_debug_params.GNC.CONTROL_CMD.PID_gains(:,1) = aocs_debug.PID_gains_0_;
Landing_debug_params.GNC.CONTROL_CMD.PID_gains(:,2) = aocs_debug.PID_gains_1_;
Landing_debug_params.GNC.CONTROL_CMD.PID_gains(:,3) = aocs_debug.PID_gains_2_;
Landing_debug_params.GNC.CONTROL_CMD.PID_gains(:,4) = aocs_debug.PID_gains_3_;
Landing_debug_params.GNC.CONTROL_CMD.PID_gains(:,5) = aocs_debug.PID_gains_4_;
Landing_debug_params.GNC.CONTROL_CMD.PID_gains(:,6) = aocs_debug.PID_gains_5_;
Landing_debug_params.GNC.CONTROL_CMD.PID_gains(:,7) = aocs_debug.PID_gains_6_;
Landing_debug_params.GNC.CONTROL_CMD.PID_gains(:,8) = aocs_debug.PID_gains_7_;
Landing_debug_params.GNC.CONTROL_CMD.PID_gains(:,9) = aocs_debug.PID_gains_8_;
Landing_debug_params.GNC.CONTROL_CMD.PID_gains(:,10) = aocs_debug.PID_gains_9_;
Landing_debug_params.GNC.CONTROL_CMD.PID_gains(:,11) = aocs_debug.PID_gains_10_;
Landing_debug_params.GNC.CONTROL_CMD.PID_gains(:,12) = aocs_debug.PID_gains_11_;

%% NAV inertial attitude
Landing_debug_params.NAV.inertial_attitude.confidence_level_q = aocs_debug.Confidece_Level_q;
Landing_debug_params.NAV.inertial_attitude.confidence_level_w = aocs_debug.Confidece_Level_w;
Landing_debug_params.NAV.inertial_attitude.estimator_choice = aocs_debug.Estimator_Choice;
Landing_debug_params.NAV.inertial_attitude.STR1_FRAME_ALG = aocs_debug.STR1_FRAME_ALG;
Landing_debug_params.NAV.inertial_attitude.STR2_FRAME_ALG = aocs_debug.STR2_FRAME_ALG;
Landing_debug_params.NAV.inertial_attitude.inertial_nav_status_telemetry = aocs_debug.INNERTIAL_NAV_STATUS_TELEMETRY;
%%%% q I2B est
Landing_debug_params.NAV.inertial_attitude.q_I2B_est(:,1) = aocs_debug.q_i2b_est_0_;
Landing_debug_params.NAV.inertial_attitude.q_I2B_est(:,2) = aocs_debug.q_i2b_est_1_;
Landing_debug_params.NAV.inertial_attitude.q_I2B_est(:,3) = aocs_debug.q_i2b_est_2_;
Landing_debug_params.NAV.inertial_attitude.q_I2B_est(:,4) = aocs_debug.q_i2b_est_3_;
%%%% w I2B est
Landing_debug_params.NAV.inertial_attitude.w_I2B_est(:,1) = aocs_debug.w_i2b_est_0_;
Landing_debug_params.NAV.inertial_attitude.w_I2B_est(:,2) = aocs_debug.w_i2b_est_1_;
Landing_debug_params.NAV.inertial_attitude.w_I2B_est(:,3) = aocs_debug.w_i2b_est_2_;

%% NAV DV
Landing_debug_params.NAV.DV.deltaV_tot = aocs_debug.deltaV_tot;

%% NAV spacecraft parameters
Landing_debug_params.NAV.spacecraft_parameters.mass = aocs_debug.mass;
Landing_debug_params.NAV.spacecraft_parameters.MassAlgorithm = aocs_debug.MassAlgorithm;
%%%% inertia
Landing_debug_params.NAV.spacecraft_parameters.inertia(:,1) = aocs_debug.inertia_0_;
Landing_debug_params.NAV.spacecraft_parameters.inertia(:,2) = aocs_debug.inertia_1_;
Landing_debug_params.NAV.spacecraft_parameters.inertia(:,3) = aocs_debug.inertia_2_;
Landing_debug_params.NAV.spacecraft_parameters.inertia(:,4) = aocs_debug.inertia_3_;
Landing_debug_params.NAV.spacecraft_parameters.inertia(:,5) = aocs_debug.inertia_4_;
Landing_debug_params.NAV.spacecraft_parameters.inertia(:,6) = aocs_debug.inertia_5_;
Landing_debug_params.NAV.spacecraft_parameters.inertia(:,7) = aocs_debug.inertia_6_;
Landing_debug_params.NAV.spacecraft_parameters.inertia(:,8) = aocs_debug.inertia_7_;
Landing_debug_params.NAV.spacecraft_parameters.inertia(:,9) = aocs_debug.inertia_8_;
%%%% CM
Landing_debug_params.NAV.spacecraft_parameters.CM(:,1) = aocs_debug.CM_0_;
Landing_debug_params.NAV.spacecraft_parameters.CM(:,2) = aocs_debug.CM_1_;
Landing_debug_params.NAV.spacecraft_parameters.CM(:,3) = aocs_debug.CM_2_;

%% NAV specific force
Landing_debug_params.NAV.specific_force.fb_est(:,1) = aocs_debug.fb_est_0_; 
Landing_debug_params.NAV.specific_force.fb_est(:,2) = aocs_debug.fb_est_1_; 
Landing_debug_params.NAV.specific_force.fb_est(:,3) = aocs_debug.fb_est_2_; 
Landing_debug_params.NAV.specific_force.algo_ID = aocs_debug.algo_ID; 
Landing_debug_params.NAV.fb_est_filt(:,1) = aocs_debug.fb_est_filt_0_;
Landing_debug_params.NAV.fb_est_filt(:,2) = aocs_debug.fb_est_filt_1_;
Landing_debug_params.NAV.fb_est_filt(:,3) = aocs_debug.fb_est_filt_2_;

%% NAV sun direction
Landing_debug_params.NAV.sun_direction.sun_vec_b_css(:,1) = aocs_debug.sun_vec_b_css_0_;
Landing_debug_params.NAV.sun_direction.sun_vec_b_css(:,2) = aocs_debug.sun_vec_b_css_1_;
Landing_debug_params.NAV.sun_direction.sun_vec_b_css(:,3) = aocs_debug.sun_vec_b_css_2_;
Landing_debug_params.NAV.sun_direction.cl_sunvec_css = aocs_debug.cl_sunvecb_css;

%% NAV engines
Landing_debug_params.NAV.engines.Force_est(:,1) = aocs_debug.Force_est_0_;
Landing_debug_params.NAV.engines.Force_est(:,2) = aocs_debug.Force_est_1_;
Landing_debug_params.NAV.engines.Force_est(:,3) = aocs_debug.Force_est_2_;
Landing_debug_params.NAV.engines.Torque_est(:,1) = aocs_debug.Torque_est_0_;
Landing_debug_params.NAV.engines.Torque_est(:,2) = aocs_debug.Torque_est_1_;
Landing_debug_params.NAV.engines.Torque_est(:,3) = aocs_debug.Torque_est_2_;

%% inertial pointing
Landing_debug_params.inertial_pointing.axis_pointing_enable = aocs_debug.Axis_Pointing_Enable;
Landing_debug_params.inertial_pointing.req_z_axis(:,1) = aocs_debug.req_inertial_z_axis_0_;
Landing_debug_params.inertial_pointing.req_z_axis(:,2) = aocs_debug.req_inertial_z_axis_1_;
Landing_debug_params.inertial_pointing.req_z_axis(:,3) = aocs_debug.req_inertial_z_axis_2_;
Landing_debug_params.inertial_pointing.MW_rpm_req = aocs_debug.mw_rpm_req;

%% Local Level
%%%% V NED
Landing_debug_params.Local_Level.vNED(:,1) = aocs_debug.v_NED_0_;
Landing_debug_params.Local_Level.vNED(:,2) = aocs_debug.v_NED_1_;
Landing_debug_params.Local_Level.vNED(:,3) = aocs_debug.v_NED_2_;
%%%% LLA
Landing_debug_params.Local_Level.LLA.lat = aocs_debug.LLA_0_;
Landing_debug_params.Local_Level.LLA.lon = aocs_debug.LLA_1_;
Landing_debug_params.Local_Level.LLA.alt = aocs_debug.LLA_2_;
%%%% DCM2LCLF2LL
Landing_debug_params.Local_Level.DCM_LCLF2LL(:,1) = aocs_debug.DCM_LCLF2LL_0_;
Landing_debug_params.Local_Level.DCM_LCLF2LL(:,2) = aocs_debug.DCM_LCLF2LL_1_;
Landing_debug_params.Local_Level.DCM_LCLF2LL(:,3) = aocs_debug.DCM_LCLF2LL_2_;
Landing_debug_params.Local_Level.DCM_LCLF2LL(:,4) = aocs_debug.DCM_LCLF2LL_3_;
Landing_debug_params.Local_Level.DCM_LCLF2LL(:,5) = aocs_debug.DCM_LCLF2LL_4_;
Landing_debug_params.Local_Level.DCM_LCLF2LL(:,6) = aocs_debug.DCM_LCLF2LL_5_;
Landing_debug_params.Local_Level.DCM_LCLF2LL(:,7) = aocs_debug.DCM_LCLF2LL_6_;
Landing_debug_params.Local_Level.DCM_LCLF2LL(:,8) = aocs_debug.DCM_LCLF2LL_7_;
Landing_debug_params.Local_Level.DCM_LCLF2LL(:,9) = aocs_debug.DCM_LCLF2LL_8_;
%%%% liberation angles
Landing_debug_params.Local_Level.lib_angles(:,1) = aocs_debug.RPY_lib_angles_0_;
Landing_debug_params.Local_Level.lib_angles(:,2) = aocs_debug.RPY_lib_angles_1_;
Landing_debug_params.Local_Level.lib_angles(:,3) = aocs_debug.RPY_lib_angles_2_;
%%%% LLA surface
Landing_debug_params.Local_Level.LLA_surf.lat = aocs_debug.LLA_surf_0_;
Landing_debug_params.Local_Level.LLA_surf.lon = aocs_debug.LLA_surf_1_;
Landing_debug_params.Local_Level.LLA_surf.alt = aocs_debug.LLA_surf_2_;
%%%% W L2B est
Landing_debug_params.Local_Level.w_L2B_est(:,1) = aocs_debug.w_i2b_est_0_;
Landing_debug_params.Local_Level.w_L2B_est(:,2) = aocs_debug.w_i2b_est_1_;
Landing_debug_params.Local_Level.w_L2B_est(:,3) = aocs_debug.w_i2b_est_2_;
%%%% q L2B est
Landing_debug_params.Local_Level.q_L2B(:,1) = aocs_debug.q_l2b_0_;
Landing_debug_params.Local_Level.q_L2B(:,2) = aocs_debug.q_l2b_1_;
Landing_debug_params.Local_Level.q_L2B(:,3) = aocs_debug.q_l2b_2_;
Landing_debug_params.Local_Level.q_L2B(:,4) = aocs_debug.q_l2b_3_;
%%%% g NED
Landing_debug_params.Local_Level.gNED(:,1) = aocs_debug.g_NED_0_;
Landing_debug_params.Local_Level.gNED(:,2) = aocs_debug.g_NED_1_;
Landing_debug_params.Local_Level.gNED(:,3) = aocs_debug.g_NED_2_;

