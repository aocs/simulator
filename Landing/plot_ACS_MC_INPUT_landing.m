function [] = plot_ACS_MC_INPUT_landing(mission_simulator_results)

figure('name','MC INPUT - ACS')
subplot 311
plot(mission_simulator_results.Time,mission_simulator_results.MC_INPUT.MC_ACS_LOGIC_CMD.ACS_PERMIT)
grid on
xlabel('Time [sec]')
title('ACS Permit')
subplot 312
plot(mission_simulator_results.Time,mission_simulator_results.MC_INPUT.MC_ACS_LOGIC_CMD.MHT_AVILABLE)
grid on
xlabel('Time [sec]')
title('MHT Available')
subplot 313
plot(mission_simulator_results.Time,mission_simulator_results.MC_INPUT.MC_ACS_LOGIC_CMD.PROCEED_WITH_HT_ONLY)
grid on
xlabel('Time [sec]')
title('Proceed with HT only')