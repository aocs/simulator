function [] = plot_GNC_CONTROL_CMD_landing(Landing_debug_params)

figure('name','GNC control CMD - PD/PID')
subplot 321
plot(Landing_debug_params.time,Landing_debug_params.GNC.CONTROL_CMD.PD_limiter,'LineWidth',2)
grid on
xlabel('time [sec]')
title('PD limiter')
hold on
subplot 322
plot(Landing_debug_params.time,Landing_debug_params.GNC.CONTROL_CMD.PID_reset,'LineWidth',2)
grid on
xlabel('time [sec]')
title('PID RESET')
hold on
subplot 323
plot(Landing_debug_params.time,Landing_debug_params.GNC.CONTROL_CMD.PD_limiter_gains,'LineWidth',2)
grid on
xlabel('time [sec]')
title('PD limiter gains')
hold on
subplot 324
plot(Landing_debug_params.time,Landing_debug_params.GNC.CONTROL_CMD.PID_gains,'LineWidth',2)
grid on
xlabel('time [sec]')
title('PID gains')
hold on
subplot 325
plot(Landing_debug_params.time,Landing_debug_params.GNC.CONTROL_CMD.PID_gains_0,'LineWidth',2)
grid on
xlabel('time [sec]')
title('PID gains 0')
hold on
subplot 326
plot(Landing_debug_params.time,Landing_debug_params.GNC.CONTROL_CMD.PID_gains_alpha,'LineWidth',2)
grid on
xlabel('time [sec]')
title('PID gains alpha')

figure('name','GNC control CMD - errors/torque/w vec/fill ratio')
subplot 321
plot(Landing_debug_params.time,Landing_debug_params.GNC.CONTROL_CMD.req_torque)
grid on
xlabel('time [sec]')
title('req torque')
legend('\tau_1','\tau_2','\tau_3')
hold on
subplot 322
plot(Landing_debug_params.time,Landing_debug_params.GNC.CONTROL_CMD.att_err,'LineWidth',2)
grid on
xlabel('time [sec]')
title('att err')
hold on
subplot 323
plot(Landing_debug_params.time,Landing_debug_params.GNC.CONTROL_CMD.err_vec)
grid on
xlabel('time [sec]')
title('err vec')
legend('err vec 1','err vec 2','err vec 3')
hold on
subplot 324
plot(Landing_debug_params.time,Landing_debug_params.GNC.CONTROL_CMD.vel_err,'LineWidth',2)
grid on
xlabel('time [sec]')
title('vel err')
hold on
subplot 325
plot(Landing_debug_params.time,Landing_debug_params.GNC.CONTROL_CMD.w_vec)
grid on
xlabel('time [sec]')
title('w vec')
legend('\omega_1','\omega_2','\omega_3')
hold on
subplot 326
plot(Landing_debug_params.time,Landing_debug_params.GNC.CONTROL_CMD.fill_ratio,'LineWidth',2)
grid on
xlabel('time [sec]')
title('fill ratio')

figure('name','GNC control CMD - ACS/OffMod/u com')
subplot 321
plot(Landing_debug_params.time,Landing_debug_params.GNC.CONTROL_CMD.ACS_fine,'LineWidth',2)
grid on
xlabel('time [sec]')
title('ACS FINE')
hold on
subplot 322
plot(Landing_debug_params.time,Landing_debug_params.GNC.CONTROL_CMD.ACS_full,'LineWidth',2)
grid on
xlabel('time [sec]')
title('ACS FULL')
hold on
subplot 323
plot(Landing_debug_params.time,Landing_debug_params.GNC.CONTROL_CMD.AHT_COM,'LineWidth',2)
grid on
xlabel('time [sec]')
title('AHT COM')
hold on
subplot 324
plot(Landing_debug_params.time,Landing_debug_params.GNC.CONTROL_CMD.OFFMODE,'LineWidth',2)
grid on
xlabel('time [sec]')
title('OFFMOD')
hold on
subplot 325
stem(Landing_debug_params.time,Landing_debug_params.GNC.CONTROL_CMD.U_com)
grid on
xlabel('time [sec]')
title('U com')
hold on
subplot 326
stem(Landing_debug_params.time,Landing_debug_params.GNC.CONTROL_CMD.Ubit_acs_req)
grid on
xlabel('time [sec]')
title('Ubit acs req')


figure('name','GNC control CMD - fine cotrol bands/int/fill ratio')
subplot 321
plot(Landing_debug_params.time,Landing_debug_params.GNC.CONTROL_CMD.fine_control_err_band,'LineWidth',2)
grid on
xlabel('time [sec]')
title('fine control err band')
hold on
subplot 322
plot(Landing_debug_params.time,Landing_debug_params.GNC.CONTROL_CMD.fine_control_omega_band,'LineWidth',2)
grid on
xlabel('time [sec]')
title('fine control omega band')
hold on
subplot 323
plot(Landing_debug_params.time,Landing_debug_params.GNC.CONTROL_CMD.int2_vec)
grid on
xlabel('time [sec]')
title('int2 vec')
hold on
subplot 324
plot(Landing_debug_params.time,Landing_debug_params.GNC.CONTROL_CMD.int_vec)
grid on
xlabel('time [sec]')
title('int vec')
hold on
subplot 325
plot(Landing_debug_params.time,Landing_debug_params.GNC.CONTROL_CMD.rate_limit,'LineWidth',2)
grid on
xlabel('time [sec]')
title('rate limit')
