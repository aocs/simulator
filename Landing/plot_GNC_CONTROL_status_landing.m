function [] = plot_GNC_CONTROL_status_landing(Landing_debug_params)

figure('name','GNC control status')
subplot 211
plot(Landing_debug_params.time,Landing_debug_params.GNC.CONTROL_status.control_mode,'LineWidth',2)
grid on
ylim([0,4])
yticklabels({'None','NULL','PID','PD Limiter','FINE'})
xlabel('time [sec]')
title('Control Mode')
hold on
subplot 212
plot(Landing_debug_params.time,Landing_debug_params.GNC.CONTROL_status.PWM_logic_mode,'LineWidth',2)
grid on
ylim([0,3])
yticklabels({'None','ACS 1','ACS 2','ACS FULL'})
xlabel('time [sec]')
title('PWM Logic Mode')
