function [] = plot_GNC_GUIDANCE_CMD_landing(Landing_debug_params)


figure('name','GNC Guidance CMD')
subplot 211
plot(Landing_debug_params.time,Landing_debug_params.GNC.GUIDANCE_CMD.ang_vel_band,'LineWidth',2)
grid on
xlabel('time [sec]')
title ('ang vel band')
hold on
subplot 212
plot(Landing_debug_params.time,Landing_debug_params.GNC.GUIDANCE_CMD.attitude_band,'LineWidth',2)
grid on
xlabel('time [sec]')
title ('attitude band')