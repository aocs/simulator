function [] = plot_GNC_GUIDANCE_status_landing(Landing_debug_params)


figure('name','GNC Guidance status')
subplot 211
plot(Landing_debug_params.time,Landing_debug_params.GNC.GUIDANCE_status.set_Attitude_status,'LineWidth',2)
grid on
yticks(0:2)
yticklabels({'None','Out Of Band','In Band'})
xlabel('time [sec]')
title ('set attitude band')
subplot 212
plot(Landing_debug_params.time,Landing_debug_params.GNC.GUIDANCE_status.braking_phase_status,'LineWidth',2)
grid on
yticks(0:4)
yticklabels({'None','Braking phase 0','Braking phase 1', 'Braking phase 2','Braking phase 3'})
xlabel('time [sec]')
title ('Braking phase status')

figure('name','GNC Landing Guidance Vars')
subplot 311
plot(Landing_debug_params.time,Landing_debug_params.GNC.GUIDANCE_status.LANDING_GUIDANCE_VARS.ct_cmd)
grid on
xlabel('time [sec]')
title ('ct CMD')
subplot 312
plot(Landing_debug_params.time,rad2deg(Landing_debug_params.GNC.GUIDANCE_status.LANDING_GUIDANCE_VARS.tilt_cmd))
grid on
xlabel('time [sec]')
title ('tilt CMD')
subplot 313
plot(Landing_debug_params.time,Landing_debug_params.GNC.GUIDANCE_status.LANDING_GUIDANCE_VARS.V_down_goal)
grid on
xlabel('time [sec]')
title ('V down goal')