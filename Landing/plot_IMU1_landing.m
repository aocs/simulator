function [] = plot_IMU1_landing(mission_simulator_results)

%%%% plot IMU 1 wb gyro
figure('name','IMU 1 Gyro')
subplot 311
plot(mission_simulator_results.Time, mission_simulator_results.AOCS_Inputs.IMUmeas1.wb_gyro{:,1})
grid on
title('IMU 1 - w_b gyro 1')
xlabel('time [sec]')
ylabel('w [deg]')
subplot 312
plot(mission_simulator_results.Time, mission_simulator_results.AOCS_Inputs.IMUmeas1.wb_gyro{:,2})
grid on
title('IMU 1 - w_b gyro 2')
xlabel('time [sec]')
ylabel('w [deg]')
subplot 313
plot(mission_simulator_results.Time, mission_simulator_results.AOCS_Inputs.IMUmeas1.wb_gyro{:,3})
grid on
title('IMU 1 - w_b gyro 3')
xlabel('time [sec]')
ylabel('w [deg]')

%%%% plot IMU 1 f accel
figure('name', 'IMU 1 F acc')
subplot 311
plot(mission_simulator_results.Time, mission_simulator_results.AOCS_Inputs.IMUmeas1.f_accel{:,1})
grid on
title('IMU 1 - f accel 1')
xlabel('time [sec]')
subplot 312
plot(mission_simulator_results.Time, mission_simulator_results.AOCS_Inputs.IMUmeas1.f_accel{:,2})
grid on
title('IMU 1 - f accel 2')
xlabel('time [sec]')
subplot 313
plot(mission_simulator_results.Time, mission_simulator_results.AOCS_Inputs.IMUmeas1.f_accel{:,3})
grid on
title('IMU 1 - f accel 3')
xlabel('time [sec]')

%%%% plot IMU 1 latency and status
figure('name','IMU 1 latency & status')
subplot 311
plot(mission_simulator_results.Time, mission_simulator_results.AOCS_Inputs.IMUmeas1.latency{:,1},'LineWidth',2)
grid on
title('IMU 1 - latency')
xlabel('time [sec]')
ylabel('time [sec]')
subplot 312
plot(mission_simulator_results.Time, mission_simulator_results.AOCS_Inputs.IMUmeas1.status_gyro{:,1},'LineWidth',2)
grid on
title('IMU 1 - gyro status')
xlabel('time [sec]')
subplot 313
plot(mission_simulator_results.Time, mission_simulator_results.AOCS_Inputs.IMUmeas1.status_acc{:,1},'LineWidth',2)
grid on
title('IMU 1 - acc status')
xlabel('time [sec]')

figure('name','IMU 1 Timetag')
plot(mission_simulator_results.Time(2:end),diff(mission_simulator_results.AOCS_Inputs.IMUmeas1.Timetag),'LineWidth',1.5)
grid on
xlabel('Time [sec]')
title('IMU 1 Timetag')