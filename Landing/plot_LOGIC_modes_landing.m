function [] = plot_LOGIC_modes_landing(mission_simulator_results)

[m,~] = size(mission_simulator_results.LOGIC.modes.AOCS_main_mode);
plot_time = linspace(mission_simulator_results.Time(1),mission_simulator_results.Time(end),m);

%% plot AOCS main mode
figure ('name','AOCS Main Mode')
plot(plot_time,mission_simulator_results.LOGIC.modes.AOCS_main_mode{:,1},'LineWidth',2);
grid on
ylim([0,8.5])
yticklabels({'None','Idle','STANDBY','CRUISE','DV MANUEVER','LANDING','SURFACE','HOPPING'})
xlabel('time [sec]')
title('AOCS Main Mode')

%% plot Landing mode and orientation
figure('name','Landing Mode') 
subplot 211
plot(plot_time,mission_simulator_results.LOGIC.modes.landing_mode{:,1});
grid on
ylim([0,3])
yticklabels({'None','ORIENTATION','DESCENT','Engine Cut Off'})
xlabel('time [sec]')
title('Landing Mode')
subplot 212
plot(plot_time,mission_simulator_results.LOGIC.modes.landing_orientation_mode{:,1});
grid on
ylim([0,3])
yticklabels({'None','NAV INIT','COARSE ATTITUDE','FINE ATTITUDE'})
xlabel('time [sec]')
title('Landing orientation Mode')

%% plot cruise mode and cruise calibration mode
figure('name','Cruise Mode')
subplot 211
plot(plot_time,mission_simulator_results.LOGIC.modes.cruise_mode{:,1},'LineWidth',2);
grid on
ylim([0,3.5])
yticklabels({'None','POINTING','CALIBRATE IMU','ACS TEST'})
xlabel('time [sec]')
title('Cruise Mode')
subplot 212
plot(plot_time,mission_simulator_results.LOGIC.modes.cruise_calibration_mode{:,1},'LineWidth',2);
grid on
ylim([0,3.5])
yticklabels({'None','PRE CALIBRATION','CALIBRATE IMU','POST CALIBRATION'})
xlabel('time [sec]')
title('Cruise Calibration Mode')

%% plot DV mode and orientation
% figure('name','DV Mode')
% subplot 211
% plot(plot_time,mission_simulator_results.LOGIC.modes.DV_mode{:,1},'LineWidth',2);
% grid on
% ylim([0,3.5])
% yticklabels({'None','ORIENTATION','FIRE','DV DONE'})
% xlabel('time [sec]')
% title('DV Mode')
% subplot 212
% plot(plot_time,mission_simulator_results.LOGIC.modes.DV_orientation_mode{:,1},'LineWidth',2);
% grid on
% ylim([0,3.5])
% yticklabels({'None','NAV INIT','COARSE POINT','FINE POINT'})
% xlabel('time [sec]')
% title('DV orientation Mode')

%% plot SP mode and safe pointing mode
figure('name','SP Mode')
subplot 211
plot(plot_time,mission_simulator_results.LOGIC.modes.SP_mode{:,1},'LineWidth',2);
grid on
ylim([0,3.5])
yticklabels({'None','NAV INIT','NAV FAIL','ACTIVE'})
xlabel('time [sec]')
title('Sun Pointing Mode')
subplot 212
plot(plot_time,mission_simulator_results.LOGIC.modes.safe_pointing_mode{:,1},'LineWidth',2);
grid on
ylim([0,3.5])
yticklabels({'None','NAV INIT','ACTIVE','NAV FAIL'})
xlabel('time [sec]')
title('Safe Pointing Mode')


