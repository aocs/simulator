function [] = plot_Local_level(Landing_debug_params)

figure('name','g NED')
subplot 311
plot(Landing_debug_params.time,Landing_debug_params.Local_Level.gNED(:,1),'LineWidth',2)
grid on
xlabel('Time [sec]')
title('gNED - X')
subplot 312
plot(Landing_debug_params.time,Landing_debug_params.Local_Level.gNED(:,2),'LineWidth',2)
grid on
xlabel('Time [sec]')
title('gNED - Y')
subplot 313
plot(Landing_debug_params.time,Landing_debug_params.Local_Level.gNED(:,3),'LineWidth',2)
grid on
xlabel('Time [sec]')
title('gNED - Z')

figure('name','q L2B')
subplot 221
plot(Landing_debug_params.time,Landing_debug_params.Local_Level.q_L2B(:,1),'LineWidth',2)
grid on
xlabel('Time [sec]')
title('q_1')
subplot 222
plot(Landing_debug_params.time,Landing_debug_params.Local_Level.q_L2B(:,2),'LineWidth',2)
grid on
xlabel('Time [sec]')
title('q_2')
subplot 223
plot(Landing_debug_params.time,Landing_debug_params.Local_Level.q_L2B(:,3),'LineWidth',2)
grid on
xlabel('Time [sec]')
title('q_3')
subplot 224
plot(Landing_debug_params.time,Landing_debug_params.Local_Level.q_L2B(:,4),'LineWidth',2)
grid on
xlabel('Time [sec]')
title('q_4')

figure('name','w L2B est')
subplot 311
plot(Landing_debug_params.time,Landing_debug_params.Local_Level.w_L2B_est(:,1),'LineWidth',1.5)
grid on
xlabel('Time [sec]')
ylabel('w_x [rad/sec]')
title('w_x')
subplot 312
plot(Landing_debug_params.time,Landing_debug_params.Local_Level.w_L2B_est(:,2),'LineWidth',1.5)
grid on
xlabel('Time [sec]')
ylabel('w_y [rad/sec]')
title('w_y')
subplot 313
plot(Landing_debug_params.time,Landing_debug_params.Local_Level.w_L2B_est(:,3),'LineWidth',1.5)
grid on
xlabel('Time [sec]')
ylabel('w_z [rad/sec]')
title('w_z')

figure('name','LLA surf')
subplot 311
plot(Landing_debug_params.time,Landing_debug_params.Local_Level.LLA_surf.lat,'LineWidth',1.5)
grid on
xlabel('Time [sec]')
ylabel('Lat [rad]')
title('Lat surf')
subplot 312
plot(Landing_debug_params.time,Landing_debug_params.Local_Level.LLA_surf.lon,'LineWidth',1.5)
grid on
xlabel('Time [sec]')
ylabel('Lon [rad]')
title('Lon surf')
subplot 313
plot(Landing_debug_params.time,Landing_debug_params.Local_Level.LLA_surf.alt,'LineWidth',1.5)
grid on
xlabel('Time [sec]')
ylabel('alt [m]')
title('alt surf')

figure('name','LLA')
subplot 311
plot(Landing_debug_params.time,Landing_debug_params.Local_Level.LLA.lat,'LineWidth',1.5)
grid on
xlabel('Time [sec]')
ylabel('Lat [rad]')
title('Lat')
subplot 312
plot(Landing_debug_params.time,Landing_debug_params.Local_Level.LLA.lon,'LineWidth',1.5)
grid on
xlabel('Time [sec]')
ylabel('Lon [rad]')
title('Lon')
subplot 313
plot(Landing_debug_params.time,Landing_debug_params.Local_Level.LLA.alt,'LineWidth',1.5)
grid on
xlabel('Time [sec]')
ylabel('alt [m]')
title('alt')

figure('name','RPY lib angles')
subplot 311
plot(Landing_debug_params.time,Landing_debug_params.Local_Level.lib_angles(:,1),'LineWidth',1.5)
grid on
xlabel('Time [sec]')
ylabel('[rad]')
title('Lib angles 1')
subplot 312
plot(Landing_debug_params.time,Landing_debug_params.Local_Level.lib_angles(:,2),'LineWidth',1.5)
grid on
xlabel('Time [sec]')
ylabel('[rad]')
title('Lib angles 1')
subplot 313
plot(Landing_debug_params.time,Landing_debug_params.Local_Level.lib_angles(:,3),'LineWidth',1.5)
grid on
xlabel('Time [sec]')
ylabel('[m]')
title('Lib angles 1')

figure('name','V NED')
subplot 311
plot(Landing_debug_params.time,Landing_debug_params.Local_Level.vNED(:,1),'LineWidth',2)
grid on
xlabel('Time [sec]')
title('vNED - X')
subplot 312
plot(Landing_debug_params.time,Landing_debug_params.Local_Level.vNED(:,2),'LineWidth',2)
grid on
xlabel('Time [sec]')
title('vNED - Y')
subplot 313
plot(Landing_debug_params.time,Landing_debug_params.Local_Level.vNED(:,3),'LineWidth',2)
grid on
xlabel('Time [sec]')
title('vNED - Z')