function [] = plot_OMPS(mission_simulator_results)

plot_time = mission_simulator_results.Time;
ranges = mission_simulator_results.AOCS_Inputs.LDVmeas.r;
ranges(ranges>10000) = nan;

figure('name','LDV ranges')
subplot 221
plot(plot_time,ranges(:,1))
grid on
xlabel('Time [sec]')
title('range 1')
subplot 222
plot(plot_time,ranges(:,2))
grid on
xlabel('Time [sec]')
title('range 2')
subplot 223
plot(plot_time,ranges(:,3))
grid on
xlabel('Time [sec]')
title('range 3')
subplot 224
plot(plot_time,ranges(:,4))
grid on
xlabel('Time [sec]')
title('range 4')

figure('name','LDV DS')
subplot 221
plot(plot_time,mission_simulator_results.AOCS_Inputs.LDVmeas.ds(:,1))
grid on
xlabel('Time [sec]')
title('DS 1')
subplot 222
plot(plot_time,mission_simulator_results.AOCS_Inputs.LDVmeas.ds(:,2))
grid on
xlabel('Time [sec]')
title('DS 2')
subplot 223
plot(plot_time,mission_simulator_results.AOCS_Inputs.LDVmeas.ds(:,3))
grid on
xlabel('Time [sec]')
title('DS 3')
subplot 224
plot(plot_time,mission_simulator_results.AOCS_Inputs.LDVmeas.ds(:,4))
grid on
xlabel('Time [sec]')
title('DS 4')


figure('name','LDV Validity')
plot(plot_time,mission_simulator_results.AOCS_Inputs.LDVmeas.Validity)
grid on
xlabel('Time [sec]')
title('LDV Validity')