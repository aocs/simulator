function [] = plot_STR_landing(mission_simulator_results)

%%%% STR 1 quaternion

figure('name','STR quat')
subplot 221
plot(mission_simulator_results.Time,mission_simulator_results.AOCS_Inputs.STRmeas.q_I2STR_meas{:,1})
grid on
title('STR - q_0')
xlabel('time [sec]')
ylim([-1,1])
subplot 222
plot(mission_simulator_results.Time,mission_simulator_results.AOCS_Inputs.STRmeas.q_I2STR_meas{:,2})
grid on
title('STR - q_1')
xlabel('time [sec]')
ylim([-1,1])
subplot 223
plot(mission_simulator_results.Time,mission_simulator_results.AOCS_Inputs.STRmeas.q_I2STR_meas{:,3})
grid on
title('STR - q_2')
xlabel('time [sec]')
ylim([-1,1])
subplot 224
plot(mission_simulator_results.Time,mission_simulator_results.AOCS_Inputs.STRmeas.q_I2STR_meas{:,4})
grid on
title('STR - q_3')
xlabel('time [sec]')
ylim([-1,1])

%%%% STR 1 return str
figure('name','Return STR')
plot(mission_simulator_results.Time,mission_simulator_results.AOCS_Inputs.STRmeas.return_STR{:,1})
grid on
title('STR - Return STR')
xlabel('time [sec]')

%%%% STR 1 epoch time
figure('name','STR Epoch time')
plot(mission_simulator_results.Time,mission_simulator_results.AOCS_Inputs.STRmeas.epoch_time{:,1})
grid on
title('STR - Epoch time')
xlabel('time [sec]')
ylabel('time [sec]')
ylim([-0.05,0.2])
