function [] = plot_spacecraft_quat_landing(mission_simulator_results,Landing_debug_params)

[m,~] = size(mission_simulator_results.Inertial_attitude.q);
plot_time = linspace(mission_simulator_results.Time(1),mission_simulator_results.Time(end),m);

figure('name','q I2B')
subplot 221
grid on
plot(plot_time,mission_simulator_results.Inertial_attitude.q(:,1))
grid on
title('q I2B - q_0')
xlabel('time [sec]')
ylim([-1,1])
subplot 222
plot(plot_time,mission_simulator_results.Inertial_attitude.q(:,2))
grid on
title('q I2B - q_1')
xlabel('time [sec]')
ylim([-1,1])
subplot 223
plot(plot_time,mission_simulator_results.Inertial_attitude.q(:,3))
grid on
title('q I2B - q_2')
xlabel('time [sec]')
ylim([-1,1])
subplot 224
plot(plot_time,mission_simulator_results.Inertial_attitude.q(:,4))
grid on
title('q I2B - q_3')
xlabel('time [sec]')
ylim([-1,1])


% figure('name','quat I2b est-real comparison')
% subplot 221
% plot(Landing_debug_params.time,Landing_debug_params.NAV.inertial_attitude.q_I2B_est(:,1),'r','LineWidth',2)
% hold on
% plot(Landing_debug_params.time,Landing_debug_params.GNC.GUIDANCE_CMD.q_I2B_req(:,1),'b','LineWidth',2)
% grid on
% title('q_1')
% xlabel('time [sec]')
% ylim([-1,1])
% legend('q I2B est','Q I2B req')
% subplot 222
% plot(Landing_debug_params.time,Landing_debug_params.NAV.inertial_attitude.q_I2B_est(:,2),'r','LineWidth',2)
% hold on
% plot(Landing_debug_params.time,Landing_debug_params.GNC.GUIDANCE_CMD.q_I2B_req(:,2),'b','LineWidth',2)
% grid on
% title('q_2')
% xlabel('time [sec]')
% ylim([-1,1])
% subplot 223
% plot(Landing_debug_params.time,Landing_debug_params.NAV.inertial_attitude.q_I2B_est(:,3),'r','LineWidth',2)
% hold on
% plot(Landing_debug_params.time,Landing_debug_params.GNC.GUIDANCE_CMD.q_I2B_req(:,3),'b','LineWidth',2)
% grid on
% title('q_3')
% xlabel('time [sec]')
% ylim([-1,1])
% subplot 224
% plot(Landing_debug_params.time,Landing_debug_params.NAV.inertial_attitude.q_I2B_est(:,4),'r','LineWidth',2)
% hold on
% plot(Landing_debug_params.time,Landing_debug_params.GNC.GUIDANCE_CMD.q_I2B_req(:,4),'b','LineWidth',2)
% grid on
% title('q_4')
% xlabel('time [sec]')
% ylim([-1,1])