function []=plot_vertical_descent_results(Landing_debug_params,mission_simulator_results)

AOCS_enumerations;
%%
VD_INIT_INDX = find(Landing_debug_params.GNC.GUIDANCE_status.Landing_Vertical_descent_status==...
    Vertical_descent_status_E.Active_Descent,1);

VD_FINAL_INDX = find(mission_simulator_results.LOGIC.modes.landing_mode.MODES_LANDING_MODE==...
    LANDING_MODES_E.Landed,1);

VD_INDXS = VD_INIT_INDX:VD_FINAL_INDX;

VD_TIMES = Landing_debug_params.time(VD_INDXS);
VD_TIMES = VD_TIMES-VD_TIMES(1);

v_down = Landing_debug_params.Local_Level.vNED(VD_INDXS,3);
v_horiz = sqrt(sum(Landing_debug_params.Local_Level.vNED(VD_INDXS,1:2).^2,2));
h_surf = Landing_debug_params.Local_Level.LLA_surf.alt(VD_INDXS);
q_L2B = Landing_debug_params.Local_Level.q_L2B(VD_INDXS,:);

up_err_deg = 90-calc_tilt_landing(q_L2B);

range = mission_simulator_results.AOCS_Inputs.LDVmeas.r(VD_INDXS,:);
range(range>10000) = nan;

doppler = mission_simulator_results.AOCS_Inputs.LDVmeas.ds(VD_INDXS,:);

%%


figure('name','alt - V down')
plot(v_down,h_surf,'LineWidth',2)
grid on
xlabel('v_{down} [m/sec]')
ylabel('h_{surf} [m]')
title('alt - V down')

figure('name','alt surf')
plot(VD_TIMES,h_surf,'LineWidth',2)
grid on
xlabel('time [sec]')
ylabel('h_{surf} [m]')
title('alt surf')

figure('name','v down')
plot(VD_TIMES,v_down,'LineWidth',2)
grid on
xlabel('time [sec]')
ylabel('v_{down} [m/sec]')
title('v down')

figure('name','v horiz')
plot(VD_TIMES,v_horiz,'LineWidth',2)
grid on
xlabel('time [sec]')
ylabel('v_{horiz} [m/sec]')
title('v horiz')

figure('name','z2up')
plot(VD_TIMES,up_err_deg,'LineWidth',2)
grid on
xlabel('time [sec]')
ylabel('up err [deg]')
title('z2up angle error')

figure('name','LDV range')
plot(VD_TIMES,range)
grid on
xlabel('time [sec]')
ylabel('range [m]')
title('omps range meas.')

figure('name','LDV doppler')
plot(VD_TIMES,doppler)
grid on
xlabel('time [sec]')
ylabel('doppler [m/sec]')
title('omps doppler meas.')
