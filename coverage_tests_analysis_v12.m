clc;close all;
clear aocs_spacecraft_data;
clear aocs_outputs;
clear aocs_inputs;
clear aocs_debug;

addpath(genpath('C:\Users\noamle.DESKTOP-MOOJ4I4\Desktop\AOCS\v1.2\AOCS\CodeGeneration\Simulator_log_files\coverage tests')) %data's directory
%% extract the data from the files
aocs_spacecraft_data = readtable('aocs_spacecraft_data.csv'); %enter the aocs_spacecraft_data current name
aocs_outputs = readtable('aocs_outputs.csv'); %enter the aocs_outputs current name
aocs_inputs = readtable('aocs_inputs.csv'); %enter the aocs_inputs current name
aocs_debug = readtable('aocs_debug.csv'); %reads data from aocs_debug

%% plots
% plot_sun_pointing_simulator_compare(aocs_spacecraft_data,logsout)
% plot_CRUISE_status_simulator_compare(aocs_inputs,aocs_outputs,aocs_spacecraft_data,aocs_debug,logsout)
% plot_GNC_status_simulator_compare(aocs_debug,aocs_outputs,aocs_spacecraft_data,logsout)