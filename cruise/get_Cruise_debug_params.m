function Cruise_debug_params = get_Cruise_debug_params(aocs_debug)

%% get sim's time and tick count
[m,~] = size(aocs_debug.SimTime);
Cruise_debug_params.time = zeros(m,1);
%%%%% sort simulator times
for i=1:m
    Cruise_debug_params.time(i) = i*0.02;
    Cruise_debug_params.Tick_time(i) = aocs_debug.TickNumber(end) - 2*(m-i);
end
Cruise_debug_params.Tick_time = Cruise_debug_params.Tick_time';

    
%% GNC guidance CMD
Cruise_debug_params.GNC.GUIDANCE_CMD.apply_DV_enable = aocs_debug.Apply_DV_Enable;
Cruise_debug_params.GNC.GUIDANCE_CMD.attitude_band = aocs_debug.attitude_band;
Cruise_debug_params.GNC.GUIDANCE_CMD.ang_vel_band = aocs_debug.ang_vel_band;
%%%% q set point
Cruise_debug_params.GNC.GUIDANCE_CMD.q_SetPoint(:,1) = aocs_debug.q_setpoint_0_;
Cruise_debug_params.GNC.GUIDANCE_CMD.q_SetPoint(:,2) = aocs_debug.q_setpoint_1_;
Cruise_debug_params.GNC.GUIDANCE_CMD.q_SetPoint(:,3) = aocs_debug.q_setpoint_2_;
Cruise_debug_params.GNC.GUIDANCE_CMD.q_SetPoint(:,4) = aocs_debug.q_setpoint_3_;
%%%% q I2B req
Cruise_debug_params.GNC.GUIDANCE_CMD.q_I2B_req(:,1) = aocs_debug.q_i2b_required_0_;
Cruise_debug_params.GNC.GUIDANCE_CMD.q_I2B_req(:,2) = aocs_debug.q_i2b_required_1_;
Cruise_debug_params.GNC.GUIDANCE_CMD.q_I2B_req(:,3) = aocs_debug.q_i2b_required_2_;
Cruise_debug_params.GNC.GUIDANCE_CMD.q_I2B_req(:,4) = aocs_debug.q_i2b_required_3_;

%% GNC guidance status
Cruise_debug_params.GNC.GUIDANCE_status.apply_DV_status = aocs_debug.ApplyDV_status;
Cruise_debug_params.GNC.GUIDANCE_status.set_Attitude_status = aocs_debug.Set_Attitude_status;

%% GNC control status
Cruise_debug_params.GNC.CONTROL_status.control_mode = aocs_debug.CONTROL_MODE;
Cruise_debug_params.GNC.CONTROL_status.PWM_logic_mode = aocs_debug.PWM_LOGIC_MODE;

%% GNC control CMD
Cruise_debug_params.GNC.CONTROL_CMD.rate_limit = aocs_debug.rate_limit;
Cruise_debug_params.GNC.CONTROL_CMD.att_err = aocs_debug.att_err;
Cruise_debug_params.GNC.CONTROL_CMD.vel_err = aocs_debug.vel_err;
Cruise_debug_params.GNC.CONTROL_CMD.PID_reset = aocs_debug.PID_RESET;
Cruise_debug_params.GNC.CONTROL_CMD.ACS_fine = aocs_debug.ACS_FINE;
Cruise_debug_params.GNC.CONTROL_CMD.ACS_full = aocs_debug.ACS_FULL;
Cruise_debug_params.GNC.CONTROL_CMD.OFFMODE = aocs_debug.OFFMOD;
Cruise_debug_params.GNC.CONTROL_CMD.PD_limiter = aocs_debug.PD_LIMITER;
Cruise_debug_params.GNC.CONTROL_CMD.fill_ratio = aocs_debug.fill_ratio;
%%% int vec
Cruise_debug_params.GNC.CONTROL_CMD.int_vec(:,1) = aocs_debug.int_vec_0_;
Cruise_debug_params.GNC.CONTROL_CMD.int_vec(:,2) = aocs_debug.int_vec_1_;
Cruise_debug_params.GNC.CONTROL_CMD.int_vec(:,3) = aocs_debug.int_vec_2_;
%%% int2 vec
Cruise_debug_params.GNC.CONTROL_CMD.int2_vec(:,1) = aocs_debug.int2_vec_0_;
Cruise_debug_params.GNC.CONTROL_CMD.int2_vec(:,2) = aocs_debug.int2_vec_1_;
Cruise_debug_params.GNC.CONTROL_CMD.int2_vec(:,3) = aocs_debug.int2_vec_2_;
%%%% fine control error band
Cruise_debug_params.GNC.CONTROL_CMD.fine_control_err_band(:,1) = aocs_debug.fine_cotrol_err_band_0_;
Cruise_debug_params.GNC.CONTROL_CMD.fine_control_err_band(:,2) = aocs_debug.fine_cotrol_err_band_1_;
Cruise_debug_params.GNC.CONTROL_CMD.fine_control_err_band(:,3) = aocs_debug.fine_cotrol_err_band_2_;
%%%% fine control omega band
Cruise_debug_params.GNC.CONTROL_CMD.fine_control_omega_band(:,1) = aocs_debug.fine_cotrol_omega_band_0_;
Cruise_debug_params.GNC.CONTROL_CMD.fine_control_omega_band(:,2) = aocs_debug.fine_cotrol_omega_band_1_;
Cruise_debug_params.GNC.CONTROL_CMD.fine_control_omega_band(:,3) = aocs_debug.fine_cotrol_omega_band_2_;
%%%% U com
Cruise_debug_params.GNC.CONTROL_CMD.U_com(:,1) = aocs_debug.u_com_0_;
Cruise_debug_params.GNC.CONTROL_CMD.U_com(:,2) = aocs_debug.u_com_1_;
Cruise_debug_params.GNC.CONTROL_CMD.U_com(:,3) = aocs_debug.u_com_2_;
Cruise_debug_params.GNC.CONTROL_CMD.U_com(:,4) = aocs_debug.u_com_3_;
Cruise_debug_params.GNC.CONTROL_CMD.U_com(:,5) = aocs_debug.u_com_4_;
Cruise_debug_params.GNC.CONTROL_CMD.U_com(:,6) = aocs_debug.u_com_5_;
Cruise_debug_params.GNC.CONTROL_CMD.U_com(:,7) = aocs_debug.u_com_6_;
Cruise_debug_params.GNC.CONTROL_CMD.U_com(:,8) = aocs_debug.u_com_7_;

%%%% Ubit acs req
Cruise_debug_params.GNC.CONTROL_CMD.Ubit_acs_req(:,1) = aocs_debug.ubit_acs_req_0_;
Cruise_debug_params.GNC.CONTROL_CMD.Ubit_acs_req(:,2) = aocs_debug.ubit_acs_req_1_;
Cruise_debug_params.GNC.CONTROL_CMD.Ubit_acs_req(:,3) = aocs_debug.ubit_acs_req_2_;
Cruise_debug_params.GNC.CONTROL_CMD.Ubit_acs_req(:,4) = aocs_debug.ubit_acs_req_3_;
Cruise_debug_params.GNC.CONTROL_CMD.Ubit_acs_req(:,5) = aocs_debug.ubit_acs_req_4_;
Cruise_debug_params.GNC.CONTROL_CMD.Ubit_acs_req(:,6) = aocs_debug.ubit_acs_req_5_;
Cruise_debug_params.GNC.CONTROL_CMD.Ubit_acs_req(:,7) = aocs_debug.ubit_acs_req_6_;
Cruise_debug_params.GNC.CONTROL_CMD.Ubit_acs_req(:,8) = aocs_debug.ubit_acs_req_7_;

%%%% AHT com
Cruise_debug_params.GNC.CONTROL_CMD.AHT_COM(:,1) = aocs_debug.AHT_COM_0_;
Cruise_debug_params.GNC.CONTROL_CMD.AHT_COM(:,2) = aocs_debug.AHT_COM_1_;
Cruise_debug_params.GNC.CONTROL_CMD.AHT_COM(:,3) = aocs_debug.AHT_COM_2_;
Cruise_debug_params.GNC.CONTROL_CMD.AHT_COM(:,4) = aocs_debug.AHT_COM_3_;
%%%%pd limiter gains
Cruise_debug_params.GNC.CONTROL_CMD.PD_limiter_gains(:,1) = aocs_debug.PD_limiter_gains_0_;
Cruise_debug_params.GNC.CONTROL_CMD.PD_limiter_gains(:,2) = aocs_debug.PD_limiter_gains_1_;
Cruise_debug_params.GNC.CONTROL_CMD.PD_limiter_gains(:,3) = aocs_debug.PD_limiter_gains_2_;
Cruise_debug_params.GNC.CONTROL_CMD.PD_limiter_gains(:,4) = aocs_debug.PD_limiter_gains_3_;
Cruise_debug_params.GNC.CONTROL_CMD.PD_limiter_gains(:,5) = aocs_debug.PD_limiter_gains_4_;
Cruise_debug_params.GNC.CONTROL_CMD.PD_limiter_gains(:,6) = aocs_debug.PD_limiter_gains_5_;
%%%% w vec
Cruise_debug_params.GNC.CONTROL_CMD.w_vec(:,1) = aocs_debug.w_vec_0_;
Cruise_debug_params.GNC.CONTROL_CMD.w_vec(:,2) = aocs_debug.w_vec_1_;
Cruise_debug_params.GNC.CONTROL_CMD.w_vec(:,3) = aocs_debug.w_vec_2_;
%%%% req torque
Cruise_debug_params.GNC.CONTROL_CMD.req_torque(:,1) = aocs_debug.req_torque_0_;
Cruise_debug_params.GNC.CONTROL_CMD.req_torque(:,2) = aocs_debug.req_torque_1_;
Cruise_debug_params.GNC.CONTROL_CMD.req_torque(:,3) = aocs_debug.req_torque_2_;
%%%% err vec
Cruise_debug_params.GNC.CONTROL_CMD.err_vec(:,1) = aocs_debug.err_vec_0_;
Cruise_debug_params.GNC.CONTROL_CMD.err_vec(:,2) = aocs_debug.err_vec_1_;
Cruise_debug_params.GNC.CONTROL_CMD.err_vec(:,3) = aocs_debug.err_vec_2_;
%%%% PID gains alpha
Cruise_debug_params.GNC.CONTROL_CMD.PID_gains_alpha(:,1) = aocs_debug.PID_gains_alpha_0_;
Cruise_debug_params.GNC.CONTROL_CMD.PID_gains_alpha(:,2) = aocs_debug.PID_gains_alpha_1_;
Cruise_debug_params.GNC.CONTROL_CMD.PID_gains_alpha(:,3) = aocs_debug.PID_gains_alpha_2_;
Cruise_debug_params.GNC.CONTROL_CMD.PID_gains_alpha(:,4) = aocs_debug.PID_gains_alpha_3_;
Cruise_debug_params.GNC.CONTROL_CMD.PID_gains_alpha(:,5) = aocs_debug.PID_gains_alpha_4_;
Cruise_debug_params.GNC.CONTROL_CMD.PID_gains_alpha(:,6) = aocs_debug.PID_gains_alpha_5_;
Cruise_debug_params.GNC.CONTROL_CMD.PID_gains_alpha(:,7) = aocs_debug.PID_gains_alpha_6_;
Cruise_debug_params.GNC.CONTROL_CMD.PID_gains_alpha(:,8) = aocs_debug.PID_gains_alpha_7_;
Cruise_debug_params.GNC.CONTROL_CMD.PID_gains_alpha(:,9) = aocs_debug.PID_gains_alpha_8_;
Cruise_debug_params.GNC.CONTROL_CMD.PID_gains_alpha(:,10) = aocs_debug.PID_gains_alpha_9_;
Cruise_debug_params.GNC.CONTROL_CMD.PID_gains_alpha(:,11) = aocs_debug.PID_gains_alpha_10_;
Cruise_debug_params.GNC.CONTROL_CMD.PID_gains_alpha(:,12) = aocs_debug.PID_gains_alpha_11_;
%%%% PID gains 0
Cruise_debug_params.GNC.CONTROL_CMD.PID_gains_0(:,1) = aocs_debug.PID_gains_0_0_;
Cruise_debug_params.GNC.CONTROL_CMD.PID_gains_0(:,2) = aocs_debug.PID_gains_0_1_;
Cruise_debug_params.GNC.CONTROL_CMD.PID_gains_0(:,3) = aocs_debug.PID_gains_0_2_;
Cruise_debug_params.GNC.CONTROL_CMD.PID_gains_0(:,4) = aocs_debug.PID_gains_0_3_;
Cruise_debug_params.GNC.CONTROL_CMD.PID_gains_0(:,5) = aocs_debug.PID_gains_0_4_;
Cruise_debug_params.GNC.CONTROL_CMD.PID_gains_0(:,6) = aocs_debug.PID_gains_0_5_;
Cruise_debug_params.GNC.CONTROL_CMD.PID_gains_0(:,7) = aocs_debug.PID_gains_0_6_;
Cruise_debug_params.GNC.CONTROL_CMD.PID_gains_0(:,8) = aocs_debug.PID_gains_0_7_;
Cruise_debug_params.GNC.CONTROL_CMD.PID_gains_0(:,9) = aocs_debug.PID_gains_0_8_;
Cruise_debug_params.GNC.CONTROL_CMD.PID_gains_0(:,10) = aocs_debug.PID_gains_0_9_;
Cruise_debug_params.GNC.CONTROL_CMD.PID_gains_0(:,11) = aocs_debug.PID_gains_0_10_;
Cruise_debug_params.GNC.CONTROL_CMD.PID_gains_0(:,12) = aocs_debug.PID_gains_0_11_;
%%%% PID gains 
Cruise_debug_params.GNC.CONTROL_CMD.PID_gains(:,1) = aocs_debug.PID_gains_0_;
Cruise_debug_params.GNC.CONTROL_CMD.PID_gains(:,2) = aocs_debug.PID_gains_1_;
Cruise_debug_params.GNC.CONTROL_CMD.PID_gains(:,3) = aocs_debug.PID_gains_2_;
Cruise_debug_params.GNC.CONTROL_CMD.PID_gains(:,4) = aocs_debug.PID_gains_3_;
Cruise_debug_params.GNC.CONTROL_CMD.PID_gains(:,5) = aocs_debug.PID_gains_4_;
Cruise_debug_params.GNC.CONTROL_CMD.PID_gains(:,6) = aocs_debug.PID_gains_5_;
Cruise_debug_params.GNC.CONTROL_CMD.PID_gains(:,7) = aocs_debug.PID_gains_6_;
Cruise_debug_params.GNC.CONTROL_CMD.PID_gains(:,8) = aocs_debug.PID_gains_7_;
Cruise_debug_params.GNC.CONTROL_CMD.PID_gains(:,9) = aocs_debug.PID_gains_8_;
Cruise_debug_params.GNC.CONTROL_CMD.PID_gains(:,10) = aocs_debug.PID_gains_9_;
Cruise_debug_params.GNC.CONTROL_CMD.PID_gains(:,11) = aocs_debug.PID_gains_10_;
Cruise_debug_params.GNC.CONTROL_CMD.PID_gains(:,12) = aocs_debug.PID_gains_11_;

%% NAV inertial attitude
Cruise_debug_params.NAV.inertial_attitude.confidence_level_q = aocs_debug.Confidece_Level_q;
Cruise_debug_params.NAV.inertial_attitude.confidence_level_w = aocs_debug.Confidece_Level_w;
Cruise_debug_params.NAV.inertial_attitude.estimator_choice = aocs_debug.Estimator_Choice;
Cruise_debug_params.NAV.inertial_attitude.STR1_FRAME_ALG = aocs_debug.STR1_FRAME_ALG;
Cruise_debug_params.NAV.inertial_attitude.STR2_FRAME_ALG = aocs_debug.STR2_FRAME_ALG;
Cruise_debug_params.NAV.inertial_attitude.inertial_nav_status_telemetry = aocs_debug.INNERTIAL_NAV_STATUS_TELEMETRY;
%%%% q I2B est
Cruise_debug_params.NAV.inertial_attitude.q_I2B_est(:,1) = aocs_debug.q_i2b_est_0_;
Cruise_debug_params.NAV.inertial_attitude.q_I2B_est(:,2) = aocs_debug.q_i2b_est_1_;
Cruise_debug_params.NAV.inertial_attitude.q_I2B_est(:,3) = aocs_debug.q_i2b_est_2_;
Cruise_debug_params.NAV.inertial_attitude.q_I2B_est(:,4) = aocs_debug.q_i2b_est_3_;
%%%% w I2B est
Cruise_debug_params.NAV.inertial_attitude.w_I2B_est(:,1) = aocs_debug.w_i2b_est_0_;
Cruise_debug_params.NAV.inertial_attitude.w_I2B_est(:,2) = aocs_debug.w_i2b_est_1_;
Cruise_debug_params.NAV.inertial_attitude.w_I2B_est(:,3) = aocs_debug.w_i2b_est_2_;

%% NAV DV
Cruise_debug_params.NAV.DV.deltaV_tot = aocs_debug.deltaV_tot;

%% NAV spacecraft parameters
Cruise_debug_params.NAV.spacecraft_parameters.mass = aocs_debug.mass;
Cruise_debug_params.NAV.spacecraft_parameters.MassAlgorithm = aocs_debug.MassAlgorithm;
%%%% inertia
Cruise_debug_params.NAV.spacecraft_parameters.inertia(:,1) = aocs_debug.inertia_0_;
Cruise_debug_params.NAV.spacecraft_parameters.inertia(:,2) = aocs_debug.inertia_1_;
Cruise_debug_params.NAV.spacecraft_parameters.inertia(:,3) = aocs_debug.inertia_2_;
Cruise_debug_params.NAV.spacecraft_parameters.inertia(:,4) = aocs_debug.inertia_3_;
Cruise_debug_params.NAV.spacecraft_parameters.inertia(:,5) = aocs_debug.inertia_4_;
Cruise_debug_params.NAV.spacecraft_parameters.inertia(:,6) = aocs_debug.inertia_5_;
Cruise_debug_params.NAV.spacecraft_parameters.inertia(:,7) = aocs_debug.inertia_6_;
Cruise_debug_params.NAV.spacecraft_parameters.inertia(:,8) = aocs_debug.inertia_7_;
Cruise_debug_params.NAV.spacecraft_parameters.inertia(:,9) = aocs_debug.inertia_8_;
%%%% CM
Cruise_debug_params.NAV.spacecraft_parameters.CM(:,1) = aocs_debug.CM_0_;
Cruise_debug_params.NAV.spacecraft_parameters.CM(:,2) = aocs_debug.CM_1_;
Cruise_debug_params.NAV.spacecraft_parameters.CM(:,3) = aocs_debug.CM_2_;

%% NAV specific force
Cruise_debug_params.NAV.specific_force.fb_est(:,1) = aocs_debug.fb_est_0_; 
Cruise_debug_params.NAV.specific_force.fb_est(:,2) = aocs_debug.fb_est_1_; 
Cruise_debug_params.NAV.specific_force.fb_est(:,3) = aocs_debug.fb_est_2_; 
Cruise_debug_params.NAV.specific_force.algo_ID = aocs_debug.algo_ID; 

%% NAV sun direction
Cruise_debug_params.NAV.sun_direction.sun_vec_b_css(:,1) = aocs_debug.sun_vec_b_css_0_;
Cruise_debug_params.NAV.sun_direction.sun_vec_b_css(:,2) = aocs_debug.sun_vec_b_css_1_;
Cruise_debug_params.NAV.sun_direction.sun_vec_b_css(:,3) = aocs_debug.sun_vec_b_css_2_;
Cruise_debug_params.NAV.sun_direction.cl_sunvec_css = aocs_debug.cl_sunvecb_css;

%% NAV engines
Cruise_debug_params.NAV.engines.Force_est(:,1) = aocs_debug.Force_est_0_;
Cruise_debug_params.NAV.engines.Force_est(:,2) = aocs_debug.Force_est_1_;
Cruise_debug_params.NAV.engines.Force_est(:,3) = aocs_debug.Force_est_2_;
Cruise_debug_params.NAV.engines.Torque_est(:,1) = aocs_debug.Torque_est_0_;
Cruise_debug_params.NAV.engines.Torque_est(:,2) = aocs_debug.Torque_est_1_;
Cruise_debug_params.NAV.engines.Torque_est(:,3) = aocs_debug.Torque_est_2_;

%% inertial pointing
Cruise_debug_params.inertial_pointing.axis_pointing_enable = aocs_debug.Axis_Pointing_Enable;
Cruise_debug_params.inertial_pointing.req_z_axis(:,1) = aocs_debug.req_inertial_z_axis_0_;
Cruise_debug_params.inertial_pointing.req_z_axis(:,2) = aocs_debug.req_inertial_z_axis_1_;
Cruise_debug_params.inertial_pointing.req_z_axis(:,3) = aocs_debug.req_inertial_z_axis_2_;
Cruise_debug_params.inertial_pointing.MW_rpm_req = aocs_debug.mw_rpm_req;
Cruise_debug_params.inertial_pointing.axis_pointing_status = aocs_debug.AxisPointing_status;


