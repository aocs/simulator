function [mission_simulator_results] = get_init_cond_simulator(aocs_spacecraft_data,mission_simulator_results)

aocs_spacecraft_data = table2array(aocs_spacecraft_data);

%% get initial conditions
year = aocs_spacecraft_data(1,4);
month = aocs_spacecraft_data(1,5);
day = aocs_spacecraft_data(1,6);
hour = aocs_spacecraft_data(1,7);
minute = aocs_spacecraft_data(1,8);
second = aocs_spacecraft_data(1,9);

X = aocs_spacecraft_data(1,11);
Y = aocs_spacecraft_data(1,12);
Z = aocs_spacecraft_data(1,13);

Vx = aocs_spacecraft_data(1,14);
Vy = aocs_spacecraft_data(1,15);
Vz = aocs_spacecraft_data(1,16);

q0 = aocs_spacecraft_data(1,17);
q1 = aocs_spacecraft_data(1,18);
q2 = aocs_spacecraft_data(1,19);
q3 = aocs_spacecraft_data(1,20);

omega_x = aocs_spacecraft_data(1,21);
omega_y = aocs_spacecraft_data(1,22);
omega_z = aocs_spacecraft_data(1,23);

%% write the initial conditions to the struct
mission_simulator_results.init_conditions.MNVR_start_date = [year, month, day, hour, minute, second];
mission_simulator_results.init_conditions.q_0 = [q0, q1, q2, q3];
mission_simulator_results.init_conditions.XI0 = [X, Y, Z];
mission_simulator_results.init_conditions.VI0 = [Vx, Vy, Vz];
mission_simulator_results.init_conditions.w_I2B0 = [omega_x, omega_y, omega_z];
