function [] = plot_CSS_cruise(mission_simulator_results)

[m,~] = size(mission_simulator_results.AOCS_Inputs.SSmeas(1).sun_angle);
plot_time = linspace(mission_simulator_results.Time(1),mission_simulator_results.Time(end),m);

%%%% CSS 1 angles and validity [rad]
figure('name','CSS 1 [rad]')
subplot 311
plot (plot_time, mission_simulator_results.AOCS_Inputs.SSmeas(1).sun_angle{:,1},'LineWidth',2)
grid on
title('CSS 1 angle 1 - rad')
xlabel('time [sec]')
ylabel('angle [rad]')
subplot 312
plot (plot_time, mission_simulator_results.AOCS_Inputs.SSmeas(1).sun_angle{:,2},'LineWidth',2)
grid on
title('CSS 1 angle 2 - rad')
xlabel('time [sec]')
ylabel('angle [rad]')
subplot 313
plot (plot_time, mission_simulator_results.AOCS_Inputs.SSmeas(1).validity{:,1},'LineWidth',2)
grid on
title('CSS 1 validity')
xlabel('time [sec]')

%%%% CSS 2 angles and validity [rad]
figure('name','CSS 2 [rad]')
subplot 311
plot (plot_time, mission_simulator_results.AOCS_Inputs.SSmeas(2).sun_angle{:,1},'LineWidth',2)
grid on
title('CSS 2 angle 1 - rad')
xlabel('time [sec]')
ylabel('angle [rad]')
subplot 312
plot (plot_time, mission_simulator_results.AOCS_Inputs.SSmeas(2).sun_angle{:,2},'LineWidth',2)
grid on
title('CSS 2 angle 2 - rad')
xlabel('time [sec]')
ylabel('angle [rad]')
subplot 313
plot (plot_time, mission_simulator_results.AOCS_Inputs.SSmeas(2).validity{:,1},'LineWidth',2)
grid on
title('CSS 2 validity')
xlabel('time [sec]')

%%%% CSS 3 angles and validity [rad]
figure('name','CSS 3 [rad]')
subplot 311
plot (plot_time, mission_simulator_results.AOCS_Inputs.SSmeas(3).sun_angle{:,1},'LineWidth',2)
grid on
title('CSS 3 angle 1 - rad')
xlabel('time [sec]')
ylabel('angle [rad]')
subplot 312
plot (plot_time, mission_simulator_results.AOCS_Inputs.SSmeas(3).sun_angle{:,2},'LineWidth',2)
grid on
title('CSS 3 angle 2 - rad')
xlabel('time [sec]')
subplot 313
plot (plot_time, mission_simulator_results.AOCS_Inputs.SSmeas(3).validity{:,1},'LineWidth',2)
grid on
title('CSS 3 validity')
xlabel('time [sec]')
ylabel('angle [rad]')

%%%% CSS 4 angles and validity [rad]
figure('name','CSS 4 [rad]')
subplot 311
plot (plot_time, mission_simulator_results.AOCS_Inputs.SSmeas(4).sun_angle{:,1},'LineWidth',2)
grid on
title('CSS 4 angle 1 - rad')
xlabel('time [sec]')
ylabel('angle [rad]')
subplot 312
plot (plot_time, mission_simulator_results.AOCS_Inputs.SSmeas(4).sun_angle{:,2},'LineWidth',2)
grid on
title('CSS 4 angle 2 - rad')
xlabel('time [sec]')
ylabel('angle [rad]')
subplot 313
plot (plot_time, mission_simulator_results.AOCS_Inputs.SSmeas(4).validity{:,1},'LineWidth',2)
grid on
title('CSS 4 validity')
xlabel('time [sec]')

%%%% CSS 5 angles and validity [rad]
figure('name','CSS 5 [rad]')
subplot 311
plot (plot_time, mission_simulator_results.AOCS_Inputs.SSmeas(5).sun_angle{:,1},'LineWidth',2)
grid on
title('CSS 5 angle 1 - rad')
xlabel('time [sec]')
ylabel('angle [rad]')
subplot 312
plot (plot_time, mission_simulator_results.AOCS_Inputs.SSmeas(5).sun_angle{:,2},'LineWidth',2)
grid on
title('CSS 5 angle 2 - rad')
xlabel('time [sec]')
ylabel('angle [rad]')
subplot 313
plot (plot_time, mission_simulator_results.AOCS_Inputs.SSmeas(5).validity{:,1},'LineWidth',2)
grid on
title('CSS 5 validity')
xlabel('time [sec]')

%%%% CSS 6 angles and validity [rad]
figure('name','CSS 6 [rad]')
subplot 311
plot (plot_time, mission_simulator_results.AOCS_Inputs.SSmeas(6).sun_angle{:,1},'LineWidth',2)
grid on
title('CSS 6 angle 1 - rad')
xlabel('time [sec]')
ylabel('angle [rad]')
subplot 312
plot (plot_time, mission_simulator_results.AOCS_Inputs.SSmeas(6).sun_angle{:,2},'LineWidth',2)
grid on
title('CSS 6 angle 2 - rad')
xlabel('time [sec]')
ylabel('angle [rad]')
subplot 313
plot (plot_time, mission_simulator_results.AOCS_Inputs.SSmeas(6).validity{:,1},'LineWidth',2)
grid on
title('CSS 6 validity')
xlabel('time [sec]')

%%%%%%%----- plot CSS angles in degrees -----%%%%%%%

%%%% CSS 1 angles and validity [deg]
figure('name','CSS 1 [deg]')
subplot 311
plot (plot_time, rad2deg(mission_simulator_results.AOCS_Inputs.SSmeas(1).sun_angle{:,1}),'LineWidth',2)
grid on
title('CSS 1 angle 1 - deg')
xlabel('time [sec]')
ylabel('angle [deg]')
subplot 312
plot (plot_time, rad2deg(mission_simulator_results.AOCS_Inputs.SSmeas(1).sun_angle{:,2}),'LineWidth',2)
grid on
title('CSS 1 angle 2 - deg')
xlabel('time [sec]')
ylabel('angle [deg]')
subplot 313
plot (plot_time, mission_simulator_results.AOCS_Inputs.SSmeas(1).validity{:,1},'LineWidth',2)
grid on
title('CSS 1 validity')
xlabel('time [sec]')

%%%% CSS 2 angles and validity [deg]
figure('name','CSS 2 [deg]')
subplot 311
plot (plot_time, rad2deg(mission_simulator_results.AOCS_Inputs.SSmeas(2).sun_angle{:,1}),'LineWidth',2)
grid on
title('CSS 2 angle 1 - deg')
xlabel('time [sec]')
ylabel('angle [deg]')
subplot 312
plot (plot_time, rad2deg(mission_simulator_results.AOCS_Inputs.SSmeas(2).sun_angle{:,2}),'LineWidth',2)
grid on
title('CSS 2 angle 2 - rad')
xlabel('time [sec]')
ylabel('angle [deg]')
subplot 313
plot (plot_time, mission_simulator_results.AOCS_Inputs.SSmeas(2).validity{:,1},'LineWidth',2)
grid on
title('CSS 2 validity')
xlabel('time [sec]')

%%%% CSS 3 angles and validity [deg]
figure('name','CSS 3 [deg]')
subplot 311
plot (plot_time, rad2deg(mission_simulator_results.AOCS_Inputs.SSmeas(3).sun_angle{:,1}),'LineWidth',2)
grid on
title('CSS 3 angle 1 - deg')
xlabel('time [sec]')
ylabel('angle [rad]')
subplot 312
plot (plot_time, rad2deg(mission_simulator_results.AOCS_Inputs.SSmeas(3).sun_angle{:,2}),'LineWidth',2)
grid on
title('CSS 3 angle 2 - deg')
xlabel('time [sec]')
subplot 313
plot (plot_time, mission_simulator_results.AOCS_Inputs.SSmeas(3).validity{:,1},'LineWidth',2)
grid on
title('CSS 3 validity')
xlabel('time [sec]')
ylabel('angle [deg]')

%%%% CSS 4 angles and validity [deg]
figure('name','CSS 4 [deg]')
subplot 311
plot (plot_time, rad2deg(mission_simulator_results.AOCS_Inputs.SSmeas(4).sun_angle{:,1}),'LineWidth',2)
grid on
title('CSS 4 angle 1 - deg')
xlabel('time [sec]')
ylabel('angle [deg]')
subplot 312
plot (plot_time, rad2deg(mission_simulator_results.AOCS_Inputs.SSmeas(4).sun_angle{:,2}),'LineWidth',2)
grid on
title('CSS 4 angle 2 - deg')
xlabel('time [sec]')
ylabel('angle [deg]')
subplot 313
plot (plot_time, mission_simulator_results.AOCS_Inputs.SSmeas(4).validity{:,1},'LineWidth',2)
grid on
title('CSS 4 validity')
xlabel('time [sec]')

%%%% CSS 5 angles and validity [deg]
figure('name','CSS 5 [deg]')
subplot 311
plot (plot_time, rad2deg(mission_simulator_results.AOCS_Inputs.SSmeas(5).sun_angle{:,1}),'LineWidth',2)
grid on
title('CSS 5 angle 1 - deg')
xlabel('time [sec]')
ylabel('angle [deg]')
subplot 312
plot (plot_time, rad2deg(mission_simulator_results.AOCS_Inputs.SSmeas(5).sun_angle{:,2}),'LineWidth',2)
grid on
title('CSS 5 angle 2 - deg')
xlabel('time [sec]')
ylabel('angle [deg]')
subplot 313
plot (plot_time, mission_simulator_results.AOCS_Inputs.SSmeas(5).validity{:,1},'LineWidth',2)
grid on
title('CSS 5 validity')
xlabel('time [sec]')

%%%% CSS 6 angles and validity [deg]
figure('name','CSS 6 [deg]')
subplot 311
plot (plot_time, rad2deg(mission_simulator_results.AOCS_Inputs.SSmeas(6).sun_angle{:,1}),'LineWidth',2)
grid on
title('CSS 6 angle 1 - deg')
xlabel('time [sec]')
ylabel('angle [deg]')
subplot 312
plot (plot_time, rad2deg(mission_simulator_results.AOCS_Inputs.SSmeas(6).sun_angle{:,2}),'LineWidth',2)
grid on
title('CSS 6 angle 2 - deg')
xlabel('time [sec]')
ylabel('angle [deg]')
subplot 313
plot (plot_time, mission_simulator_results.AOCS_Inputs.SSmeas(6).validity{:,1},'LineWidth',2)
grid on
title('CSS 6 validity')
xlabel('time [sec]')

figure('name','ICSS')
plot(mission_simulator_results.Time,mission_simulator_results.NAV.SUN_DIRECTION.icss)
grid on
xlabel('time [sec]')
title('ICSS')