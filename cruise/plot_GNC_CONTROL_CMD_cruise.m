function [] = plot_GNC_CONTROL_CMD_cruise(Cruise_debug_params)

figure('name','GNC control CMD - errors/torque/w vec/fill ratio')
subplot 321
plot(Cruise_debug_params.time,Cruise_debug_params.GNC.CONTROL_CMD.req_torque)
grid on
xlabel('time [sec]')
title('req torque')
legend('\tau_1','\tau_2','\tau_3')
hold on
subplot 322
plot(Cruise_debug_params.time,Cruise_debug_params.GNC.CONTROL_CMD.att_err,'LineWidth',2)
grid on
xlabel('time [sec]')
title('att err')
hold on
subplot 323
plot(Cruise_debug_params.time,Cruise_debug_params.GNC.CONTROL_CMD.err_vec)
grid on
xlabel('time [sec]')
title('err vec')
legend('err vec 1','err vec 2','err vec 3')
hold on
subplot 324
plot(Cruise_debug_params.time,Cruise_debug_params.GNC.CONTROL_CMD.vel_err,'LineWidth',2)
grid on
xlabel('time [sec]')
title('vel err')
hold on
subplot 325
plot(Cruise_debug_params.time,Cruise_debug_params.GNC.CONTROL_CMD.w_vec)
grid on
xlabel('time [sec]')
title('w vec')
legend('\omega_1','\omega_2','\omega_3')
hold on
subplot 326
plot(Cruise_debug_params.time,Cruise_debug_params.GNC.CONTROL_CMD.fill_ratio,'LineWidth',2)
grid on
xlabel('time [sec]')
title('fill ratio')
