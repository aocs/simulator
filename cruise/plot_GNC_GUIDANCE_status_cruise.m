function [] = plot_GNC_GUIDANCE_status_cruise(Cruise_debug_params)


figure('name','GNC Guidance status')
subplot 311
plot(Cruise_debug_params.time,Cruise_debug_params.GNC.GUIDANCE_status.apply_DV_status,'LineWidth',2)
grid on
ylim([0,3])
yticklabels({'None','DV MHT','DV AHT','DV FINAL'})
xlabel('time [sec]')
title ('apply DV status')
hold on
subplot 312
plot(Cruise_debug_params.time,Cruise_debug_params.GNC.GUIDANCE_status.FIRE,'LineWidth',2)
grid on
xlabel('time [sec]')
title ('Fire Time')
hold on
subplot 313
plot(Cruise_debug_params.time,Cruise_debug_params.GNC.GUIDANCE_status.set_Attitude_status,'LineWidth',2)
grid on
ylim([0,2])
yticklabels({'None','Out Of Band','In Band'})
xlabel('time [sec]')
title ('set attitude band')