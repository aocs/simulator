function [] = plot_IMU2_cruise(mission_simulator_results)

%%%% plot IMU 2 wb gyro
figure('name','IMU 2 Gyro')
subplot 311
plot(mission_simulator_results.Time, mission_simulator_results.AOCS_Inputs.IMUmeas2.wb_gyro{:,1})
grid on
title('IMU 2 - w_b gyro 1')
xlabel('time [sec]')
ylabel('w [deg]')
subplot 312
plot(mission_simulator_results.Time, mission_simulator_results.AOCS_Inputs.IMUmeas2.wb_gyro{:,2})
grid on
title('IMU 2 - w_b gyro 2')
xlabel('time [sec]')
ylabel('w [deg]')
subplot 313
plot(mission_simulator_results.Time, mission_simulator_results.AOCS_Inputs.IMUmeas2.wb_gyro{:,3})
grid on
title('IMU 2 - w_b gyro 3')
xlabel('time [sec]')
ylabel('w [deg]')

%%%% plot IMU 2 f accel
figure('name', 'IMU 2 F acc')
subplot 311
plot(mission_simulator_results.Time, mission_simulator_results.AOCS_Inputs.IMUmeas2.f_accel{:,1})
grid on
title('IMU 2 - f accel 1')
xlabel('time [sec]')
subplot 312
plot(mission_simulator_results.Time, mission_simulator_results.AOCS_Inputs.IMUmeas2.f_accel{:,2})
grid on
title('IMU 2 - f accel 2')
xlabel('time [sec]')
subplot 313
plot(mission_simulator_results.Time, mission_simulator_results.AOCS_Inputs.IMUmeas2.f_accel{:,3})
grid on
title('IMU 2 - f accel 3')
xlabel('time [sec]')

%%%% plot IMU 2 latency and status
figure('name','IMU 2 latency & status')
subplot 311
plot(mission_simulator_results.Time, mission_simulator_results.AOCS_Inputs.IMUmeas2.latency{:,1},'LineWidth',2)
grid on
title('IMU 2 - latency')
xlabel('time [sec]')
ylabel('time [sec]')
subplot 312
plot(mission_simulator_results.Time, mission_simulator_results.AOCS_Inputs.IMUmeas2.status_gyro{:,1},'LineWidth',2)
grid on
title('IMU 2 - gyro status')
xlabel('time [sec]')
subplot 313
plot(mission_simulator_results.Time, mission_simulator_results.AOCS_Inputs.IMUmeas2.status_acc{:,1},'LineWidth',2)
grid on
title('IMU 2 - acc status')
xlabel('time [sec]')

figure('name','IMU 2 Timetag')
plot(mission_simulator_results.Time(2:end),diff(mission_simulator_results.AOCS_Inputs.IMUmeas2.Timetag),'LineWidth',1.5)
grid on
xlabel('Time [sec]')
title('IMU 2 Timetag')