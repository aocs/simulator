function [] = plot_NAV_InertialAttitude_cruise(Cruise_debug_params)

figure('name','estimator choice')
plot(Cruise_debug_params.time,Cruise_debug_params.NAV.inertial_attitude.estimator_choice,'LineWidth',2)
xlabel('time [sec]')
ylim([0,10])
yticklabels({'None','STR1 GYRO EKF','STR2 GYRO EKF','STR1 ONLY EKF','STR1 ONLY EKF GYRO AID','STR2 ONLY EKF','STR2 ONLY EKF GYRO AID','GYRO AID ONLY','CSS ONLY EKF','REBOOT','Q REL'})
title('estimator choice')
grid on

figure('name','NAV - confidence level/STR frame')
subplot 221
plot(Cruise_debug_params.time,Cruise_debug_params.NAV.inertial_attitude.confidence_level_q,'LineWidth',2)
xlabel('time [sec]')
title('confidence level q')
grid on
hold on
subplot 222
plot(Cruise_debug_params.time,Cruise_debug_params.NAV.inertial_attitude.confidence_level_w,'LineWidth',2)
xlabel('time [sec]')
title('confidence level w')
grid on
hold on
subplot 223
plot(Cruise_debug_params.time,Cruise_debug_params.NAV.inertial_attitude.STR1_FRAME_ALG,'LineWidth',2)
ylim([0,3])
yticklabels({'None','STR GYRO EKF','STR ONLY EKF','IDLE'})
xlabel('time [sec]')
title('STR1 FRAME ALG')
grid on
hold on
subplot 224
plot(Cruise_debug_params.time,Cruise_debug_params.NAV.inertial_attitude.STR2_FRAME_ALG,'LineWidth',2)
ylim([0,3])
yticklabels({'None','STR GYRO EKF','STR ONLY EKF','IDLE'})
xlabel('time [sec]')
title('STR2 FRAME ALG')
grid on

figure('name','NAV - q I2B est/w I2B est/nav status telemetry')
subplot 311
plot(Cruise_debug_params.time,Cruise_debug_params.NAV.inertial_attitude.q_I2B_est)
xlabel('time [sec]')
legend('q_1','q_2','q_3','q_4')
title('q I2B est')
grid on
hold on
subplot 312
plot(Cruise_debug_params.time,Cruise_debug_params.NAV.inertial_attitude.w_I2B_est)
xlabel('time [sec]')
legend('w_1','w_2','w_3')
title('w I2B est')
grid on
hold on
subplot 313
plot(Cruise_debug_params.time,Cruise_debug_params.NAV.inertial_attitude.inertial_nav_status_telemetry,'LineWidth',2)
xlabel('time [sec]')
title('inertial nav status telemetry')
grid on