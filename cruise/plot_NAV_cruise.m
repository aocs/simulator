function [] = plot_NAV_cruise(Cruise_debug_params)

figure('name','NAV - SPACECRAFT PARAMMETERS')
subplot 221
plot(Cruise_debug_params.time,Cruise_debug_params.NAV.spacecraft_parameters.mass,'LineWidth',2)
xlabel('time [sec]')
title('mass')
grid on
hold on
subplot 222
plot(Cruise_debug_params.time,Cruise_debug_params.NAV.spacecraft_parameters.MassAlgorithm,'LineWidth',2)
ylim([0,3])
yticklabels({'None','TABLE','ESTIMATOR'})
xlabel('time [sec]')
title('Mass Algorithm')
grid on
hold on
subplot 223
plot(Cruise_debug_params.time,Cruise_debug_params.NAV.spacecraft_parameters.CM,'LineWidth',2)
xlabel('time [sec]')
legend('X','Y','Z')
title('CM')
grid on
hold on
subplot 224
plot(Cruise_debug_params.time,Cruise_debug_params.NAV.spacecraft_parameters.inertia,'LineWidth',2)
xlabel('time [sec]')
title('inertia')
grid on

figure('name','NAV - SPECIFIC FORCE')
subplot 211
plot(Cruise_debug_params.time,Cruise_debug_params.NAV.specific_force.algo_ID,'LineWidth',2)
xlabel('time [sec]')
title('algo ID')
grid on
hold on
subplot 212
plot(Cruise_debug_params.time,Cruise_debug_params.NAV.specific_force.fb_est)
xlabel('time [sec]')
legend('f_1','f_2','f_3')
title('fb est')
grid on

figure('name','NAV - SUN DIRECTION')
subplot 211
plot(Cruise_debug_params.time,Cruise_debug_params.NAV.sun_direction.cl_sunvec_css,'LineWidth',2)
xlabel('time [sec]')
title('cl sunvec css')
grid on
hold on
subplot 212
plot(Cruise_debug_params.time,Cruise_debug_params.NAV.sun_direction.sun_vec_b_css,'LineWidth',2)
xlabel('time [sec]')
legend('\theta_1','\theta_2','\theta_3')
title('sun vec b CSS')
grid on

figure('name','NAV - ENGINES')
subplot 211
plot(Cruise_debug_params.time,Cruise_debug_params.NAV.engines.Force_est,'LineWidth',2)
xlabel('time [sec]')
legend('F_1','F_2','F_3')
title('Force est')
grid on
hold on
subplot 212
plot(Cruise_debug_params.time,Cruise_debug_params.NAV.engines.Torque_est)
xlabel('time [sec]')
legend('\tau_1','\tau_2','\tau_3')
title('Torque est')
grid on