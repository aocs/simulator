function [] = plot_RW_cruise(mission_simulator_results)

[m,~] = size(mission_simulator_results.GNC.ENG_CMD.MHT.CMD_to_MC1{:,1});
plot_time = linspace(mission_simulator_results.Time(1),mission_simulator_results.Time(end),m);

figure('name','RW RPM meas-req')
plot(plot_time,mission_simulator_results.RW.rpm_req,'r','LineWidth',1.5)
hold on
plot(mission_simulator_results.Time,mission_simulator_results.RW.RW_meas,'b')
grid on
legend('RPM req','RPM meas')
xlabel('time [sec]')
ylabel('rpm')
title('RW RPM')