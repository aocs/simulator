function [] = plot_inertial_pointing_cruise(Cruise_debug_params,aocs_outputs)

figure('name','inertial pointing')
subplot 311
plot(Cruise_debug_params.time,Cruise_debug_params.inertial_pointing.req_z_axis,'LineWidth',2)
xlabel('time [sec]')
legend('\theta_1','\theta_2','\theta_3')
title('req z axis')
grid on
hold on
subplot 312
plot(Cruise_debug_params.time,Cruise_debug_params.inertial_pointing.axis_pointing_enable)
xlabel('time [sec]')
title('axis pointing enable')
grid on
hold on
subplot 313
plot(Cruise_debug_params.time,aocs_outputs.CMD_CRS_POINTING_MODE_CMD)
xlabel('time [sec]')
title('pointing mode CMD')
grid on

figure('name','Axis pointing status')
plot(Cruise_debug_params.time,Cruise_debug_params.inertial_pointing.axis_pointing_status,'LineWidth',2)
grid on
yticks(0:5)
yticklabels({'None','POINTING ACQUISITION','SPIN UP','FREE SPIN','FINE POINT'})
xlabel('time [sec]')
title('pointing acquisition status')