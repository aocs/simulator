clc;close all;

addpath(genpath('C:\Users\orell\Desktop\v1.2\AOCS\CodeGeneration\Simulator_log_files\cruise')) %data's directory
rmpath('C:\Users\orell\Desktop\v1.2\AOCS\CodeGeneration\Simulator_log_files\DV')
rmpath('C:\Users\orell\Desktop\v1.2\AOCS\CodeGeneration\Simulator_log_files\import data from simulator')
rmpath('C:\Users\orell\Desktop\v1.2\AOCS\CodeGeneration\Simulator_log_files\Landing')

%% extract the data from the files
aocs_spacecraft_data = readtable('aocs_spacecraft_data.csv'); %enter the aocs_spacecraft_data current name
aocs_outputs = readtable('aocs_outputs.csv'); %enter the aocs_outputs current name
aocs_inputs = readtable('aocs_inputs.csv'); %enter the aocs_inputs current name
aocs_debug = readtable('aocs_debug.csv'); %reads data from aocs_debug


%% create the data struct
mission_simulator_results = get_simulator_data(aocs_spacecraft_data,aocs_outputs,aocs_inputs,aocs_debug);
Cruise_debug_params = get_Cruise_debug_params(aocs_debug);
%% plots
% plot_engines_CMD_cruise(mission_simulator_results) %engines CMD
% plot_GNC_CONTROL_CMD_cruise(Cruise_debug_params) %GNC Control CMD
% plot_GNC_CONTROL_status_cruise(Cruise_debug_params) %GNC Control status
% plot_inertial_pointing_cruise(Cruise_debug_params,aocs_outputs) %Inertial pointing
% plot_LOGIC_events_cruise(mission_simulator_results) %Logic Events
% plot_LOGIC_modes_cruise(mission_simulator_results) %Logic Modes
% plot_NAV_cruise(Cruise_debug_params) %NAV
% plot_NAV_InertialAttitude_cruise(Cruise_debug_params) %NAV Inertial Attitude
% plot_RW_cruise(mission_simulator_results) %RW
% plot_spacecraft_orientation_cruise(mission_simulator_results) %SC orientation and daylight
% plot_spacecraft_properties_cruise(mission_simulator_results) %SC mass and sun angle
% plot_spacecraft_quat_cruise(mission_simulator_results) %SC Quat
% plot_ACS_MC_INPUT_cruise(mission_simulator_results)
% plot_TLM_events(mission_simulator_results)

%% Sensors plots
% plot_CSS_cruise(mission_simulator_results) %CSS
% plot_IMU1_cruise(mission_simulator_results) %IMU 1
% plot_IMU2_cruise(mission_simulator_results) %IMU 2
% plot_STR_cruise(mission_simulator_results) %STR