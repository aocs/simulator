%%%% run after project


Ejection_date = [2018,11,4,6,0,0]; 

JD_Ejection = juliandate(datetime(Ejection_date));

mission_time_INIT_vec_JD_Sun_Ephemeris=(ceil(JD_Ejection):(1/24):ceil(JD_Ejection+100))'; % time vector with hour increment
SunPosAOCS= planetEphemeris(mission_time_INIT_vec_JD_Sun_Ephemeris,'Earth','Sun','421')*1000; % [m]
% hours since Ejection (OBC_clock=0)
Polynom_time_vec_Hours_Sun_Ephemeris=(mission_time_INIT_vec_JD_Sun_Ephemeris-JD_Ejection)*24;
coeff_sunposx = single(polyfit(Polynom_time_vec_Hours_Sun_Ephemeris,SunPosAOCS(:,1),2));% 2 is sufficient but to make it similar to the moon computation we set it to 4 BUT since it is not working with 4 (polynomial badly conditionned) we seit it back to 2
coeff_sunposy= single(polyfit(Polynom_time_vec_Hours_Sun_Ephemeris,SunPosAOCS(:,2),2));
coeff_sunposz = single(polyfit(Polynom_time_vec_Hours_Sun_Ephemeris,SunPosAOCS(:,3),2));
add_zero=zeros(1,2,'single');% to have the coefff_sunpos of length 5 while they represent a 3rd order polynomial
coeff_sunposx=[add_zero coeff_sunposx];
coeff_sunposy=[add_zero coeff_sunposy];
coeff_sunposz=[add_zero coeff_sunposz];

num2str(coeff_sunposx)
num2str(coeff_sunposy)
num2str(coeff_sunposz)