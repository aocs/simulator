function [obc_date] = get_obc_tick(date)
%%%% change the date_convert time in the following format:
%%%%% [year, month, day, hour, minute, second]

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
date_convert = juliandate(datetime(date(1),date(2),date(3),date(4),date(5),date(6))); % date to convert

date_ejection = juliandate(datetime(2018,12,15,18,40,39.359)); %mission start time

date2obc = 3600*24*100; % date to obc coeff
obc_date = round((date_convert - date_ejection)*date2obc);
