clc;close all

addpath('C:\Users\noamle.DESKTOP-MOOJ4I4\Desktop\AOCS\Mission Simulator\Space101\AOCS\CodeGeneration\Simulator_log_files\general functions')
%% change parameters
date = [2018,12,16,23,37,12]; %input date in the following format (y,m,d,hr,min,sec)
angle = 100; %input init angle in deg

%% calculate parameters
JD_init = juliandate(datetime(date(1),date(2),date(3),date(4),date(5),date(6)));
position_Sun = planetEphemeris(JD_init,'Moon','Sun','421')*1000; % m

%% Sun position
sun_uvec_i = position_Sun/norm(position_Sun);
u3=sun_uvec_i';
u2=cross([0;0;1],u3); u2=u2/norm(u2);
u1=cross(u2,u3);
C_i2b_0=[u1,u2,u3]';
q_i2b_0=dcm2quat(C_i2b_0);
[q_err]=get_q_init_from_angle(angle);

%% Earth position - find quat from moon to earth
% position_earth = planetEphemeris(JD_init,'Moon','Earth','421')*1000; % m
% HGTA1 = [0.7313537,0.55174,0.587785]; %antenna orientation
% earth_vec_i = position_earth/norm(position_earth);
% u_earth_3 = earth_vec_i';
% u_earth_2=cross([0;0;1],u_earth_3); 
% u_earth_2=u_earth_2/norm(u_earth_2);
% u_earth_1=cross(u_earth_2,u3);
% C_earth_i2b_0=[u_earth_1,u_earth_2,u_earth_3]';
% q_earth_i2b_0=dcm2quat(C_earth_i2b_0);
% [q_earth_err]=get_q_init_from_angle(angle);
% 
% q_earth_i2b_0 = quatmultiply(q_earth_i2b_0,q_earth_err');
% 
% rot_ang = atan2(cross(earth_vec_i,HGTA1),dot(earth_vec_i,HGTA1));
% theta_x = rot_ang(1);
% theta_y = rot_ang(2);
% theta_z = rot_ang(3);
% 
% DCM_x = [1 0 0; 0 cos(theta_x) -sin(theta_x);0 sin(theta_x) cos(theta_x)];
% DCM_y = [cos(theta_y) 0 sin(theta_y); 0 1 0; -sin(theta_y) 0 cos(theta_y)];
% DCM_z = [cos(theta_z) -sin(theta_z) 0; sin(theta_z) cos(theta_z) 0; 0 0 1];
% 
% DCM_tot = DCM_x*DCM_y*DCM_z;
% q_init = dcm2quat(DCM_tot);

%% find parameters for display
q_i2b_0 = quatmultiply(q_i2b_0,q_err'); %get q_I2B 0
[obc_date] = get_obc_tick(date); % get obc tick

%% display results
disp('q I2B:');
disp(q_i2b_0);
disp('MC time tick:');
disp(num2str(obc_date));