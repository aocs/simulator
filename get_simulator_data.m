function mission_simulator_results = get_simulator_data(aocs_spacecraft_data,aocs_outputs,aocs_inputs,aocs_debug)

%%%% sort simulation times
[m,~] = size(aocs_inputs.SimTime);
mission_simulator_results.Time = zeros(m,1);
for i=1:m
    mission_simulator_results.Time(i) = i*0.02;
    mission_simulator_results.Tick_time(i) = aocs_inputs.TickNumber(end) - 2*(m-i);
end
mission_simulator_results.Tick_time = mission_simulator_results.Tick_time';

%%%% get simulation's eclipse mode
mission_simulator_results.AOCS_Inputs.MC_Inputs.daylight = aocs_inputs.MC_INPUT_DayLight;
%%%% STR 
mission_simulator_results.AOCS_Inputs.STRmeas.q_I2STR_meas = aocs_inputs(:,18:21);
mission_simulator_results.AOCS_Inputs.STRmeas.return_STR = aocs_inputs(:,26);
mission_simulator_results.AOCS_Inputs.STRmeas.epoch_time = aocs_inputs(:,28);
%%%% IMU
mission_simulator_results.AOCS_Inputs.IMUmeas1.wb_gyro = aocs_inputs(:,29:31);
mission_simulator_results.AOCS_Inputs.IMUmeas2.wb_gyro = aocs_inputs(:,40:42);
mission_simulator_results.AOCS_Inputs.IMUmeas1.f_accel = aocs_inputs(:,32:34);
mission_simulator_results.AOCS_Inputs.IMUmeas2.f_accel = aocs_inputs(:,43:45);
mission_simulator_results.AOCS_Inputs.IMUmeas1.latency = aocs_inputs(:,36);
mission_simulator_results.AOCS_Inputs.IMUmeas1.Timetag = aocs_inputs.IMU_MEAS_0__TimeTag;
mission_simulator_results.AOCS_Inputs.IMUmeas2.latency = aocs_inputs(:,47);
mission_simulator_results.AOCS_Inputs.IMUmeas1.status_acc = aocs_inputs(:,38);
mission_simulator_results.AOCS_Inputs.IMUmeas1.status_gyro = aocs_inputs(:,39);
mission_simulator_results.AOCS_Inputs.IMUmeas2.status_acc = aocs_inputs(:,49);
mission_simulator_results.AOCS_Inputs.IMUmeas2.status_gyro = aocs_inputs(:,50);
mission_simulator_results.AOCS_Inputs.IMUmeas2.Timetag = aocs_inputs.IMU_MEAS_1__TimeTag;
%%%% CSS
mission_simulator_results.AOCS_Inputs.SSmeas(1).validity = aocs_inputs(:,53);
mission_simulator_results.AOCS_Inputs.SSmeas(1).sun_angle = aocs_inputs(:,51:52);
mission_simulator_results.AOCS_Inputs.SSmeas(2).validity = aocs_inputs(:,57);
mission_simulator_results.AOCS_Inputs.SSmeas(2).sun_angle = aocs_inputs(:,55:56);
mission_simulator_results.AOCS_Inputs.SSmeas(3).validity = aocs_inputs(:,61);
mission_simulator_results.AOCS_Inputs.SSmeas(3).sun_angle = aocs_inputs(:,59:60);
mission_simulator_results.AOCS_Inputs.SSmeas(4).validity = aocs_inputs(:,65);
mission_simulator_results.AOCS_Inputs.SSmeas(4).sun_angle = aocs_inputs(:,63:64);
mission_simulator_results.AOCS_Inputs.SSmeas(5).validity = aocs_inputs(:,69);
mission_simulator_results.AOCS_Inputs.SSmeas(5).sun_angle = aocs_inputs(:,67:68);
mission_simulator_results.AOCS_Inputs.SSmeas(6).validity = aocs_inputs(:,73);
mission_simulator_results.AOCS_Inputs.SSmeas(6).sun_angle = aocs_inputs(:,71:72);
%%%% LDV
mission_simulator_results.AOCS_Inputs.LDVmeas.ds(:,1) = aocs_inputs.LDV_MEAS_ds_0_;
mission_simulator_results.AOCS_Inputs.LDVmeas.ds(:,2) = aocs_inputs.LDV_MEAS_ds_1_;
mission_simulator_results.AOCS_Inputs.LDVmeas.ds(:,3) = aocs_inputs.LDV_MEAS_ds_2_;
mission_simulator_results.AOCS_Inputs.LDVmeas.ds(:,4) = aocs_inputs.LDV_MEAS_ds_3_;
mission_simulator_results.AOCS_Inputs.LDVmeas.r(:,1) = aocs_inputs.LDV_MEAS_r_0_;
mission_simulator_results.AOCS_Inputs.LDVmeas.r(:,2) = aocs_inputs.LDV_MEAS_r_1_;
mission_simulator_results.AOCS_Inputs.LDVmeas.r(:,3) = aocs_inputs.LDV_MEAS_r_2_;
mission_simulator_results.AOCS_Inputs.LDVmeas.r(:,4) = aocs_inputs.LDV_MEAS_r_3_;
mission_simulator_results.AOCS_Inputs.LDVmeas.snr_r_ds(:,1) = aocs_inputs.LDV_MEAS_snr_r_ds_0_;
mission_simulator_results.AOCS_Inputs.LDVmeas.snr_r_ds(:,2) = aocs_inputs.LDV_MEAS_snr_r_ds_1_;
mission_simulator_results.AOCS_Inputs.LDVmeas.snr_r_ds(:,3) = aocs_inputs.LDV_MEAS_snr_r_ds_2_;
mission_simulator_results.AOCS_Inputs.LDVmeas.snr_r_ds(:,4) = aocs_inputs.LDV_MEAS_snr_r_ds_3_;
mission_simulator_results.AOCS_Inputs.LDVmeas.snr_r_ds(:,5) = aocs_inputs.LDV_MEAS_snr_r_ds_4_;
mission_simulator_results.AOCS_Inputs.LDVmeas.snr_r_ds(:,6) = aocs_inputs.LDV_MEAS_snr_r_ds_5_;
mission_simulator_results.AOCS_Inputs.LDVmeas.snr_r_ds(:,7) = aocs_inputs.LDV_MEAS_snr_r_ds_6_;
mission_simulator_results.AOCS_Inputs.LDVmeas.snr_r_ds(:,8) = aocs_inputs.LDV_MEAS_snr_r_ds_7_;
mission_simulator_results.AOCS_Inputs.LDVmeas.Validity = aocs_inputs.Validity_ldv;
%%%% engines
mission_simulator_results.GNC.ENG_CMD.MHT.CMD_to_MC0 = aocs_outputs(:,21);
mission_simulator_results.GNC.ENG_CMD.MHT.CMD_to_MC1 = aocs_outputs(:,22);
mission_simulator_results.GNC.ENG_CMD.ACS.ACS(1).CMD_to_MC0 = aocs_outputs(:,23);
mission_simulator_results.GNC.ENG_CMD.ACS.ACS(1).CMD_to_MC1 = aocs_outputs(:,31);
mission_simulator_results.GNC.ENG_CMD.ACS.ACS(2).CMD_to_MC0 = aocs_outputs(:,24);
mission_simulator_results.GNC.ENG_CMD.ACS.ACS(2).CMD_to_MC1 = aocs_outputs(:,32);
mission_simulator_results.GNC.ENG_CMD.ACS.ACS(3).CMD_to_MC0 = aocs_outputs(:,25);
mission_simulator_results.GNC.ENG_CMD.ACS.ACS(3).CMD_to_MC1 = aocs_outputs(:,33);
mission_simulator_results.GNC.ENG_CMD.ACS.ACS(4).CMD_to_MC0 = aocs_outputs(:,26);
mission_simulator_results.GNC.ENG_CMD.ACS.ACS(4).CMD_to_MC1 = aocs_outputs(:,34);
mission_simulator_results.GNC.ENG_CMD.ACS.ACS(5).CMD_to_MC0 = aocs_outputs(:,27);
mission_simulator_results.GNC.ENG_CMD.ACS.ACS(5).CMD_to_MC1 = aocs_outputs(:,35);
mission_simulator_results.GNC.ENG_CMD.ACS.ACS(6).CMD_to_MC0 = aocs_outputs(:,28);
mission_simulator_results.GNC.ENG_CMD.ACS.ACS(6).CMD_to_MC1 = aocs_outputs(:,36);
mission_simulator_results.GNC.ENG_CMD.ACS.ACS(7).CMD_to_MC0 = aocs_outputs(:,29);
mission_simulator_results.GNC.ENG_CMD.ACS.ACS(7).CMD_to_MC1 = aocs_outputs(:,37);
mission_simulator_results.GNC.ENG_CMD.ACS.ACS(8).CMD_to_MC0 = aocs_outputs(:,30);
mission_simulator_results.GNC.ENG_CMD.ACS.ACS(8).CMD_to_MC1 = aocs_outputs(:,38);
%%%% inertial attitude from the simulator
mission_simulator_results.Inertial_attitude.location.X = aocs_spacecraft_data(:,11);
mission_simulator_results.Inertial_attitude.location.Y = aocs_spacecraft_data(:,12);
mission_simulator_results.Inertial_attitude.location.Z = aocs_spacecraft_data(:,13);
mission_simulator_results.Inertial_attitude.velocity.Vx = aocs_spacecraft_data(:,14);
mission_simulator_results.Inertial_attitude.velocity.Vy = aocs_spacecraft_data(:,15);
mission_simulator_results.Inertial_attitude.velocity.Vz = aocs_spacecraft_data(:,16);
mission_simulator_results.Inertial_attitude.omega.omega_x = aocs_spacecraft_data(:,21);
mission_simulator_results.Inertial_attitude.omega.omega_y = aocs_spacecraft_data(:,22);
mission_simulator_results.Inertial_attitude.omega.omega_z = aocs_spacecraft_data(:,23);
mission_simulator_results.Inertial_attitude.mass = aocs_spacecraft_data(:,10);
mission_simulator_results.Inertial_attitude.sun_long = aocs_spacecraft_data(:,28);
mission_simulator_results.Inertial_attitude.sun_col = aocs_spacecraft_data(:,27);
mission_simulator_results.Inertial_attitude.q(:,1) = aocs_spacecraft_data.Q1;
mission_simulator_results.Inertial_attitude.q(:,2) = aocs_spacecraft_data.Q2;
mission_simulator_results.Inertial_attitude.q(:,3) = aocs_spacecraft_data.Q3;
mission_simulator_results.Inertial_attitude.q(:,4) = aocs_spacecraft_data.Q4;

%%%% Modes
mission_simulator_results.LOGIC.modes.AOCS_main_mode = aocs_outputs(:,3);
mission_simulator_results.LOGIC.modes.landing_mode = aocs_outputs(:,4);
mission_simulator_results.LOGIC.modes.landing_orientation_mode = aocs_outputs(:,5);
mission_simulator_results.LOGIC.modes.DV_mode = aocs_outputs(:,6);
mission_simulator_results.LOGIC.modes.DV_orientation_mode = aocs_outputs(:,7);
mission_simulator_results.LOGIC.modes.cruise_mode = aocs_outputs(:,8);
mission_simulator_results.LOGIC.modes.cruise_calibration_mode = aocs_outputs(:,10);
mission_simulator_results.LOGIC.modes.SP_mode = aocs_outputs(:,9);
mission_simulator_results.LOGIC.modes.HOP_mode = aocs_outputs(:,11);
mission_simulator_results.LOGIC.modes.surface_mode = aocs_outputs(:,12);
mission_simulator_results.LOGIC.modes.surface_calibration_mode = aocs_outputs(:,13);
mission_simulator_results.LOGIC.modes.safe_pointing_mode = aocs_outputs(:,14);
mission_simulator_results.LOGIC.events.telemetry = aocs_outputs(:,15);
mission_simulator_results.LOGIC.events.NAV_CMD_telemetry = aocs_outputs(:,16);
mission_simulator_results.LOGIC.events.AOCS_logic_events = aocs_outputs(:,17);
mission_simulator_results.LOGIC.events.telemetry_events_0 = aocs_outputs(:,18);
mission_simulator_results.LOGIC.events.telemetry_events_1 = aocs_outputs(:,19);

mission_simulator_results.LOGIC.modes.SUN_POINTING_MODES = aocs_debug.SUN_POINTING_MODES;
mission_simulator_results.LOGIC.modes.SAFE_POINTING_MODES = aocs_debug.SAFE_POINTING_MODE;
mission_simulator_results.LOGIC.modes.INERTIAL_POINTING_MODES = aocs_debug.INERTIAL_POINTING_MODE;
mission_simulator_results.LOGIC.modes.LOST_MODES = aocs_debug.LOST_MODE;

%%%% Modified Julian Date and gregorian date
mission_simulator_results.initial_conditions.MJD = aocs_spacecraft_data(1,3);
mission_simulator_results.initial_conditions.date = aocs_spacecraft_data(1,4:9);

%%%% RW
mission_simulator_results.RW.rpm_req = aocs_outputs.mw_rpm_req;
mission_simulator_results.RW.PNT_NOM_free_spin_mw_rpm_req_high = aocs_outputs.CMD_PNT_NOM_free_spin_mw_rpm_req_high;
mission_simulator_results.RW.PNT_NOM_free_spin_mw_rpm_req_low = aocs_outputs.CMD_PNT_NOM_free_spin_mw_rpm_req_low;
mission_simulator_results.RW.CMD_PNT_NOM_pointing_acquisition_mw_rpm_req = aocs_outputs.CMD_PNT_NOM_pointing_acquisition_mw_rpm_req;
mission_simulator_results.RW.RW_meas = aocs_inputs.MW_meas_w_mw_rpm;
mission_simulator_results.RW.status = aocs_inputs.MW_meas_status_mw;
mission_simulator_results.RW.TimeTag = aocs_inputs.MW_meas_TimeTag;

%%%% MC INPUT
mission_simulator_results.MC_INPUT.MC_ACS_LOGIC_CMD.ACS_PERMIT = aocs_inputs.ACS_PERMIT;
mission_simulator_results.MC_INPUT.MC_ACS_LOGIC_CMD.MHT_AVILABLE = aocs_inputs.MHT_AVAILABLE;
mission_simulator_results.MC_INPUT.MC_ACS_LOGIC_CMD.PROCEED_WITH_HT_ONLY = aocs_inputs.PROCEED_WITH_HT_ONLY_FLAG;

%%%% NAV
mission_simulator_results.NAV.SENSORS.SENSORS_EVENTS.logic_event_STR1_failed = aocs_outputs.logic_event_str1_failed;
mission_simulator_results.NAV.SENSORS.SENSORS_EVENTS.logic_event_STR2_failed = aocs_outputs.logic_event_str2_failed;
mission_simulator_results.NAV.SENSORS.SENSORS_EVENTS.logic_event_IMU1_failed = aocs_outputs.logic_event_IMU1_failed;
mission_simulator_results.NAV.SENSORS.SENSORS_EVENTS.logic_event_IMU1_failed = aocs_outputs.logic_event_IMU2_failed;
mission_simulator_results.NAV.SUN_DIRECTION.icss = aocs_outputs.icss;