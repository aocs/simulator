clc;

FDS_DV;
% 
% % orbit_case = 'Earth';
% orbit_case = 'Moon';
% 
% seconds_from_eject = OBC_clock_at_sim_init/100;
% ejecetion_array = datevec(Ejection_Time);
% 
% 
% %%%% get initial conditions for simulator from matlab
% simulator_init.X = XI0; %simulator location
% simulator_init.V = VI0; %simulator velocity
% 
% simulator_init.Q_I2B = q_i2b_0; %simulator quaternion
% simulator_init.W_I2B = w_i2b_b_0; %simulator w I2B
% 
% simulator_init.OBC = OBC_clock_at_sim_init; %init obc clock
% simulator_init.fuel_ratio = fuel_ratio_0; % init fuel ratio
% 
% simulator_init.date = datetime(ejecetion_array + [0,0,0,0,0,seconds_from_eject]);