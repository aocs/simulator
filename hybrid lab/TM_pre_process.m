function [TM_analysis] = TM_pre_process()

addpath('C:\Users\orell\Desktop\v1.2\AOCS\CodeGeneration\Simulator_log_files\hybrid lab\TLM analysis\check results') %results directory
TM = readtable('AOCS TM.csv'); %get TM results
%%%%% get a large file that cannot be converted to CSV

SDB = readtable('SDB for matlab.csv'); %get data from SDB
TM_events = readtable('events legend.csv'); %get TM events legend
%% sort data from TM
[TM_analysis,obc_set_clk] = get_events_from_tlm(TM,TM_events);

%%
TM_renamed = TM(:,1:4);
SDB_ID = SDB.fullID; %SDB ID for comparison with TM
SDB_name = SDB.Prm_Name_50_; %names in SDB

[C,~,ic] = unique(TM.PrmID);

for i=1:numel(C)
    
    T_raw = TM.TickTime(ic==i) - obc_set_clk;
    T = T_raw(T_raw >= 0);
    D = TM.PrmValue(ic==i);
    D = D(T_raw >= 0);
    
    ind_ParName = cell2mat(cellfun(@(x)strcmp(C{i},x),SDB_ID,'UniformOutput',false));
    TM_analysis.(SDB_name{ind_ParName}).Time = T/100;
    TM_analysis.(SDB_name{ind_ParName}).Data = D;
    
end

%% times and save mat file
TM_analysis.tick = linspace(TM_renamed.TickTime(1),TM_renamed.TickTime(end)); %tick count 10[Hz]
TM_analysis.time = linspace(0,TM_analysis.AOCS_MODES_TELEMETRY.Time(end),numel(TM_analysis.AOCS_MODES_TELEMETRY.Time)); %time [sec]

save('TM_analysis.mat','TM_analysis');