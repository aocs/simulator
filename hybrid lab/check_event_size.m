function [TM_analysis] = check_event_size(TM_analysis)

[data_size1,~] = size(TM_analysis.TMP_EVENT1_TIME.Data);
[time_size1,~] = size(TM_analysis.TMP_EVENT1_CODE.Data);
[data_size2,~] = size(TM_analysis.TMP_EVENT2_TIME.Data);
[time_size2,~] = size(TM_analysis.TMP_EVENT2_CODE.Data);
[data_size3,~] = size(TM_analysis.TMP_EVENT3_TIME.Data);
[time_size3,~] = size(TM_analysis.TMP_EVENT3_CODE.Data);
[data_size4,~] = size(TM_analysis.TMP_EVENT4_TIME.Data);
[time_size4,~] = size(TM_analysis.TMP_EVENT4_CODE.Data);

if(data_size1>time_size1)
    TM_analysis.TMP_EVENT1_TIME.Data(end+1) = TM_analysis.TMP_EVENT1_CODE.Time*100;
end
    
if(data_size2>time_size2)
    TM_analysis.TMP_EVENT2_TIME.Data(end+1) = TM_analysis.TMP_EVENT2_CODE.Time*100;
end

if(data_size3>time_size3)
    TM_analysis.TMP_EVENT3_TIME.Data(end+1) = TM_analysis.TMP_EVENT3_CODE.Time*100;
end

if(data_size4>time_size4)
    TM_analysis.TMP_EVENT4_TIME.Data(end+1) = TM_analysis.TMP_EVENT4_CODE.Time*100;
end