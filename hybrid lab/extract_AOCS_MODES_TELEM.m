function [modes,fds_index,NAV_SF,NAV_SC]=extract_AOCS_MODES_TELEM(tlm)

% modes
 modes_table = {'DV.NAV_INIT';                %1
                'DV.COARSE_ATT';              %2
                'DV.FINE_ATT';                %3
                'DV.FIRE';                    %4
                'DV.NAV_FAIL';                %5
                'DV.DV_DONE';                 %6
                '';                           %7
                'LAND.NAV_INIT';              %8
                'LAND.COARSE_ATT';            %9
                'LAND.FINE_ATT';              %10
                'LAND.DESCENT';               %11
                'LAND.ENGCUTOFF';             %12
                'LAND.TOUCHDOWN';             %13
                '';                           %14
                '';                           %15
                '';                           %16
                'SURF.WAIT';                  %17
                'SURF.ACTIVE';                %18
                'SURF.PRE_CAL';              %19
                'SURF.Calib';                 %20
                'SURF.POST_CAL';             %21
                '';                           %22
                'STBY.NOMINAL';               %23
                'STBY.PRE_CAL';              %24
                'STBY.CALIB';                 %25
                'STBY.POST_CAL';             %26
                '';                           %27
                'CRS.SP.NAV_INIT';            %28
                'CRS.SP.ACTIVE';              %29
                'CRS.SP.NAV_FAIL';            %30
                'CRS.SAFE.NAV_INIT';          %31
                'CRS.SAFE.Active';            %32
                'CRS.SAFE.NAV_FAIL';          %33
                'CRS.IP.NAV_INIT';            %34
                'CRS.IP.ACTIVE';              %35
                'CRS.IP.NAV_FAIL';            %36
                'CRS.ECLIPSE';                %37
                'CRS.LOST.STOP';              %38
                'CRS.LOST.WAIT';              %39
                'CRS.LOST.TURN';              %40
                'CRS.LOST.TOTAL';             %41
                'CRS.PRE_CAL';                %42
                'CRS.CALIB';                  %43
                'CRS.POST_CAL';               %44
                '';                           %45
                'HOP.NAV_INIT';               %46
                'HOP.HOP';                    %47
                'HOP.ENGCUTOFF';              %48
                'HOP.TOUCHDOWN' };            %49
 mode_u = sum(bitset(uint32(0),find(bitget(tlm,1:6,'uint32')>0)));
 
 if mode_u>0
    modes = modes_table{mode_u}; 
 else
    modes= 'IDLE';
 end
 
 % fds_index
 
 fds_index = sum(bitset(uint32(0),find(bitget(tlm,7:11,'uint32')>0)));

 algo_ID = sum(bitset(uint32(0),find(bitget(tlm,12:14,'uint32')>0)));
switch algo_ID
    case 1
        NAV_SF.Alg = 'NOMINAL';
    case 2
        NAV_SF.Alg = 'PARTIAL';

    case 3
        NAV_SF.Alg = 'ENGINE';

    otherwise
end
NAV_SF.cl_specforce=sum(bitset(uint32(0),find(bitget(tlm,15:16,'uint32')>0)));
NAV_SF.bias1_cl = bitget(tlm,17,'uint32');
NAV_SF.bias2_cl = bitget(tlm,18,'uint32');
NAV_SF.count1_notuse_mod16 = sum(bitset(uint32(0),find(bitget(tlm,19:22,'uint32')>0)));
NAV_SF.count2_notuse_mod16 = sum(bitset(uint32(0),find(bitget(tlm,23:26,'uint32')>0)));

%NAV.SpacecraftParams
NAV_SC.Alg = char(MASS_ESTIMATE_ALGORITHM_E(sum(bitset(uint32(0),find(bitget(tlm,27:28,'uint32')>0)))));
NAV_SC.MassEstMode = char(MASS_ESTIMATOR_MODES_E(sum(bitset(uint32(0),find(bitget(tlm,29:30,'uint32')>0)))));
NAV_SC.IsValidTable = bitget(tlm,31,'uint32');


