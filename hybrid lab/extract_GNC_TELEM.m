function [gnc_mode,controller,pwm]=extract_GNC_TELEM(tlm)

% modes
gnc_table = {'POINT.ACQUISITION';     %1
    'POINT.SPIN_UP';                  %2
    'POINT.FREE_SPIN';                %3
    'POINT.FINE';                     %4
    'SET_ATT.OUT_BAND';               %5
    'SET_ATT.IN_BAND';                %6
    'DV.AHT';                         %7
    'DV.MHT';                         %8
    'DV.FINAL';                       %9
    'DESCENT.BREAK0';                 %10
    'DESCENT.BREAK1';                 %11
    'DESCENT.BREAK2.OMPS_NOT_VALID';  %12
    'DESCENT.BREAK2.OMPS_VALID';      %13
    'DESCENT.BREAK3';                 %14
    'DESCENT.VD.START_MHT_OFF';       %15          
    'DESCENT.VD.START_MHT_ON';        %16                  
    'DESCENT.VD.MIN_ON';              %17
    'DESCENT.VD.NOMINAL';             %18
    'DESCENT.VD.MAX_ON';              %19
    'DESCENT.VD.ACS_FALL';            %20
    'DESCENT.VD.FREE_FALL';           %21
    'HOP.BLAST_OFF';                  %22
    'HOP.ACCELERATION';               %23
    'HOP.FREE_FALL';                  %24
    'HOP.DECELERATION';               %25
    'HOP.VD.MHT_OFF';                 %26
    'HOP.VD.MIN_ON';                  %27
    'HOP.VD.NOMINAL';                 %28
    'HOP.VD.MAX_ON';                  %29
    'HOP.VD.ACS_FALL';                %30
    'HOP.VD.FREE_FALL'};              %31

controller_modes = {'PID','PD_LIMITER', 'FINE'};
pwm_modes = {'ACS1','ACS2', 'ACS_FULL'};
if ~isempty(find(bitget(tlm,1:5,'uint32')>0))
    gnc_mode = gnc_table{sum(bitset(uint32(0),find(bitget(tlm,1:5,'uint32')>0)))};
else
    gnc_mode = 'None';
end

if ~isempty(find(bitget(tlm,7:8,'uint32')>0))
    controller = controller_modes{sum(bitset(uint32(0),find(bitget(tlm,7:8,'uint32')>0)))};
else
    controller = 'Null';
end
if ~isempty(find(bitget(tlm,9:10,'uint32')>0))    
    pwm = pwm_modes{sum(bitset(uint32(0),find(bitget(tlm,9:10,'uint32')>0)))};
else
    pwm = 'None';
end
