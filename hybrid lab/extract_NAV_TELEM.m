function [Inertial,SunDir]=extract_NAV_TELEM(tlm)

Inertial.estimator_choice = INNER_ATT_EST_CHOICE_E(sum(bitset(uint32(0),find(bitget(tlm,1:4,'uint32')>0))));
Inertial.STR1_frame = STR_FRAME_ALGORITHM_E(sum(bitset(uint32(0),find(bitget(tlm,5:6,'uint32')>0))));
Inertial.STR2_frame = STR_FRAME_ALGORITHM_E(sum(bitset(uint32(0),find(bitget(tlm,7:8,'uint32')>0))));
Inertial.IsRebootMode = bitget(tlm,9,'uint32')>0;
STR1_GYRO_EKF_mode_b = sum(bitset(uint32(0),find(bitget(tlm,10:11,'uint32')>0)));
% STR1_GYRO
if STR1_GYRO_EKF_mode_b == uint32(1)
    Inertial.STR1_GYRO_EKF_mode = 'EKF';
elseif STR1_GYRO_EKF_mode_b == uint32(2)
    Inertial.STR1_GYRO_EKF_mode = 'Simple';
else
    Inertial.STR1_GYRO_EKF_mode = 'None';
end
Inertial.STR1_GYRO_KF_active = bitget(tlm,12,'uint32')>0;
Inertial.STR1_GYRO_Confidence_q = sum(bitset(uint32(0),find(bitget(tlm,13:14,'uint32')>0)));
if Inertial.STR1_GYRO_Confidence_q > 0
    Inertial.STR1_GYRO_Confidence_q = Inertial.STR1_GYRO_Confidence_q+1;
end
Inertial.STR1_GYRO_Confidence_w = sum(bitset(uint32(0),find(bitget(tlm,15:16,'uint32')>0)));
% STR2_GYRO
STR2_GYRO_EKF_mode_b = sum(bitset(uint32(0),find(bitget(tlm,17:18,'uint32')>0)));

if STR2_GYRO_EKF_mode_b == uint32(1)
    Inertial.STR2_GYRO_EKF_mode = 'EKF';
elseif STR2_GYRO_EKF_mode_b == uint32(2)
    Inertial.STR2_GYRO_EKF_mode = 'Simple';
else
    Inertial.STR2_GYRO_EKF_mode = 'None';
end
Inertial.STR2_GYRO_KF_active = bitget(tlm,19,'uint32')>0;
Inertial.STR2_GYRO_Confidence_q = sum(bitset(uint32(0),find(bitget(tlm,20:21,'uint32')>0)));

if Inertial.STR2_GYRO_Confidence_q > 0
    Inertial.STR2_GYRO_Confidence_q = Inertial.STR2_GYRO_Confidence_q+1;
end
Inertial.STR2_GYRO_Confidence_w = sum(bitset(uint32(0),find(bitget(tlm,22:23,'uint32')>0)));


Inertial.STR1_ONLY_Confidence_w = bitget(tlm,24,'uint32');
Inertial.STR2_ONLY_Confidence_w = bitget(tlm,25,'uint32');
Inertial.CSS_ONLY_Confidence_w = bitget(tlm,26,'uint32');

SunDir.cl_sunvecb_css = bitget(tlm,27,'uint32');
SunDir.cl_sunvecb_str = bitget(tlm,28,'uint32');
SunDir_alg = bitget(tlm,29,'uint32');
if SunDir_alg == uint32(0)
   SunDir.alg = 'CSS';
else
   SunDir.alg = 'STR';

end
SunDir.icss = sum(bitset(uint32(0),find(bitget(tlm,30:32,'uint32')>0)));