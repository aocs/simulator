function [AOCS_Modes_TM_results,GNC_Status_TM_results,INNERTIAL_NAV_TM_results] = get_AOCS_tlm(TM_analysis)


counter_size = min([numel(TM_analysis.AOCS_MODES_TELEMETRY.Data),numel(TM_analysis.GNC_STATUS_TELEMETRY.Data),numel(TM_analysis.INNERTIAL_NAV_STATUS_TELEMETRY.Data)]);
AOCS_modes_times = zeros(1,counter_size);
GNC_status_times = zeros(1,counter_size);
innertial_nav_times = zeros(1,counter_size);

for i=1:counter_size
    
    [modes,fds_index,NAV_SF,NAV_SC]=extract_AOCS_MODES_TELEM(uint32(TM_analysis.AOCS_MODES_TELEMETRY.Data(i)));
    AOCS_modes_times(i) = TM_analysis.AOCS_MODES_TELEMETRY.Time(i);
    [gnc_mode,controller,pwm]=extract_GNC_TELEM(TM_analysis.GNC_STATUS_TELEMETRY.Data(i));
    GNC_status_times(i) = TM_analysis.GNC_STATUS_TELEMETRY.Time(i);
    [Inertial,SunDir]=extract_NAV_TELEM(TM_analysis.INNERTIAL_NAV_STATUS_TELEMETRY.Data(i));
    innertial_nav_times(i) = TM_analysis.INNERTIAL_NAV_STATUS_TELEMETRY.Time(i);
    
    AOCS_Modes_TM_results.modes{i} = modes;
    AOCS_Modes_TM_results.fds_index(i) = fds_index;
    AOCS_Modes_TM_results.NAV_SC.Alg{i} = NAV_SC.Alg;
    AOCS_Modes_TM_results.NAV_SC.IsValidTable(i) = NAV_SC.IsValidTable;
    AOCS_Modes_TM_results.NAV_SC.MassEstMode{i} = NAV_SC.MassEstMode;
    AOCS_Modes_TM_results.NAV_SF.Alg{i} = NAV_SF.Alg;
    AOCS_Modes_TM_results.NAV_SF.bias1_cl(i) = NAV_SF.bias1_cl;
    AOCS_Modes_TM_results.NAV_SF.bias2_cl(i) = NAV_SF.bias2_cl;
    AOCS_Modes_TM_results.NAV_SF.cl_specforce(i) = NAV_SF.cl_specforce;
    AOCS_Modes_TM_results.NAV_SF.count1_notuse_mod16(i) = NAV_SF.count1_notuse_mod16;
    AOCS_Modes_TM_results.NAV_SF.count2_notuse_mod16(i) = NAV_SF.count2_notuse_mod16;
    
    GNC_Status_TM_results.gnc_mode{i} = gnc_mode;
    GNC_Status_TM_results.controller{i} = controller;
    GNC_Status_TM_results.pwm{i} = pwm;
    
    INNERTIAL_NAV_TM_results.Inertial.CSS_ONLY_Confidence_w(i) = Inertial.CSS_ONLY_Confidence_w;
    INNERTIAL_NAV_TM_results.Inertial.estimator_choice{i} = char(Inertial.estimator_choice);
    INNERTIAL_NAV_TM_results.Inertial.IsRebootMode(i) = Inertial.IsRebootMode;
    INNERTIAL_NAV_TM_results.Inertial.STR1_frame{i} = char(Inertial.STR1_frame);
    INNERTIAL_NAV_TM_results.Inertial.STR1_GYRO_Confidence_q(i) = Inertial.STR1_GYRO_Confidence_q;
    INNERTIAL_NAV_TM_results.Inertial.STR1_GYRO_Confidence_w(i) = Inertial.STR1_GYRO_Confidence_w;
    INNERTIAL_NAV_TM_results.Inertial.STR1_GYRO_EKF_mode{i} = Inertial.STR1_GYRO_EKF_mode;
    INNERTIAL_NAV_TM_results.Inertial.STR1_GYRO_KF_active(i) = Inertial.STR1_GYRO_KF_active;
    INNERTIAL_NAV_TM_results.Inertial.STR1_ONLY_Confidence_w(i) = Inertial.STR1_ONLY_Confidence_w;
    INNERTIAL_NAV_TM_results.Inertial.STR2_frame{i} = char(Inertial.STR2_frame);
    INNERTIAL_NAV_TM_results.Inertial.STR2_GYRO_Confidence_q(i) = Inertial.STR2_GYRO_Confidence_q;
    INNERTIAL_NAV_TM_results.Inertial.STR2_GYRO_Confidence_w(i) = Inertial.STR2_GYRO_Confidence_w;
    INNERTIAL_NAV_TM_results.Inertial.STR2_GYRO_EKF_mode{i} = Inertial.STR2_GYRO_EKF_mode;
    INNERTIAL_NAV_TM_results.Inertial.STR2_GYRO_KF_active(i) = Inertial.STR2_GYRO_KF_active;
    INNERTIAL_NAV_TM_results.Inertial.STR2_ONLY_Confidence_w(i) = Inertial.STR2_ONLY_Confidence_w;
    
    INNERTIAL_NAV_TM_results.SunDir.alg{i} = SunDir.alg;
    INNERTIAL_NAV_TM_results.SunDir.cl_sunvecb_css(i) = SunDir.cl_sunvecb_css;
    INNERTIAL_NAV_TM_results.SunDir.cl_sunvecb_str(i) = SunDir.cl_sunvecb_str;
    INNERTIAL_NAV_TM_results.SunDir.icss(i) = SunDir.icss;
    
end
AOCS_Modes_TM_results.times = AOCS_modes_times;
GNC_Status_TM_results.times = GNC_status_times;
INNERTIAL_NAV_TM_results.times = innertial_nav_times;