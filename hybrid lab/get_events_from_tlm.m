function [TM_analysis,obc_set_clk] = get_events_from_tlm(TM,TM_events)


events_code_1_ind = find(strcmp(TM.PrmID,'W382'));
events_code_2_ind = find(strcmp(TM.PrmID,'W385'));
events_code_3_ind = find(strcmp(TM.PrmID,'W388'));
events_code_4_ind = find(strcmp(TM.PrmID,'W391'));

all_events_ind = [events_code_1_ind;events_code_2_ind;events_code_3_ind;events_code_4_ind];

EVENTS_raw = TM(all_events_ind,:);

set_clk_event_ID = 12;

%% get obc after set clock

ind_set_clk = find(EVENTS_raw.PrmValue == set_clk_event_ID);

if isempty(ind_set_clk)
    obc_set_clk = 0;
else
    obc_set_clk = EVENTS_raw.TickTime(ind_set_clk);
end
%% get needed events and translate
j=1;

for i=1:numel(all_events_ind)
    
   if EVENTS_raw.TickTime(i) >= obc_set_clk
       filtered_events(j,:) = EVENTS_raw(i,:);
       j = j+1;
   end
    
end

events_ID_dec = cellfun(@(x)hex2dec(x(3:6)),TM_events.Code); %get decimal events ID
[C_events,~,ic_events] = unique(filtered_events.PrmValue);

EVENTS_TABLE = cell(numel(C_events),2);

for k=1:numel(C_events)
    
    %%%%
%     EVENTS_TABLE{k} = filtered_events(ic_events==k,:);
    %%%%
    ind_params = find(C_events(k) == filtered_events.PrmValue);
    ind_event = find(C_events(k) == events_ID_dec);
    if isempty(ind_event)
        all_events.Data(ind_params) = {'unknown'};
        all_events.Time(ind_params) = filtered_events.TickTime(ind_params);
    elseif isequal(TM_events.isNeeded{ind_event},'needed')
        all_events.Data(ind_params) = TM_events.Name(ind_event);
        all_events.Time(ind_params) = filtered_events.TickTime(ind_params);
        indx_event = find(ic_events==k);
        EVENTS_TABLE{k,1} = all_events.Data(indx_event(1));
        EVENTS_TABLE{k,2} = [all_events.Data(indx_event);num2cell(all_events.Time(indx_event)/100)];
    else
        all_events.Data(ind_params) = {'Not needed'};
        all_events.Time(ind_params) = filtered_events.TickTime(ind_params);
    end
    
end

needed_index = find(cellfun(@(x)~isequal(x,'Not needed') & ~isequal(x,'unknown'),all_events.Data));
TM_analysis.needed_events.Time = (all_events.Time(needed_index) - obc_set_clk)/100;
TM_analysis.needed_events.Data = all_events.Data(needed_index);
TM_analysis.needed_events.Data = TM_analysis.needed_events.Data';
TM_analysis.events_table = EVENTS_TABLE;