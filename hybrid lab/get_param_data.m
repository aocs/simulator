
function P = get_param_data(TM,SDB)

%%

[IDS,INDXS] = sort(TM.PrmID);
[~,indx_init_param] = unique(IDS);
N_params = length(indx_init_param);

times_day = datenum(TM.Time(INDXS));
init_time_day = min(times_day);

RawValue = TM.RawValue(INDXS);

day2sec = 24*3600;
%%

PS = cell(1,N_params);

for i=1:N_params
    
    i1 = indx_init_param(i);
    
    if i<N_params
        i2 = indx_init_param(i+1)-1;
        P.times = (times_day(i1:i2)-init_time_day)*day2sec;
        P.vals = RawValue(i1:i2);
    else        
        P.times = (times_day(i1:end)-init_time_day)*day2sec;
        P.vals = RawValue(i1:end);
    end
    
    P.name=[];
    for k=1:numel(SDB.fullID(:))
        if isequal(SDB.fullID{k},IDS{i1})
            P.name = SDB.Prm_Name_50_(k);
            break
        end
    end
    
    PS{i} = P;
    
end
