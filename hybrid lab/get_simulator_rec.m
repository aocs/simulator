clc;

dataArray = readtable('Hybrid Lab Results.csv');

%% Create output variable
simulator_rec = table;
simulator_rec.Time = dataArray{:, 1};
simulator_rec.W_x = dataArray{:, 2};
simulator_rec.W_y = dataArray{:, 3};
simulator_rec.W_z = dataArray{:, 4};
simulator_rec.q_2 = dataArray{:, 5};
simulator_rec.q_3 = dataArray{:, 6};
simulator_rec.q_4 = dataArray{:, 7};
simulator_rec.q_1 = dataArray{:, 8};
simulator_rec.X = dataArray{:, 9};
simulator_rec.Y = dataArray{:, 10};
simulator_rec.Z = dataArray{:, 11};
simulator_rec.Vx = dataArray{:, 12};
simulator_rec.Vy = dataArray{:, 13};
simulator_rec.Vz = dataArray{:, 14};
simulator_rec.sun_colatitude = dataArray{:, 15};
simulator_rec.IMU1_Wx = dataArray{:, 16};
simulator_rec.IMU1_Wy = dataArray{:, 17};
simulator_rec.IMU1_Wz = dataArray{:, 18};
simulator_rec.IMU2_Wx = dataArray{:, 19};
simulator_rec.IMU2_Wy = dataArray{:, 20};
simulator_rec.IMU2_Wz = dataArray{:, 21};
simulator_rec.STR1_validity = dataArray{:, 22};
simulator_rec.STR2_validity = dataArray{:, 23};
simulator_rec.STR1_epoch_time = dataArray{:, 24};
simulator_rec.STR2_epoch_time = dataArray{:, 25};

%% plots

