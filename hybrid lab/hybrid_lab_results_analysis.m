clc;clear all;close all

AOCS_enumerations;
%% create TM analysis mat file
TM_analysis = TM_pre_process;

%% load an existing mat file
% load('TM_analysis.mat');

%% aocs modes telemetry
[AOCS_Modes_TM_results,GNC_Status_TM_results,INNERTIAL_NAV_TM_results] = get_AOCS_tlm(TM_analysis);

%% plot AOCS TM
% plot_AOCS_Modes_TM(AOCS_Modes_TM_results)
% plot_GNC_Status_TM(GNC_Status_TM_results)
% plot_innertial_NAV_Status_TM(INNERTIAL_NAV_TM_results)

%% plot results
% plot_SC_logic_state(TM_analysis) %plot SC state and substate
% plot_SC_params(TM_analysis); %plot quaternion mass and omega
% plot_engines(TM_analysis) %plot engines activation time
% plot_cruise_results(TM_analysis)
% plot_DV_results(TM_analysis)
% plot_landing_results(TM_analysis)
% plot_obc(TM_analysis)
% plot_events(TM_analysis) %plot all events

%% plot sensors
% plot_IMU_params(TM_analysis); %plot IMUs status and gyro
% plot_STR_params(TM_analysis) %plot STRs epoch time and quaternions
% plot_RW(TM_analysis)
% plot_OMPS(TM_analysis)
% plot_sunvec_params(TM_analysis); %plot sunVec Daylight flag and CSS  
