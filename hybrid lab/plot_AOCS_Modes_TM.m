function [] = plot_AOCS_Modes_TM(AOCS_Modes_TM_results)

[C_modes,~,ic_modes] = unique(AOCS_Modes_TM_results.modes);

figure('name','AOCS Modes')
plot(AOCS_Modes_TM_results.times,ic_modes,'o','MarkerSize',5)
grid on
xlabel('Time [sec]')
yticks(1:1:numel(C_modes))
yticklabels(C_modes)
set(gca,'TickLabelInterpreter','none')
title('AOCS MODES')

[C_NAV_SC_alg,~,ic_NAV_SC_alg] = unique(AOCS_Modes_TM_results.NAV_SC.Alg);
[C_NAV_SC_MassEst,~,ic_NAV_SC_MassEst] = unique(AOCS_Modes_TM_results.NAV_SC.MassEstMode);

figure('name','AOCS Modes - NAV SC')
subplot 311
plot(AOCS_Modes_TM_results.times,ic_NAV_SC_alg,'o')
grid on
xlabel('Time [sec]')
yticks(1:1:numel(C_NAV_SC_alg))
yticklabels(C_NAV_SC_alg)
set(gca,'TickLabelInterpreter','none')
title('AOCS MODES - NAV SC - ALG')
subplot 312
plot(AOCS_Modes_TM_results.times,AOCS_Modes_TM_results.NAV_SC.IsValidTable,'o')
grid on
xlabel('Time [sec]')
title('AOCS MODES - NAV SC - IsValidTable')
subplot 313
plot(AOCS_Modes_TM_results.times,ic_NAV_SC_MassEst,'o')
grid on
xlabel('Time [sec]')
yticks(1:numel(C_NAV_SC_MassEst))
yticklabels(C_NAV_SC_MassEst)
set(gca,'TickLabelInterpreter','none')
title('AOCS MODES - NAV SC - Mass Est Mode')

[C_NAV_SF_alg,~,ic_NAV_SF_alg] = unique(AOCS_Modes_TM_results.NAV_SF.Alg);

figure('name','AOCS Modes - NAV SF')
subplot 321
plot(AOCS_Modes_TM_results.times,ic_NAV_SF_alg,'o')
grid on
xlabel('Time [sec]')
yticks(1:1:numel(C_NAV_SF_alg))
yticklabels(C_NAV_SF_alg)
set(gca,'TickLabelInterpreter','none')
title('AOCS MODES - NAV SF - ALG')
subplot 322
plot(AOCS_Modes_TM_results.times,AOCS_Modes_TM_results.NAV_SF.bias1_cl,'o')
grid on
xlabel('Time [sec]')
title('AOCS MODES - NAV SF - bias1 cl')
subplot 323
plot(AOCS_Modes_TM_results.times,AOCS_Modes_TM_results.NAV_SF.bias2_cl,'o')
grid on
xlabel('Time [sec]')
title('AOCS MODES - NAV SF - bias2 cl')
subplot 324
plot(AOCS_Modes_TM_results.times,AOCS_Modes_TM_results.NAV_SF.cl_specforce,'o')
grid on
xlabel('Time [sec]')
title('AOCS MODES - NAV SF - cl specific force')
subplot 325
plot(AOCS_Modes_TM_results.times,AOCS_Modes_TM_results.NAV_SF.count1_notuse_mod16,'o')
grid on
xlabel('Time [sec]')
title('AOCS MODES - NAV SF - count1 notuse mod16')
subplot 326
plot(AOCS_Modes_TM_results.times,AOCS_Modes_TM_results.NAV_SF.count2_notuse_mod16,'o')
grid on
xlabel('Time [sec]')
title('AOCS MODES - NAV SF - count2 notuse mod16')