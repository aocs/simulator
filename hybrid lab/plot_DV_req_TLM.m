function plot_DV_req_TLM(rec_TLM_analysis)

figure('name','Delta V cross')
subplot 311
plot(rec_TLM_analysis.deltaV_tot.Time,rec_TLM_analysis.deltaV_tot.Data,'o')
grid on
xlabel('Time [sec]')
ylabel('DV [m/sec]')
title('Delta V Total')
subplot 323
plot(rec_TLM_analysis.deltaV_cross_1.Time,rec_TLM_analysis.deltaV_cross_1.Data,'o')
grid on
xlabel('Time [sec]')
ylabel('DV [m/sec]')
title('Delta V cross 1')
subplot 324
plot(rec_TLM_analysis.deltaV_cross_2.Time,rec_TLM_analysis.deltaV_cross_2.Data,'o')
grid on
xlabel('Time [sec]')
ylabel('DV [m/sec]')
title('Delta V cross 2')
subplot 325
plot(rec_TLM_analysis.deltaV_cross_3.Time,rec_TLM_analysis.deltaV_cross_3.Data,'o')
grid on
xlabel('Time [sec]')
ylabel('DV [m/sec]')
title('Delta V cross 3')
subplot 326
plot(rec_TLM_analysis.deltaV_along.Time,rec_TLM_analysis.deltaV_along.Data,'o')
grid on
xlabel('Time [sec]')
ylabel('DV [m/sec]')
title('Delta V Along')