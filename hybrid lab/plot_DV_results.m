function [] = plot_DV_results(TM_analysis)

figure('name','req torque')
subplot 311
plot(TM_analysis.req_torque_1.Time,TM_analysis.req_torque_1.Data,'o')
grid on
xlabel('Time [sec]')
title('req torque 1')
subplot 312
plot(TM_analysis.req_torque_2.Time,TM_analysis.req_torque_2.Data,'o')
grid on
xlabel('Time [sec]')
title('req torque 2')
subplot 313
plot(TM_analysis.req_torque_2.Time,TM_analysis.req_torque_3.Data,'o')
grid on
xlabel('Time [sec]')
title('req torque 3')

figure('name','fb est')
% subplot 321
% plot(TM_analysis.fb_est_filt_1.Time,TM_analysis.fb_est_filt_1.Data,'o')
% grid on
% xlabel('Time [sec]')
% title('fb est filt 1')
subplot 311
plot(TM_analysis.fb_est_for_integ_1.Time,TM_analysis.fb_est_for_integ_1.Data,'o')
grid on
xlabel('Time [sec]')
title('fb est for integ 1')
% subplot 323
% plot(TM_analysis.fb_est_filt_2.Time,TM_analysis.fb_est_filt_2.Data,'o')
% grid on
% xlabel('Time [sec]')
% title('fb est filt 2')
subplot 312
plot(TM_analysis.fb_est_for_integ_2.Time,TM_analysis.fb_est_for_integ_2.Data,'o')
grid on
xlabel('Time [sec]')
title('fb est for integ 2')
% subplot 325
% plot(TM_analysis.fb_est_filt_3.Time,TM_analysis.fb_est_filt_3.Data,'o')
% grid on
% xlabel('Time [sec]')
% title('fb est filt 3')
subplot 313
plot(TM_analysis.fb_est_for_integ_3.Time,TM_analysis.fb_est_for_integ_3.Data,'o')
grid on
xlabel('Time [sec]')
title('fb est for integ 3')

figure('name','DV tot')
plot(TM_analysis.deltaV_tot.Time,TM_analysis.deltaV_tot.Data,'o')
grid on
xlabel('Time [sec]')
title('DV tot')

figure('name','SG dt no STR')
subplot 211
plot(TM_analysis.SG11_dt_NO_STR.Time,TM_analysis.SG11_dt_NO_STR.Data,'.')
grid on
xlabel('time[sec]')
title('SG11 dt no STR')
subplot 212
plot(TM_analysis.SG22_dt_NO_STR.Time,TM_analysis.SG22_dt_NO_STR.Data,'.')
grid on
xlabel('time[sec]')
title('SG22 dt no STR')

