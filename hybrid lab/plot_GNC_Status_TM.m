function [] = plot_GNC_Status_TM(GNC_Status_TM_results)

[C_modes,~,ic_modes] = unique(GNC_Status_TM_results.gnc_mode);
[C_controller,~,ic_controller] = unique(GNC_Status_TM_results.controller);
[C_pwm,~,ic_pwm] = unique(GNC_Status_TM_results.pwm);

figure('name','GNC Status TM')
subplot 311
plot(GNC_Status_TM_results.times,ic_modes,'o')
grid on
xlabel('Time [sec]')
yticks(1:1:numel(C_modes))
yticklabels(C_modes)
set(gca,'TickLabelInterpreter','none')
title('GNC Modes')
subplot 312
plot(GNC_Status_TM_results.times,ic_controller,'o')
grid on
xlabel('Time [sec]')
yticks(1:1:numel(C_controller))
yticklabels(C_controller)
set(gca,'TickLabelInterpreter','none')
title('GNC Controller')
subplot 313
plot(GNC_Status_TM_results.times,ic_pwm,'o')
grid on
xlabel('Time [sec]')
yticks(1:1:numel(C_pwm))
yticklabels(C_pwm)
set(gca,'TickLabelInterpreter','none')
title('GNC PWM')