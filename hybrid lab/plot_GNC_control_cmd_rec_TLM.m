function plot_GNC_control_cmd_rec_TLM(rec_TLM_analysis)

figure('name','err vec')
subplot 311
plot(rec_TLM_analysis.err_vec_1.Time,rec_TLM_analysis.err_vec_1.Data,'o')
grid on
xlabel('Time [sec]')
title('err Vec - X')
subplot 312
plot(rec_TLM_analysis.err_vec_2.Time,rec_TLM_analysis.err_vec_2.Data,'o')
grid on
xlabel('Time [sec]')
title('err Vec - Y')
subplot 313
plot(rec_TLM_analysis.err_vec_3.Time,rec_TLM_analysis.err_vec_3.Data,'o')
grid on
xlabel('Time [sec]')
title('err Vec - Z')

figure('name','req Torque')
subplot 311
plot(rec_TLM_analysis.req_torque_1.Time,rec_TLM_analysis.req_torque_1.Data,'o')
grid on
xlabel('Time [sec]')
title('req Torque - X')
subplot 312
plot(rec_TLM_analysis.req_torque_2.Time,rec_TLM_analysis.req_torque_2.Data,'o')
grid on
xlabel('Time [sec]')
title('req Torque - Y')
subplot 313
plot(rec_TLM_analysis.req_torque_3.Time,rec_TLM_analysis.req_torque_3.Data,'o')
grid on
xlabel('Time [sec]')
title('req Torque - Z')