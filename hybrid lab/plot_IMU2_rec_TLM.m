function plot_IMU2_rec_TLM(rec_TLM_analysis)

figure('name','IMU 2 - accelero est bias')
subplot 311
plot(rec_TLM_analysis.Accelero2_estimatedbias_1.Time,rec_TLM_analysis.Accelero2_estimatedbias_1.Data,'o')
grid on
xlabel('Time [sec]')
ylabel('acc est bias [m/sec^2]')
title('accelero1 estimated bias - X')
subplot 312
plot(rec_TLM_analysis.Accelero2_estimatedbias_2.Time,rec_TLM_analysis.Accelero2_estimatedbias_2.Data,'o')
grid on
xlabel('Time [sec]')
ylabel('acc est bias [m/sec^2]')
title('accelero1 estimated bias - Y')
subplot 313
plot(rec_TLM_analysis.Accelero2_estimatedbias_3.Time,rec_TLM_analysis.Accelero2_estimatedbias_3.Data,'o')
grid on
xlabel('Time [sec]')
ylabel('acc est bias [m/sec^2]')
title('accelero1 estimated bias - Z')

figure('name','IMU 2 - accelero CL/ Gyro not used')
subplot 311
plot(rec_TLM_analysis.Accelero2_estimatedbias_cl.Time,rec_TLM_analysis.Accelero2_estimatedbias_cl.Data,'o')
grid on
xlabel('Time [sec]')
title('accelero1 estimated bias CL')
subplot 312
plot(rec_TLM_analysis.IMU2_count_gyro_notused.Time,rec_TLM_analysis.IMU2_count_gyro_notused.Data,'o')
grid on
xlabel('Time [sec]')
title('IMU1 count gyro notused')
subplot 313
plot(rec_TLM_analysis.count_acc2_notused.Time,rec_TLM_analysis.count_acc2_notused.Data,'o')
grid on
xlabel('Time [sec]')
title('IMU2 count acc notused')