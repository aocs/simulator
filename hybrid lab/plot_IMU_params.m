function [] = plot_IMU_params(TM_analysis)

figure('name','IMUs status')
subplot 221
plot(TM_analysis.IMU1_OK.Time,TM_analysis.IMU1_OK.Data,'o','LineWidth',1.5)
grid on
xlabel('time [sec]')
title('IMU1 Ok')
subplot 222
plot(TM_analysis.IMU2_OK.Time,TM_analysis.IMU2_OK.Data,'o','LineWidth',1.5)
grid on
xlabel('time [sec]')
title('IMU2 Ok')
subplot 223
plot(TM_analysis.IMU1_READY.Time,TM_analysis.IMU1_READY.Data,'o','LineWidth',1.5)
grid on
xlabel('time [sec]')
title('IMU1 On')
subplot 224
plot(TM_analysis.IMU2_READY.Time,TM_analysis.IMU2_READY.Data,'o','LineWidth',1.5)
grid on
xlabel('time [sec]')
title('IMU2 On')

figure('name','IMU1 gyro')
subplot 311 
plot(TM_analysis.IMU1_gyro_x.Time,TM_analysis.IMU1_gyro_x.Data,'o')
grid on
xlabel('time [sec]')
ylabel('gyro [deg/sec]')
title('IMU1 gyro X')
subplot 312 
plot(TM_analysis.IMU1_gyro_y.Time,TM_analysis.IMU1_gyro_y.Data,'o')
grid on
xlabel('time [sec]')
ylabel('gyro [deg/sec]')
title('IMU1 gyro Y')
subplot 313
plot(TM_analysis.IMU1_gyro_z.Time,TM_analysis.IMU1_gyro_z.Data,'o')
grid on
xlabel('time [sec]')
ylabel('gyro [deg/sec]')
title('IMU1 gyro Z')

figure('name','IMU2 gyro')
subplot 311 
plot(TM_analysis.IMU2_gyro_x.Time,TM_analysis.IMU2_gyro_x.Data,'o')
grid on
xlabel('time [sec]')
ylabel('gyro [deg/sec]')
title('IMU2 gyro X')
subplot 312 
plot(TM_analysis.IMU2_gyro_y.Time,TM_analysis.IMU2_gyro_y.Data,'o')
grid on
xlabel('time [sec]')
ylabel('gyro [deg/sec]')
title('IMU2 gyro Y')
subplot 313
plot(TM_analysis.IMU2_gyro_z.Time,TM_analysis.IMU2_gyro_z.Data,'o')
grid on
xlabel('time [sec]')
ylabel('gyro [deg/sec]')
title('IMU2 gyro Z')

figure('name','IMU1 acc')
subplot 311 
plot(TM_analysis.IMU1_accel_x.Time,TM_analysis.IMU1_accel_x.Data,'o')
grid on
xlabel('time [sec]')
ylabel('acc [g]')
title('IMU1 acc X')
subplot 312 
plot(TM_analysis.IMU1_accel_y.Time,TM_analysis.IMU1_accel_y.Data,'o')
grid on
xlabel('time [sec]')
ylabel('acc [g]')
title('IMU1 acc Y')
subplot 313
plot(TM_analysis.IMU1_accel_y.Time,TM_analysis.IMU1_accel_y.Data,'o')
grid on
xlabel('time [sec]')
ylabel('acc [g]')
title('IMU1 acc Z')

figure('name','IMU2 acc')
subplot 311 
plot(TM_analysis.IMU2_accel_x.Time,TM_analysis.IMU2_accel_x.Data,'o')
grid on
xlabel('time [sec]')
ylabel('acc [g]')
title('IMU2 acc X')
subplot 312 
plot(TM_analysis.IMU2_accel_y.Time,TM_analysis.IMU2_accel_y.Data,'o')
grid on
xlabel('time [sec]')
ylabel('acc [g]')
title('IMU2 acc Y')
subplot 313
plot(TM_analysis.IMU2_accel_y.Time,TM_analysis.IMU2_accel_y.Data,'o')
grid on
xlabel('time [sec]')
ylabel('acc [g]')
title('IMU2 acc Z')