function plot_Landing_rec_TLM(rec_TLM_analysis)

figure('name','LLA')
subplot 221
plot(rec_TLM_analysis.LLA_1.Time,rad2deg(rec_TLM_analysis.LLA_1.Data),'o')
grid on
xlabel('Time [sec]')
ylabel('lat [deg]')
title('Lat')
subplot 222
plot(rec_TLM_analysis.LLA_2.Time,rad2deg(rec_TLM_analysis.LLA_2.Data),'o')
grid on
xlabel('Time [sec]')
ylabel('lon [deg]')
title('Lon')
subplot 223
plot(rec_TLM_analysis.LLA_3.Time,rec_TLM_analysis.LLA_3.Data,'o')
grid on
xlabel('Time [sec]')
ylabel('Alt [m]')
title('Lat')
subplot 224
plot(rec_TLM_analysis.LLA_surf_3.Time,rec_TLM_analysis.LLA_surf_3.Data,'o')
grid on
xlabel('Time [sec]')
ylabel('alt [deg]')
title('Alt surf')

figure('name','q L2B')
subplot 221
plot(rec_TLM_analysis.q_l2b_1.Time,rec_TLM_analysis.q_l2b_1.Data,'o')
grid on
xlabel('Time [sec]')
ylabel('q_1')
title('q_1')
subplot 222
plot(rec_TLM_analysis.q_l2b_2.Time,rec_TLM_analysis.q_l2b_2.Data,'o')
grid on
xlabel('Time [sec]')
ylabel('q_2')
title('q_2')
subplot 223
plot(rec_TLM_analysis.q_l2b_3.Time,rec_TLM_analysis.q_l2b_3.Data,'o')
grid on
xlabel('Time [sec]')
ylabel('q_3')
title('q_3')
subplot 224
plot(rec_TLM_analysis.q_l2b_4.Time,rec_TLM_analysis.q_l2b_4.Data,'o')
grid on
xlabel('Time [sec]')
ylabel('q_4')
title('q_4')

figure('name','W L2B')
subplot 311
plot(rec_TLM_analysis.w_l2b_b_1.Time,rec_TLM_analysis.w_l2b_b_1.Data,'o')
grid on
xlabel('Time [sec]')
ylabel('W_1 [rad/sec]')
title('W_1')
subplot 312
plot(rec_TLM_analysis.w_l2b_b_2.Time,rec_TLM_analysis.w_l2b_b_2.Data,'o')
grid on
xlabel('Time [sec]')
ylabel('W_2 [rad/sec]')
title('W_2')
subplot 313
plot(rec_TLM_analysis.w_l2b_b_3.Time,rec_TLM_analysis.w_l2b_b_3.Data,'o')
grid on
xlabel('Time [sec]')
ylabel('W_3 [rad/sec]')
title('W_3')

figure('name','Landing CMD')
subplot 311
plot(rec_TLM_analysis.ct_cmd.Time,rec_TLM_analysis.ct_cmd.Data,'o')
grid on
xlabel('Time [sec]')
title('ct CMD')
subplot 312
plot(rec_TLM_analysis.tilt_cmd.Time,rec_TLM_analysis.tilt_cmd.Data,'o')
grid on
xlabel('Time [sec]')
title('tilt CMD')
subplot 313
plot(rec_TLM_analysis.V_down_goal.Time,rec_TLM_analysis.V_down_goal.Data,'o')
grid on
xlabel('Time [sec]')
title('V Down Goal')