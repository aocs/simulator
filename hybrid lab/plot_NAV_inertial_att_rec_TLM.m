function plot_NAV_inertial_att_rec_TLM(rec_TLM_analysis)

figure('name','q I2B est')
subplot 221
plot(rec_TLM_analysis.q_i2b_est_1.Time,rec_TLM_analysis.q_i2b_est_1.Data,'o')
grid on
xlabel('Time [sec]')
title('q_1')
subplot 222
plot(rec_TLM_analysis.q_i2b_est_2.Time,rec_TLM_analysis.q_i2b_est_2.Data,'o')
grid on
xlabel('Time [sec]')
title('q_2')
subplot 223
plot(rec_TLM_analysis.q_i2b_est_3.Time,rec_TLM_analysis.q_i2b_est_3.Data,'o')
grid on
xlabel('Time [sec]')
title('q_3')
subplot 224
plot(rec_TLM_analysis.q_i2b_est_4.Time,rec_TLM_analysis.q_i2b_est_4.Data,'o')
grid on
xlabel('Time [sec]')
title('q_4')

figure('name','w I2B est')
subplot 311
plot(rec_TLM_analysis.w_i2b_est_1.Time,rec_TLM_analysis.w_i2b_est_1.Data,'o')
grid on
xlabel('Time [sec]')
title('W_x')
subplot 312
plot(rec_TLM_analysis.w_i2b_est_2.Time,rec_TLM_analysis.w_i2b_est_2.Data,'o')
grid on
xlabel('Time [sec]')
title('W_y')
subplot 313
plot(rec_TLM_analysis.w_i2b_est_3.Time,rec_TLM_analysis.w_i2b_est_3.Data,'o')
grid on
xlabel('Time [sec]')
title('W_z')

figure('name','mass')
plot(rec_TLM_analysis.mass.Time,rec_TLM_analysis.mass.Data,'o')
grid on
xlabel('Time [sec]')
ylabel('mass [Kg]')
title('mass')
