function plot_NAV_rec_TLM(rec_TLM_analysis)

figure('name','Force est')
subplot 311
plot(rec_TLM_analysis.Force_est_1.Time,rec_TLM_analysis.Force_est_1.Data,'o')
grid on
xlabel('Time [sec]')
title('force est - X')
subplot 312
plot(rec_TLM_analysis.Force_est_2.Time,rec_TLM_analysis.Force_est_2.Data,'o')
grid on
xlabel('Time [sec]')
title('force est - Y')
subplot 313
plot(rec_TLM_analysis.Force_est_3.Time,rec_TLM_analysis.Force_est_3.Data,'o')
grid on
xlabel('Time [sec]')
title('force est - Z')

figure('name','Torque est')
subplot 311
plot(rec_TLM_analysis.Torque_est_1.Time,rec_TLM_analysis.Torque_est_1.Data,'o')
grid on
xlabel('Time [sec]')
title('Torque est - X')
subplot 312
plot(rec_TLM_analysis.Torque_est_2.Time,rec_TLM_analysis.Torque_est_2.Data,'o')
grid on
xlabel('Time [sec]')
title('Torque est - Y')
subplot 313
plot(rec_TLM_analysis.Torque_est_3.Time,rec_TLM_analysis.Torque_est_3.Data,'o')
grid on
xlabel('Time [sec]')
title('Torque est - Z')

figure('name','fb est')
subplot 321
plot(rec_TLM_analysis.fb_est_1.Time,rec_TLM_analysis.fb_est_1.Data,'o')
grid on
xlabel('Time [sec]')
title('fb est - X')
subplot 322
plot(rec_TLM_analysis.fb_est_for_integ_1.Time,rec_TLM_analysis.fb_est_for_integ_1.Data,'o')
grid on
xlabel('Time [sec]')
title('fb est for integration - X')
subplot 323
plot(rec_TLM_analysis.fb_est_2.Time,rec_TLM_analysis.fb_est_2.Data,'o')
grid on
xlabel('Time [sec]')
title('fb est - Y')
subplot 324
plot(rec_TLM_analysis.fb_est_for_integ_2.Time,rec_TLM_analysis.fb_est_for_integ_2.Data,'o')
grid on
xlabel('Time [sec]')
title('fb est for integration - Y')
subplot 325
plot(rec_TLM_analysis.fb_est_3.Time,rec_TLM_analysis.fb_est_3.Data,'o')
grid on
xlabel('Time [sec]')
title('fb est - Z')
subplot 326
plot(rec_TLM_analysis.fb_est_for_integ_3.Time,rec_TLM_analysis.fb_est_for_integ_3.Data,'o')
grid on
xlabel('Time [sec]')
title('fb est for integration - Z')

figure('name','SunVec B CSS')
subplot 311
plot(rec_TLM_analysis.sun_vec_b_css_1.Time,rec_TLM_analysis.sun_vec_b_css_1.Data,'o')
grid on
xlabel('Time [sec]')
title('SunVec B CSS - X')
subplot 312
plot(rec_TLM_analysis.sun_vec_b_css_2.Time,rec_TLM_analysis.sun_vec_b_css_2.Data,'o')
grid on
xlabel('Time [sec]')
title('SunVec B CSS - Y')
subplot 313
plot(rec_TLM_analysis.sun_vec_b_css_3.Time,rec_TLM_analysis.sun_vec_b_css_3.Data,'o')
grid on
xlabel('Time [sec]')
title('SunVec B CSS - Z')