function [] = plot_OMPS(TM_analysis)

figure('name','h est from ranges')
plot(TM_analysis.h_surf_est_from_ranges.Time,TM_analysis.h_surf_est_from_ranges.Data)
grid on
xlabel('Time [sec]')
ylabel('height [m]')
title('h surf est from ranges')