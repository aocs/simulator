function plot_OMPS_rec_TLM(rec_TLM_analysis)

figure('name','Doppler')
subplot 221
plot(rec_TLM_analysis.ds_1.Time,rec_TLM_analysis.ds_1.Data,'o')
grid on
xlabel('Time [sec]')
title('ds_1')
subplot 222
plot(rec_TLM_analysis.ds_2.Time,rec_TLM_analysis.ds_2.Data,'o')
grid on
xlabel('Time [sec]')
title('ds_2')
subplot 223
plot(rec_TLM_analysis.ds_3.Time,rec_TLM_analysis.ds_3.Data,'o')
grid on
xlabel('Time [sec]')
title('ds_3')
subplot 224
plot(rec_TLM_analysis.ds_4.Time,rec_TLM_analysis.ds_4.Data,'o')
grid on
xlabel('Time [sec]')
title('ds_4')

figure('name','Ranges')
subplot 221
plot(rec_TLM_analysis.r_1.Time,rec_TLM_analysis.r_1.Data,'o')
grid on
xlabel('Time [sec]')
title('R_1')
subplot 222
plot(rec_TLM_analysis.r_2.Time,rec_TLM_analysis.r_2.Data,'o')
grid on
xlabel('Time [sec]')
title('R_2')
subplot 223
plot(rec_TLM_analysis.r_3.Time,rec_TLM_analysis.r_3.Data,'o')
grid on
xlabel('Time [sec]')
title('R_3')
subplot 224
plot(rec_TLM_analysis.r_4.Time,rec_TLM_analysis.r_4.Data,'o')
grid on
xlabel('Time [sec]')
title('R_4')

figure('name','quaternion B2L OMPS')
subplot 221
plot(rec_TLM_analysis.ql2bomps_1.Time,rec_TLM_analysis.ql2bomps_1.Data,'o')
grid on
xlabel('Time [sec]')
title('q_1')
subplot 222
plot(rec_TLM_analysis.ql2bomps_2.Time,rec_TLM_analysis.ql2bomps_2.Data,'o')
grid on
xlabel('Time [sec]')
title('q_2')
subplot 223
plot(rec_TLM_analysis.ql2bomps_3.Time,rec_TLM_analysis.ql2bomps_3.Data,'o')
grid on
xlabel('Time [sec]')
title('q_3')
subplot 224
plot(rec_TLM_analysis.ql2bomps_4.Time,rec_TLM_analysis.ql2bomps_4.Data,'o')
grid on
xlabel('Time [sec]')
title('q_4')

figure('name','h sutf est from ranges')
plot(rec_TLM_analysis.h_surf_est_from_ranges.Time,rec_TLM_analysis.h_surf_est_from_ranges.Data,'o')
grid on
xlabel('Time [sec]')
ylabel('alt [m]')
title('h surf est from ranges')