function [] = plot_RW(TM_analysis)

figure('name','RW Status')
subplot 221
plot(TM_analysis.HPDU2_RW.Time,TM_analysis.HPDU2_RW.Data,'o','LineWidth',1.5)
grid on
xlabel('Time [sec]')
title('RW Power')
subplot 222
plot(TM_analysis.RW_OK.Time,TM_analysis.RW_OK.Data,'o','LineWidth',1.5)
grid on
xlabel('Time [sec]')
title('RW OK')
subplot 223
plot(TM_analysis.RW_OmegaMeasured.Time,TM_analysis.RW_OmegaMeasured.Data,'o','LineWidth',1.5)
grid on
xlabel('Time [sec]')
title('RW meas [rpm]')
subplot 224
plot(TM_analysis.RW_OmegaSetPoint.Time,TM_analysis.RW_OmegaSetPoint.Data,'o','LineWidth',1.5)
grid on
xlabel('Time [sec]')
title('RW SetPoint [rpm]')