function [] = plot_SC_logic_state(TM_analysis)

filename = 'SC Logic State.csv';
SC_SubState_Legend = get_SC_Logic_State(filename);
C_State = table2cell(SC_SubState_Legend(:,1));

figure('name','SC Logic State')
plot(TM_analysis.SC_LOGIC_SUBSTATE.Time,TM_analysis.SC_LOGIC_SUBSTATE.Data,'o')
grid on
xlabel('time [sec]')
yticks(0:1:numel(C_State))
yticklabels(C_State)
set(gca,'TickLabelInterpreter','none')
title('SC Logic state')