function [] = plot_SC_params(TM_analysis)

figure('name','Q I2B est')
subplot 221
plot(TM_analysis.q_i2b_est_1.Time,TM_analysis.q_i2b_est_1.Data,'O')
grid on
xlabel('time [sec]')
title('q_1')
subplot 222
plot(TM_analysis.q_i2b_est_2.Time,TM_analysis.q_i2b_est_2.Data,'O')
grid on
xlabel('time [sec]')
title('q_2')
subplot 223
plot(TM_analysis.q_i2b_est_3.Time,TM_analysis.q_i2b_est_3.Data,'O')
grid on
xlabel('time [sec]')
title('q_3')
subplot 224
plot(TM_analysis.q_i2b_est_4.Time,TM_analysis.q_i2b_est_4.Data,'O')
grid on
xlabel('time [sec]')
title('q_4')

figure('name','Mass')
plot(TM_analysis.mass.Time,TM_analysis.mass.Data,'O')
grid on
xlabel('time [sec]')
ylabel('mass [Kg]')
title('mass')

figure('name','W I2B est')
subplot 311
plot(TM_analysis.w_i2b_est_1.Time,TM_analysis.w_i2b_est_1.Data,'O')
grid on
xlabel('time [sec]')
ylabel('[rad/sec]')
title('W I2B est 1')
subplot 312
plot(TM_analysis.w_i2b_est_2.Time,TM_analysis.w_i2b_est_2.Data,'O')
grid on
xlabel('time [sec]')
ylabel('[rad/sec]')
title('W I2B est 2')
subplot 313
plot(TM_analysis.w_i2b_est_3.Time,TM_analysis.w_i2b_est_3.Data,'O')
grid on
xlabel('time [sec]')
ylabel('[rad/sec]')
title('W I2B est 3')