function plot_SG_rec_TLM(rec_TLM_analysis)

figure('name','SG11 Gyro bias est EKF')
subplot 311
plot(rec_TLM_analysis.SG11_Gyro_bias_est_EKF_1.Time,rec_TLM_analysis.SG11_Gyro_bias_est_EKF_1.Data,'o')
grid on
xlabel('Time [sec]')
title('Gyro bias est EKF - X')
subplot 312
plot(rec_TLM_analysis.SG11_Gyro_bias_est_EKF_2.Time,rec_TLM_analysis.SG11_Gyro_bias_est_EKF_2.Data,'o')
grid on
xlabel('Time [sec]')
title('Gyro bias est EKF - Y')
subplot 313
plot(rec_TLM_analysis.SG11_Gyro_bias_est_EKF_3.Time,rec_TLM_analysis.SG11_Gyro_bias_est_EKF_3.Data,'o')
grid on
xlabel('Time [sec]')
title('Gyro bias est EKF - Z')

figure('name','SG22 Gyro bias est EKF')
subplot 311
plot(rec_TLM_analysis.SG22_Gyro_bias_est_EKF_1.Time,rec_TLM_analysis.SG22_Gyro_bias_est_EKF_1.Data,'o')
grid on
xlabel('Time [sec]')
title('Gyro bias est EKF - X')
subplot 312
plot(rec_TLM_analysis.SG22_Gyro_bias_est_EKF_2.Time,rec_TLM_analysis.SG22_Gyro_bias_est_EKF_2.Data,'o')
grid on
xlabel('Time [sec]')
title('Gyro bias est EKF - Y')
subplot 313
plot(rec_TLM_analysis.SG22_Gyro_bias_est_EKF_3.Time,rec_TLM_analysis.SG22_Gyro_bias_est_EKF_3.Data,'o')
grid on
xlabel('Time [sec]')
title('Gyro bias est EKF - Z')

figure('name','SG Innovation/ dt no STR')
subplot 321
plot(rec_TLM_analysis.SG11_Innov_ang_deg_simple.Time,rec_TLM_analysis.SG11_Innov_ang_deg_simple.Data,'o')
grid on
xlabel('Time [sec]')
title('SG11 innovation ang deg simple')
subplot 322
plot(rec_TLM_analysis.SG11_Innov_angle_deg_EKF.Time,rec_TLM_analysis.SG11_Innov_angle_deg_EKF.Data,'o')
grid on
xlabel('Time [sec]')
title('SG11 innovation ang deg EKF')
subplot 323
plot(rec_TLM_analysis.SG22_Innov_ang_deg_simple.Time,rec_TLM_analysis.SG22_Innov_ang_deg_simple.Data,'o')
grid on
xlabel('Time [sec]')
title('SG22 innovation ang deg simple')
subplot 324
plot(rec_TLM_analysis.SG22_Innov_angle_deg_EKF.Time,rec_TLM_analysis.SG22_Innov_angle_deg_EKF.Data,'o')
grid on
xlabel('Time [sec]')
title('SG22 innovation ang deg EKF')
subplot 325
plot(rec_TLM_analysis.SG11_dt_NO_STR.Time,rec_TLM_analysis.SG11_dt_NO_STR.Data,'o')
grid on
xlabel('Time [sec]')
title('SG11 dt no STR')
subplot 326
plot(rec_TLM_analysis.SG22_dt_NO_STR.Time,rec_TLM_analysis.SG22_dt_NO_STR.Data,'o')
grid on
xlabel('Time [sec]')
title('SG22 dt no STR')

figure('name','SG11 q I2B est')
subplot 221
plot(rec_TLM_analysis.SG11_q_i2b_est_1.Time,rec_TLM_analysis.SG11_q_i2b_est_1.Data,'o')
grid on
xlabel('Time [sec]')
ylabel('q_1')
title('q I2B est 1')
subplot 222
plot(rec_TLM_analysis.SG11_q_i2b_est_2.Time,rec_TLM_analysis.SG11_q_i2b_est_2.Data,'o')
grid on
xlabel('Time [sec]')
ylabel('q_2')
title('q I2B est 2')
subplot 223
plot(rec_TLM_analysis.SG11_q_i2b_est_3.Time,rec_TLM_analysis.SG11_q_i2b_est_3.Data,'o')
grid on
xlabel('Time [sec]')
ylabel('q_3')
title('q I2B est 3')
subplot 224
plot(rec_TLM_analysis.SG11_q_i2b_est_4.Time,rec_TLM_analysis.SG11_q_i2b_est_4.Data,'o')
grid on
xlabel('Time [sec]')
ylabel('q_4')
title('q I2B est 4')

% figure('name','SG22 q I2B est')
% subplot 221
% plot(rec_TLM_analysis.SG22_q_i2b_est_1.Time,rec_TLM_analysis.SG22_q_i2b_est_1.Data,'o')
% grid on
% xlabel('Time [sec]')
% ylabel('q_1')
% title('q I2B est 1')
% subplot 222
% plot(rec_TLM_analysis.SG22_q_i2b_est_2.Time,rec_TLM_analysis.SG22_q_i2b_est_2.Data,'o')
% grid on
% xlabel('Time [sec]')
% ylabel('q_2')
% title('q I2B est 2')
% subplot 223
% plot(rec_TLM_analysis.SG22_q_i2b_est_3.Time,rec_TLM_analysis.SG22_q_i2b_est_3.Data,'o')
% grid on
% xlabel('Time [sec]')
% ylabel('q_3')
% title('q I2B est 3')
% subplot 224
% plot(rec_TLM_analysis.SG22_q_i2b_est_4.Time,rec_TLM_analysis.SG22_q_i2b_est_4.Data,'o')
% grid on
% xlabel('Time [sec]')
% ylabel('q_4')
% title('q I2B est 4')

figure('name','SG11 q I2B est simple')
subplot 221
plot(rec_TLM_analysis.SG11_q_i2b_est_simple_1.Time,rec_TLM_analysis.SG11_q_i2b_est_simple_1.Data,'o')
grid on
xlabel('Time [sec]')
ylabel('q_1')
title('q I2B est 1')
subplot 222
plot(rec_TLM_analysis.SG11_q_i2b_est_simple_2.Time,rec_TLM_analysis.SG11_q_i2b_est_simple_2.Data,'o')
grid on
xlabel('Time [sec]')
ylabel('q_2')
title('q I2B est 2')
subplot 223
plot(rec_TLM_analysis.SG11_q_i2b_est_simple_3.Time,rec_TLM_analysis.SG11_q_i2b_est_simple_3.Data,'o')
grid on
xlabel('Time [sec]')
ylabel('q_3')
title('q I2B est 3')
subplot 224
plot(rec_TLM_analysis.SG11_q_i2b_est_simple_4.Time,rec_TLM_analysis.SG11_q_i2b_est_simple_4.Data,'o')
grid on
xlabel('Time [sec]')
ylabel('q_4')
title('q I2B est 4')

figure('name','SG11 W I2B est')
subplot 311
plot(rec_TLM_analysis.SG11_w_i2b_b_est_1.Time,rec_TLM_analysis.SG11_w_i2b_b_est_1.Data,'o')
grid on
xlabel('Time [sec]')
ylabel('W_1')
title('W I2B est 1')
subplot 312
plot(rec_TLM_analysis.SG11_w_i2b_b_est_2.Time,rec_TLM_analysis.SG11_w_i2b_b_est_2.Data,'o')
grid on
xlabel('Time [sec]')
ylabel('W_2')
title('W I2B est 2')
subplot 313
plot(rec_TLM_analysis.SG11_w_i2b_b_est_3.Time,rec_TLM_analysis.SG11_w_i2b_b_est_3.Data,'o')
grid on
xlabel('Time [sec]')
ylabel('W_3')
title('W I2B est 3')