function [] = plot_STR_params(TM_analysis)

figure('name','STRs status')
subplot 221
plot(TM_analysis.STR1_READY.Time,TM_analysis.STR1_READY.Data,'o','LineWidth',1.5)
grid on
xlabel('time [sec]')
title('STR1 Ok')
subplot 222
plot(TM_analysis.STR2_READY.Time,TM_analysis.STR2_READY.Data,'o','LineWidth',1.5)
grid on
xlabel('time [sec]')
title('STR2 Ok')
subplot 223
plot(TM_analysis.HPDU1_STR1.Time,TM_analysis.HPDU1_STR1.Data,'o','LineWidth',1.5)
grid on
xlabel('time [sec]')
title('STR1 On')
subplot 224
plot(TM_analysis.HPDU2_STR2.Time,TM_analysis.HPDU2_STR2.Data,'o','LineWidth',1.5)
grid on
xlabel('time [sec]')
title('STR2 On')

figure('name','STRs epoch time')
subplot 211
plot(TM_analysis.STR1_epoch_time.Time,TM_analysis.STR1_epoch_time.Data,'o','LineWidth',1.5)
grid on
xlabel('time[sec]')
title('STR 1 epoch time')
subplot 212
plot(TM_analysis.STR2_epoch_time.Time,TM_analysis.STR2_epoch_time.Data,'o','LineWidth',1.5)
grid on
xlabel('time[sec]')
title('STR 2 epoch time')

figure('name','STR 1 quaternion')
subplot 221
plot(TM_analysis.STR1_inertial_att_q0.Time,TM_analysis.STR1_inertial_att_q0.Data,'o')
grid on
xlabel('time[sec]')
title('STR 1 q_1')
subplot 222
plot(TM_analysis.STR1_inertial_att_q1.Time,TM_analysis.STR1_inertial_att_q1.Data,'o')
grid on
xlabel('time[sec]')
title('STR 1 q_2')
subplot 223
plot(TM_analysis.STR1_inertial_att_q2.Time,TM_analysis.STR1_inertial_att_q2.Data,'o')
grid on
xlabel('time[sec]')
title('STR 1 q_3')
subplot 224
plot(TM_analysis.STR1_inertial_att_q3.Time,TM_analysis.STR1_inertial_att_q3.Data,'o')
grid on
xlabel('time[sec]')
title('STR 1 q_4')


figure('name','STR 2 quaternion')
subplot 221
plot(TM_analysis.STR2_inertial_att_q0.Time,TM_analysis.STR2_inertial_att_q0.Data,'o')
grid on
xlabel('time[sec]')
title('STR 2 q_1')
subplot 222
plot(TM_analysis.STR2_inertial_att_q1.Time,TM_analysis.STR2_inertial_att_q1.Data,'o')
grid on
xlabel('time[sec]')
title('STR 2 q_2')
subplot 223
plot(TM_analysis.STR2_inertial_att_q2.Time,TM_analysis.STR2_inertial_att_q2.Data,'o')
grid on
xlabel('time[sec]')
title('STR 2 q_3')
subplot 224
plot(TM_analysis.STR2_inertial_att_q3.Time,TM_analysis.STR2_inertial_att_q3.Data,'o')
grid on
xlabel('time[sec]')
title('STR 2 q_4')

