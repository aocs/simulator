function plot_STR_rec_TLM(rec_TLM_analysis)

figure('name','STR q I2STR meas')
subplot 221
plot(rec_TLM_analysis.STR_q_I_2_STR_meas_1.Time,rec_TLM_analysis.STR_q_I_2_STR_meas_1.Data,'o')
grid on
xlabel('Time [sec]')
ylabel('q_1')
title('q_1')
subplot 222
plot(rec_TLM_analysis.STR_q_I_2_STR_meas_2.Time,rec_TLM_analysis.STR_q_I_2_STR_meas_2.Data,'o')
grid on
xlabel('Time [sec]')
ylabel('q_2')
title('q_2')
subplot 223
plot(rec_TLM_analysis.STR_q_I_2_STR_meas_3.Time,rec_TLM_analysis.STR_q_I_2_STR_meas_3.Data,'o')
grid on
xlabel('Time [sec]')
ylabel('q_3')
title('q_3')
subplot 224
plot(rec_TLM_analysis.STR_q_I_2_STR_meas_4.Time,rec_TLM_analysis.STR_q_I_2_STR_meas_4.Data,'o')
grid on
xlabel('Time [sec]')
ylabel('q_4')
title('q_4')

figure('name','STR Timetag and epoch time')
subplot 211
plot(rec_TLM_analysis.STR_epoch_time.Time,rec_TLM_analysis.STR_epoch_time.Data,'o')
grid on
xlabel('Time [sec]')
title('Epoch time')
subplot 212
plot(rec_TLM_analysis.STR_TimeTag.Time,rec_TLM_analysis.STR_TimeTag.Data,'o')
grid on
xlabel('Time [sec]')
title('TimeTag')