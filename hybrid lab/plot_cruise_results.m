function [] = plot_cruise_results(TM_analysis)

figure('name','err Vec')
subplot 311
plot(TM_analysis.err_vec_1.Time,TM_analysis.err_vec_1.Data,'o')
grid on
xlabel('Time [sec]')
title('err Vec 1')
subplot 312
plot(TM_analysis.err_vec_2.Time,TM_analysis.err_vec_2.Data,'o')
grid on
xlabel('Time [sec]')
title('err Vec 2')
subplot 313
plot(TM_analysis.err_vec_3.Time,TM_analysis.err_vec_3.Data,'o')
grid on
xlabel('Time [sec]')
title('err Vec 3')

figure('name','accelero estimated bias')
subplot 321
plot(TM_analysis.Accelero1_estimatedbias_1.Time,TM_analysis.Accelero1_estimatedbias_1.Data,'o')
grid on
xlabel('Time [sec]')
title('Accelero1 estimatedbias 1')
subplot 322
plot(TM_analysis.Accelero2_estimatedbias_1.Time,TM_analysis.Accelero2_estimatedbias_1.Data,'o')
grid on
xlabel('Time [sec]')
title('Accelero2 estimatedbias 1')
subplot 323
plot(TM_analysis.Accelero1_estimatedbias_2.Time,TM_analysis.Accelero1_estimatedbias_2.Data,'o')
grid on
xlabel('Time [sec]')
title('Accelero1 estimatedbias 2')
subplot 324
plot(TM_analysis.Accelero2_estimatedbias_2.Time,TM_analysis.Accelero2_estimatedbias_2.Data,'o')
grid on
xlabel('Time [sec]')
title('Accelero2 estimatedbias 2')
subplot 325
plot(TM_analysis.Accelero1_estimatedbias_3.Time,TM_analysis.Accelero1_estimatedbias_3.Data,'o')
grid on
xlabel('Time [sec]')
title('Accelero1 estimatedbias 3')
subplot 326
plot(TM_analysis.Accelero2_estimatedbias_3.Time,TM_analysis.Accelero2_estimatedbias_3.Data,'o')
grid on
xlabel('Time [sec]')
title('Accelero2 estimatedbias 1')