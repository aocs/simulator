function [] = plot_engines(TM_analysis)

figure('name','ACS 1 total fire time')
subplot 221
stem(TM_analysis.HT11_TOT_FIRE_TIME.Time,TM_analysis.HT11_TOT_FIRE_TIME.Data)
grid on
xlabel('Time [sec]')
title('HT11')
subplot 222
stem(TM_analysis.HT12_TOT_FIRE_TIME.Time,TM_analysis.HT12_TOT_FIRE_TIME.Data)
grid on
xlabel('Time [sec]')
title('HT12')
subplot 223
stem(TM_analysis.HT13_TOT_FIRE_TIME.Time,TM_analysis.HT13_TOT_FIRE_TIME.Data)
grid on
xlabel('Time [sec]')
title('HT13')
subplot 224
stem(TM_analysis.HT14_TOT_FIRE_TIME.Time,TM_analysis.HT14_TOT_FIRE_TIME.Data)
grid on
xlabel('Time [sec]')
title('HT14')

figure('name','ACS 2 total fire time')
subplot 221
stem(TM_analysis.HT21_TOT_FIRE_TIME.Time,TM_analysis.HT21_TOT_FIRE_TIME.Data)
grid on
xlabel('Time [sec]')
title('HT21')
subplot 222
stem(TM_analysis.HT22_TOT_FIRE_TIME.Time,TM_analysis.HT22_TOT_FIRE_TIME.Data)
grid on
xlabel('Time [sec]')
title('HT22')
subplot 223
stem(TM_analysis.HT23_TOT_FIRE_TIME.Time,TM_analysis.HT23_TOT_FIRE_TIME.Data)
grid on
xlabel('Time [sec]')
title('HT23')
subplot 224
stem(TM_analysis.HT24_TOT_FIRE_TIME.Time,TM_analysis.HT24_TOT_FIRE_TIME.Data)
grid on
xlabel('Time [sec]')
title('HT24')
% 
% figure('name','MHT')
% stem(TM_analysis.MHT_hold_TOT_FIRE_TIME.Time,TM_analysis.MHT_hold_TOT_FIRE_TIME.Data)
% grid on
% xlabel('Time [sec]')
% ylabel ('engine activations')
% title('MHT')