function plot_engines_rec_TLM(rec_TLM_analysis)

figure('name','ACS 1')
subplot 221
stem(rec_TLM_analysis.ACS_CMD_to_MC_TELEMETRY_1.Time,rec_TLM_analysis.ACS_CMD_to_MC_TELEMETRY_1.Data)
grid on
xlabel('Time [sec]')
ylabel ('engine activations')
title('HT11')
subplot 222
stem(rec_TLM_analysis.ACS_CMD_to_MC_TELEMETRY_2.Time,rec_TLM_analysis.ACS_CMD_to_MC_TELEMETRY_2.Data)
grid on
xlabel('Time [sec]')
ylabel ('engine activations')
title('HT12')
subplot 223
stem(rec_TLM_analysis.ACS_CMD_to_MC_TELEMETRY_3.Time,rec_TLM_analysis.ACS_CMD_to_MC_TELEMETRY_3.Data)
grid on
xlabel('Time [sec]')
ylabel ('engine activations')
title('HT13')
subplot 224
stem(rec_TLM_analysis.ACS_CMD_to_MC_TELEMETRY_4.Time,rec_TLM_analysis.ACS_CMD_to_MC_TELEMETRY_4.Data)
grid on
xlabel('Time [sec]')
ylabel ('engine activations')
title('HT14')


figure('name','ACS 2')
subplot 221
stem(rec_TLM_analysis.ACS_CMD_to_MC_TELEMETRY_5.Time,rec_TLM_analysis.ACS_CMD_to_MC_TELEMETRY_5.Data)
grid on
xlabel('Time [sec]')
ylabel ('engine activations')
title('HT21')
subplot 222
stem(rec_TLM_analysis.ACS_CMD_to_MC_TELEMETRY_6.Time,rec_TLM_analysis.ACS_CMD_to_MC_TELEMETRY_6.Data)
grid on
xlabel('Time [sec]')
ylabel ('engine activations')
title('HT22')
subplot 223
stem(rec_TLM_analysis.ACS_CMD_to_MC_TELEMETRY_7.Time,rec_TLM_analysis.ACS_CMD_to_MC_TELEMETRY_7.Data)
grid on
xlabel('Time [sec]')
ylabel ('engine activations')
title('HT23')
subplot 224
stem(rec_TLM_analysis.ACS_CMD_to_MC_TELEMETRY_8.Time,rec_TLM_analysis.ACS_CMD_to_MC_TELEMETRY_8.Data)
grid on
xlabel('Time [sec]')
ylabel ('engine activations')
title('HT24')

figure('name','MHT')
stem(rec_TLM_analysis.MHT_CMD_to_MC_TELEMETRY.Time,rec_TLM_analysis.MHT_CMD_to_MC_TELEMETRY.Data)
grid on
xlabel('Time [sec]')
ylabel ('engine activations')
title('MHT')