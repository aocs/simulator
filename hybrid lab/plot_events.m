function [] = plot_events(TM_analysis)

[C,~,~] = unique(TM_analysis.needed_events.Data);

for i=1:numel(C)
    
    compare_chk = cell2mat(cellfun(@(x)strcmp(C(i),x),TM_analysis.needed_events.Data,'UniformOutput',false));
    index = find(compare_chk);
    plot_time(index) = TM_analysis.needed_events.Time(index);
    plot_params(index) = i;
    
end

figure('name','events')
stem(plot_time,plot_params)%,'yticks',0:numel(C)-1,'yticklabels',C)
grid on
xlabel('time [sec]')
yticks(1:numel(C))
yticklabels(C)
set(gca,'TickLabelInterpreter','none')

