function [] = plot_innertial_NAV_Status_TM(INNERTIAL_NAV_TM_results)

[C_alg,~,ic_alg] = unique(INNERTIAL_NAV_TM_results.SunDir.alg);

figure('name','Innertial NAV TM - SunDir')
subplot 221
plot(INNERTIAL_NAV_TM_results.times,ic_alg,'o')
grid on
xlabel('Time [sec]')
yticks(1:1:numel(C_alg))
yticklabels(C_alg)
set(gca,'TickLabelInterpreter','none')
title('innertial Nav TM - SunDir - ALG')
subplot 222
plot(INNERTIAL_NAV_TM_results.times,INNERTIAL_NAV_TM_results.SunDir.cl_sunvecb_css,'o')
grid on
xlabel('Time [sec]')
title('innertial Nav TM - SunDir - cl SunVecB CSS')
subplot 223
plot(INNERTIAL_NAV_TM_results.times,INNERTIAL_NAV_TM_results.SunDir.cl_sunvecb_str,'o')
grid on
xlabel('Time [sec]')
title('innertial Nav TM - SunDir - cl SunVecB STR')
subplot 224
plot(INNERTIAL_NAV_TM_results.times,INNERTIAL_NAV_TM_results.SunDir.icss,'o')
grid on
xlabel('Time [sec]')
title('innertial Nav TM - SunDir - icss')

[C_str1_frame,~,ic_str1_frame] = unique(INNERTIAL_NAV_TM_results.Inertial.STR1_frame);
[C_str2_frame,~,ic_str2_frame] = unique(INNERTIAL_NAV_TM_results.Inertial.STR2_frame);
[C_str1_EKF,~,ic_str1_EKF] = unique(INNERTIAL_NAV_TM_results.Inertial.STR1_GYRO_EKF_mode);
[C_str2_EKF,~,ic_str2_EKF] = unique(INNERTIAL_NAV_TM_results.Inertial.STR2_GYRO_EKF_mode);

figure('name','Innertial NAV TM - STR 1')
subplot 321
plot(INNERTIAL_NAV_TM_results.times,ic_str1_frame,'o')
grid on
xlabel('Time [sec]')
yticks(1:1:numel(C_str1_frame))
yticklabels(C_str1_frame)
set(gca,'TickLabelInterpreter','none')
title('innertial Nav TM - STR1 frame')
subplot 322
plot(INNERTIAL_NAV_TM_results.times,ic_str1_EKF,'o')
grid on
xlabel('Time [sec]')
yticks(1:1:numel(C_str1_EKF))
yticklabels(C_str1_EKF)
set(gca,'TickLabelInterpreter','none')
title('innertial Nav TM - STR1 Gyro EKF Mode')
subplot 323
plot(INNERTIAL_NAV_TM_results.times,INNERTIAL_NAV_TM_results.Inertial.STR1_GYRO_Confidence_q,'o')
grid on
xlabel('Time [sec]')
title('innertial Nav TM - STR1 Gyro Confidence q')
subplot 324
plot(INNERTIAL_NAV_TM_results.times,INNERTIAL_NAV_TM_results.Inertial.STR1_GYRO_Confidence_w,'o')
grid on
xlabel('Time [sec]')
title('innertial Nav TM - STR1 Gyro Confidence w')
subplot 325
plot(INNERTIAL_NAV_TM_results.times,INNERTIAL_NAV_TM_results.Inertial.STR1_ONLY_Confidence_w,'o')
grid on
xlabel('Time [sec]')
title('innertial Nav TM - STR1 Only Confidence q')
subplot 326
plot(INNERTIAL_NAV_TM_results.times,INNERTIAL_NAV_TM_results.Inertial.STR1_GYRO_KF_active,'o')
grid on
xlabel('Time [sec]')
title('innertial Nav TM - STR1 Gyro KF Active')


figure('name','Innertial NAV TM - STR 2')
subplot 321
plot(INNERTIAL_NAV_TM_results.times,ic_str2_frame,'o')
grid on
xlabel('Time [sec]')
yticks(1:1:numel(C_str2_frame))
yticklabels(C_str2_frame)
set(gca,'TickLabelInterpreter','none')
title('innertial Nav TM - STR2 frame')
subplot 322
plot(INNERTIAL_NAV_TM_results.times,ic_str2_EKF,'o')
grid on
xlabel('Time [sec]')
yticks(1:1:numel(C_str2_EKF))
yticklabels(C_str2_EKF)
set(gca,'TickLabelInterpreter','none')
title('innertial Nav TM - STR2 Gyro EKF Mode')
subplot 323
plot(INNERTIAL_NAV_TM_results.times,INNERTIAL_NAV_TM_results.Inertial.STR2_GYRO_Confidence_q,'o')
grid on
xlabel('Time [sec]')
title('innertial Nav TM - STR2 Gyro Confidence q')
subplot 324
plot(INNERTIAL_NAV_TM_results.times,INNERTIAL_NAV_TM_results.Inertial.STR2_GYRO_Confidence_w,'o')
grid on
xlabel('Time [sec]')
title('innertial Nav TM - STR2 Gyro Confidence w')
subplot 325
plot(INNERTIAL_NAV_TM_results.times,INNERTIAL_NAV_TM_results.Inertial.STR2_ONLY_Confidence_w,'o')
grid on
xlabel('Time [sec]')
title('innertial Nav TM - STR2 Only Confidence q')
subplot 326
plot(INNERTIAL_NAV_TM_results.times,INNERTIAL_NAV_TM_results.Inertial.STR2_GYRO_KF_active,'o')
grid on
xlabel('Time [sec]')
title('innertial Nav TM - STR2 Gyro KF Active')

[C_est_choice,~,ic_est_choice] = unique(INNERTIAL_NAV_TM_results.Inertial.estimator_choice);

figure('name','estimator choice/reboot/CSS only confidence w')
subplot 311
plot(INNERTIAL_NAV_TM_results.times,ic_est_choice,'o')
grid on
xlabel('Time [sec]')
yticks(1:1:numel(C_est_choice))
yticklabels(C_est_choice)
set(gca,'TickLabelInterpreter','none')
title('estimator choice')
subplot 312
plot(INNERTIAL_NAV_TM_results.times,INNERTIAL_NAV_TM_results.Inertial.CSS_ONLY_Confidence_w,'o')
grid on
xlabel('Time [sec]')
title('CSS only confidence w')
subplot 313
plot(INNERTIAL_NAV_TM_results.times,INNERTIAL_NAV_TM_results.Inertial.IsRebootMode,'o')
grid on
xlabel('Time [sec]')
title('Is Reboot Mode')