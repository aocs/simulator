function [] = plot_landing_results(TM_analysis)

figure('name','LLA')
subplot 311
plot(TM_analysis.LLA_1.Time,rad2deg(TM_analysis.LLA_1.Data),'LineWidth',1.5)
grid on
xlabel('Time [sec]')
ylabel('lat [deg]')
title('Lat')
subplot 312
plot(TM_analysis.LLA_2.Time,rad2deg(TM_analysis.LLA_2.Data),'LineWidth',1.5)
grid on
xlabel('Time [sec]')
ylabel('lon [deg]')
title('Lon')
subplot 313
plot(TM_analysis.LLA_3.Time,TM_analysis.LLA_3.Data,'LineWidth',1.5)
hold on
plot(TM_analysis.LLA_surf_3.Time,TM_analysis.LLA_surf_3.Data,'--','LineWidth',1.5)
grid on
xlabel('Time [sec]')
ylabel('alt [m]')
legend('alt',' alt surf')
title('alt')

figure('name','V NED')
subplot 311
plot(TM_analysis.v_NED_1.Time,TM_analysis.v_NED_1.Data,'LineWidth',1.5)
grid on
xlabel('Time [sec]')
ylabel('V [m/s]')
title('V North')
subplot 312
plot(TM_analysis.v_NED_2.Time,TM_analysis.v_NED_2.Data,'LineWidth',1.5)
grid on
xlabel('Time [sec]')
ylabel('V [m/s]')
title('V East')
subplot 313
plot(TM_analysis.v_NED_3.Time,TM_analysis.v_NED_3.Data,'LineWidth',1.5)
hold on
plot(TM_analysis.V_down_goal.Time,TM_analysis.V_down_goal.Data,'--','LineWidth',1.5)
grid on
xlabel('Time [sec]')
ylabel('V [m/s]')
legend('V down','V down goal')
title('V Down')

figure('name','W L2Body')
subplot 311
plot(TM_analysis.w_l2b_b_1.Time,TM_analysis.w_l2b_b_1.Data,'LineWidth',1.5)
grid on
xlabel('Time [sec]')
ylabel('W [deg/s]')
title('W L2B_1')
subplot 312
plot(TM_analysis.w_l2b_b_2.Time,TM_analysis.w_l2b_b_2.Data,'LineWidth',1.5)
grid on
xlabel('Time [sec]')
ylabel('W [deg/s]')
title('W L2B_2')
subplot 313
plot(TM_analysis.w_l2b_b_3.Time,TM_analysis.w_l2b_b_3.Data,'LineWidth',1.5)
grid on
xlabel('Time [sec]')
ylabel('W [deg/s]')
title('W L2B_3')

figure('name','ct & tilt cmd')
subplot 211
plot(TM_analysis.ct_cmd.Time,TM_analysis.ct_cmd.Data,'LineWidth',1.5)
grid on
xlabel('Time [sec]')
title('ct CMD')
subplot 212
plot(TM_analysis.tilt_cmd.Time,TM_analysis.tilt_cmd.Data,'LineWidth',1.5)
grid on
xlabel('Time [sec]')
title('tilt CMD')

figure('name','W L2Body')
subplot 221
plot(TM_analysis.q_l2b_1.Time,TM_analysis.q_l2b_1.Data,'LineWidth',1.5)
grid on
xlabel('Time [sec]')
title('q L2B_1')
subplot 222
plot(TM_analysis.q_l2b_2.Time,TM_analysis.q_l2b_2.Data,'LineWidth',1.5)
grid on
xlabel('Time [sec]')
title('q L2B_2')
subplot 223
plot(TM_analysis.q_l2b_3.Time,TM_analysis.q_l2b_3.Data,'LineWidth',1.5)
grid on
xlabel('Time [sec]')
title('q L2B_3')
subplot 224
plot(TM_analysis.q_l2b_4.Time,TM_analysis.q_l2b_4.Data,'LineWidth',1.5)
grid on
xlabel('Time [sec]')
title('q L2B_4')