function [] = plot_sunvec_params(TM_analysis)

sun_dir_colat = timeseries(acosd(TM_analysis.sun_vec_b_css_3.Data),TM_analysis.sun_vec_b_css_3.Time);

figure('name','Sun Direction')
plot(sun_dir_colat)
grid on
xlabel('time [sec]')
title('Sun Colatitude')

figure('name','SunVec B2CSS')
subplot 311
plot(TM_analysis.sun_vec_b_css_1.Time,TM_analysis.sun_vec_b_css_1.Data,'o')
grid on
xlabel('time [sec]')
title('SunVec b css 1')
subplot 312
plot(TM_analysis.sun_vec_b_css_2.Time,TM_analysis.sun_vec_b_css_2.Data,'o')
grid on
xlabel('time [sec]')
title('SunVec b css 2')
subplot 313
plot(TM_analysis.sun_vec_b_css_3.Time,TM_analysis.sun_vec_b_css_3.Data,'o')
grid on
xlabel('time [sec]')
title('SunVec b css 3')

figure('name','DayLight')
plot(TM_analysis.DAYLIGHT.Time,TM_analysis.DAYLIGHT.Data,'o')
grid on
xlabel('time [sec]')
title('DayLight')

figure('name','CSS Validity')
subplot 321
plot(TM_analysis.CSS1_Validity.Time,TM_analysis.CSS1_Validity.Data,'o')
grid on
xlabel('time [sec]')
title('CSS_1 Validity')
subplot 322
plot(TM_analysis.CSS2_Validity.Time,TM_analysis.CSS2_Validity.Data,'o')
grid on
xlabel('time [sec]')
title('CSS_2 Validity')
subplot 323
plot(TM_analysis.CSS3_Validity.Time,TM_analysis.CSS3_Validity.Data,'o')
grid on
xlabel('time [sec]')
title('CSS_3 Validity')
subplot 324
plot(TM_analysis.CSS4_Validity.Time,TM_analysis.CSS4_Validity.Data,'o')
grid on
xlabel('time [sec]')
title('CSS_4 Validity')
subplot 325
plot(TM_analysis.CSS5_Validity.Time,TM_analysis.CSS5_Validity.Data,'o')
grid on
xlabel('time [sec]')
title('CSS_5 Validity')
subplot 326
plot(TM_analysis.CSS6_Validity.Time,TM_analysis.CSS6_Validity.Data,'o')
grid on
xlabel('time [sec]')
title('CSS_6 Validity')