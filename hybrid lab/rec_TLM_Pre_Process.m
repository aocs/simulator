function [rec_TLM_analysis] = rec_TLM_Pre_Process()

addpath('C:\Users\orell\Desktop\v1.2\AOCS\CodeGeneration\Simulator_log_files\hybrid lab\TLM analysis\check results') %results directory
Rec_TM = readtable('AOCS_REC_TM.csv'); %get TM results
SDB = readtable('SDB for matlab.csv'); %get data from SDB
%% sort data from TM

TM_renamed = Rec_TM(:,1:4);
SDB_ID = SDB.fullID; %SDB ID for comparison with TM
SDB_name = SDB.Prm_Name_50_; %names in SDB

[C,~,ic] = unique(Rec_TM.PrmID);

for i=1:numel(C)
    
    T = Rec_TM.TickTime(ic==i);
    D = Rec_TM.PrmValue(ic==i);
    
    ind_ParName = cell2mat(cellfun(@(x)strcmp(C{i},x),SDB_ID,'UniformOutput',false));
    rec_TLM_analysis.(SDB_name{ind_ParName}).Time = T;
    rec_TLM_analysis.(SDB_name{ind_ParName}).Data = D;
    
end


%% times and save mat file
rec_TLM_analysis.tick = linspace(TM_renamed.TickTime(1),TM_renamed.TickTime(end)); %tick count 10[Hz]
rec_TLM_analysis.time = linspace(0,rec_TLM_analysis.AOCS_MODES_TELEMETRY.Time(end),numel(rec_TLM_analysis.AOCS_MODES_TELEMETRY.Time)); %time [sec]

save('rec_TLM_analysis.mat','rec_TLM_analysis');