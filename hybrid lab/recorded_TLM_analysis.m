% clc;clear all;close all

AOCS_enumerations;
%% create TM analysis mat file
rec_TLM_analysis = rec_TLM_Pre_Process();

%% load an existing mat file
% load('rec_TLM_analysis.mat');

%% get AOCS TLM
[AOCS_Modes_rec_TM_results,GNC_Status_rec_TM_results,INNERTIAL_rec_NAV_TM_results] = get_AOCS_tlm(rec_TLM_analysis);

%% plot AOCS TLM
% plot_AOCS_Modes_TM(AOCS_Modes_rec_TM_results)
% plot_GNC_Status_TM(GNC_Status_rec_TM_results)
% plot_innertial_NAV_Status_TM(GNC_Status_rec_TM_results)

%% plots
% plot_engines_rec_TLM(rec_TLM_analysis) %plot recorded engines
% plot_NAV_inertial_att_rec_TLM(rec_TLM_analysis) %NAV inertial attitude
% plot_GNC_control_cmd_rec_TLM(rec_TLM_analysis) %GNC control CMD
% plot_NAV_rec_TLM(rec_TLM_analysis) %NAV
% plot_Landing_rec_TLM(rec_TLM_analysis) %Landing
% plot_DV_req_TLM(rec_TLM_analysis) %DV
% plot_SG_rec_TLM(rec_TLM_analysis) %SG11 & SG22

%% sensors plots
% plot_IMU1_rec_TLM(rec_TLM_analysis) %IMU 1
% plot_IMU2_rec_TLM(rec_TLM_analysis) %IMU 2
% plot_STR_rec_TLM(rec_TLM_analysis) %STR
% plot_OMPS_rec_TLM(rec_TLM_analysis) %OMPS
