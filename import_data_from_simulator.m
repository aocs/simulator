addpath(genpath('C:\Users\orell\Desktop\v1.2\AOCS\CodeGeneration\Simulator_log_files\import data from simulator')) %data's directory
rmpath('C:\Users\orell\Desktop\v1.2\AOCS\CodeGeneration\Simulator_log_files\cruise')
rmpath('C:\Users\orell\Desktop\v1.2\AOCS\CodeGeneration\Simulator_log_files\Landing')
rmpath('C:\Users\orell\Desktop\v1.2\AOCS\CodeGeneration\Simulator_log_files\DV')

filename1 = 'aocs_inputs.csv';
aocsinputs = readtable(filename1);
[m,~] = size(aocsinputs);

filename2 = 'aocs_spacecraft_data.csv';
aocsspacecraft = readtable(filename2);
time_inputs = zeros(m,1);
%% create input for model
for i=1:m
    time_inputs(i) = 0.02*i;
end
aocsinputs.SimTime = time_inputs;
AOCS_MODE_CMD_input =[time_inputs aocsinputs.MC_INPUT_AOCS_MODE_CMD];
sensors_availability_input = [time_inputs aocsinputs.MC_INPUT_sensors_availability_0_ ...
    aocsinputs.MC_INPUT_sensors_availability_1_ aocsinputs.MC_INPUT_sensors_availability_2_ ...
    aocsinputs.MC_INPUT_sensors_availability_3_ aocsinputs.MC_INPUT_sensors_availability_4_...
    aocsinputs.MC_INPUT_sensors_availability_5_ aocsinputs.MC_INPUT_sensors_availability_6_...
    aocsinputs.MC_INPUT_sensors_availability_7_ aocsinputs.MC_INPUT_sensors_availability_8_...
    aocsinputs.MC_INPUT_sensors_availability_9_ aocsinputs.MC_INPUT_sensors_availability_10_];
go_command_str1_str2_input = [time_inputs aocsinputs.MC_INPUT_go_command_str1_str2_0_ aocsinputs.MC_INPUT_go_command_str1_str2_1_];
Daylight_input = [time_inputs aocsinputs.MC_INPUT_DayLight];

q_I_2_STR_input = [time_inputs aocsinputs.STR_MEAS_q_I_2_STR_meas_0_ aocsinputs.STR_MEAS_q_I_2_STR_meas_1_ aocsinputs.STR_MEAS_q_I_2_STR_meas_2_ aocsinputs.STR_MEAS_q_I_2_STR_meas_3_];
wb_STR_input = [time_inputs aocsinputs.STR_MEAS_wb_STR_0_ aocsinputs.STR_MEAS_wb_STR_1_ aocsinputs.STR_MEAS_wb_STR_2_];
STR_source_input = [time_inputs aocsinputs.STR_MEAS_STR_source];
return_str_input = [time_inputs aocsinputs.STR_MEAS_return_str];
STR_epoch_time_input = [time_inputs aocsinputs.STR_MEAS_epoch_time];
STR_TimeTag_input = [time_inputs aocsinputs.STR_MEAS_TimeTag];


Wb_gyro_1_input = [time_inputs aocsinputs.IMU_MEAS_0__Wb_gyro_meas_0_ aocsinputs.IMU_MEAS_0__Wb_gyro_meas_1_ aocsinputs.IMU_MEAS_0__Wb_gyro_meas_2_];
f_accel_1_input = [time_inputs aocsinputs.IMU_MEAS_0__f_accel_meas_0_ aocsinputs.IMU_MEAS_0__f_accel_meas_1_ aocsinputs.IMU_MEAS_0__f_accel_meas_2_];
status_acc_1_input = [time_inputs aocsinputs.IMU_MEAS_0__status_acc];
status_gyro_1_input = [time_inputs aocsinputs.IMU_MEAS_0__status_gyro];
counter_1_input = [time_inputs aocsinputs.IMU_MEAS_0__counter];
latency_1_input = [time_inputs aocsinputs.IMU_MEAS_0__latency];
IMU_TimeTag_1_input = [time_inputs aocsinputs.IMU_MEAS_0__TimeTag];



Wb_gyro_2_input = [time_inputs aocsinputs.IMU_MEAS_1__Wb_gyro_meas_0_ aocsinputs.IMU_MEAS_1__Wb_gyro_meas_1_ aocsinputs.IMU_MEAS_1__Wb_gyro_meas_2_];
f_accel_2_input = [time_inputs aocsinputs.IMU_MEAS_1__f_accel_meas_0_ aocsinputs.IMU_MEAS_1__f_accel_meas_1_ aocsinputs.IMU_MEAS_1__f_accel_meas_2_];
status_acc_2_input = [time_inputs aocsinputs.IMU_MEAS_1__status_acc];
status_gyro_2_input = [time_inputs aocsinputs.IMU_MEAS_1__status_gyro];
counter_2_input = [time_inputs aocsinputs.IMU_MEAS_1__counter];
latency_2_input = [time_inputs aocsinputs.IMU_MEAS_1__latency];
IMU_TimeTag_2_input = [time_inputs aocsinputs.IMU_MEAS_1__TimeTag];




Sun_Direction_1_input = [time_inputs aocsinputs.SUN_SENSORS_MEAS_0__Sun_Direction_meas_0_ aocsinputs.SUN_SENSORS_MEAS_0__Sun_Direction_meas_1_];
css_validity_1_input = [time_inputs aocsinputs.SUN_SENSORS_MEAS_0__Validity];
css_TimeTag_1_input = [time_inputs aocsinputs.SUN_SENSORS_MEAS_0__TimeTag];

Sun_Direction_2_input = [time_inputs aocsinputs.SUN_SENSORS_MEAS_1__Sun_Direction_meas_0_ aocsinputs.SUN_SENSORS_MEAS_1__Sun_Direction_meas_1_];
css_validity_2_input = [time_inputs aocsinputs.SUN_SENSORS_MEAS_1__Validity];
css_TimeTag_2_input = [time_inputs aocsinputs.SUN_SENSORS_MEAS_1__TimeTag];

Sun_Direction_3_input = [time_inputs aocsinputs.SUN_SENSORS_MEAS_2__Sun_Direction_meas_0_ aocsinputs.SUN_SENSORS_MEAS_2__Sun_Direction_meas_1_];
css_validity_3_input = [time_inputs aocsinputs.SUN_SENSORS_MEAS_2__Validity];
css_TimeTag_3_input = [time_inputs aocsinputs.SUN_SENSORS_MEAS_2__TimeTag];

Sun_Direction_4_input = [time_inputs aocsinputs.SUN_SENSORS_MEAS_3__Sun_Direction_meas_0_ aocsinputs.SUN_SENSORS_MEAS_3__Sun_Direction_meas_1_];
css_validity_4_input = [time_inputs aocsinputs.SUN_SENSORS_MEAS_3__Validity];
css_TimeTag_4_input = [time_inputs aocsinputs.SUN_SENSORS_MEAS_3__TimeTag];

Sun_Direction_5_input = [time_inputs aocsinputs.SUN_SENSORS_MEAS_4__Sun_Direction_meas_0_ aocsinputs.SUN_SENSORS_MEAS_4__Sun_Direction_meas_1_];
css_validity_5_input = [time_inputs aocsinputs.SUN_SENSORS_MEAS_4__Validity];
css_TimeTag_5_input = [time_inputs aocsinputs.SUN_SENSORS_MEAS_4__TimeTag];

Sun_Direction_6_input = [time_inputs aocsinputs.SUN_SENSORS_MEAS_5__Sun_Direction_meas_0_ aocsinputs.SUN_SENSORS_MEAS_5__Sun_Direction_meas_1_];
css_validity_6_input = [time_inputs aocsinputs.SUN_SENSORS_MEAS_5__Validity];
css_TimeTag_6_input = [time_inputs aocsinputs.SUN_SENSORS_MEAS_5__TimeTag];

MC_safe_flag_input = [time_inputs aocsinputs.MC_INPUT_safe_flag];

