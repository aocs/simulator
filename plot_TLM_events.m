function [] = plot_TLM_events(mission_simulator_results)
r = groot;
sz = r.get('ScreenSize');
set(groot,'defaultAxesTickLabelInterpreter','none')

%% DV status
figure('OuterPosition',sz,'Name','AOCS TELEMETRY EVENTS')
telemetry_events_def_table = importLogicEvents('AOCS_Events.xlsx','Telemetry Events',3,53);

events_data = table2array([mission_simulator_results.LOGIC.events.telemetry_events_0,mission_simulator_results.LOGIC.events.telemetry_events_1]);
% time = mission_simulator_results.Time;
[m,~] = size(events_data);
time = linspace(mission_simulator_results.Time(1),mission_simulator_results.Time(end),m);
bit_on1=log2(double(events_data(:,1)))+1;
bit_on2=log2(double(events_data(:,2)))+1;

bit_on1(bit_on1<0)=0;
bit_on2(bit_on2<0)=0;
bit_on2(bit_on2>0) = bit_on2(bit_on2>0)+32;
bit_on = bit_on1 +bit_on2;
plot(time,bit_on,'LineWidth',2)
ind1 = find(bit_on1>0);
ind2 = find(bit_on2>0);
for i = 1: numel(ind1)
    text(time(ind1(i)),bit_on1(ind1(i))+0.3,telemetry_events_def_table.Event{bit_on1(ind1(i))},'FontSize',14,'FontWeight','bold','Interpreter','none')
end

for i = 1: numel(ind2)
    text(time(ind2(i)),bit_on2(ind2(i))+0.3,telemetry_events_def_table.Event{bit_on2(ind2(i))},'FontSize',14,'FontWeight','bold','Interpreter','none')
end
set(gca,'FontSize',14,'FontWeight','bold');%,'YTickMode','manual','YTick',0:numel(telemetry_events_def_table.Event),'TickLabelInterpreter','none');

grid on