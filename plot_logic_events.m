r = groot;
sz = r.get('ScreenSize');
set(groot,'defaultAxesTickLabelInterpreter','none')

%% DV status
figure('OuterPosition',sz,'Name','AOCS LOGIC EVENTS')


logic_events_def_table = importLogicEvents('AOCS_Events.xlsx','Logic Events',3,19);

events_data = logsout.get('AOCS_EVENTS').Values.AOCS_Logic_events.Data;
time = logsout.get('AOCS_EVENTS').Values.AOCS_Logic_events.Time;
bit_on=log2(double(events_data))+1;

bit_on(bit_on<0)=0;
plot(time,bit_on,'LineWidth',2)
ind = find(bit_on>0);
if numel(ind)<100
for i = 1: numel(ind)
    i1_f = i;i2_f=i;
    if mod(bit_on(ind(i)),1)>1e-6
        for i1 = 0:numel(logic_events_def_table.Event)-1
            for i2 = 0:numel(logic_events_def_table.Event)-1
                if abs(2^i1 + 2^i2 - events_data(ind(i)))<0.1
                    
                    i1_f = i1;i2_f=i2;
                end
            end
        end
    end
    if i1_f==i2_f
        text(time(ind(i)),bit_on(ind(i)),logic_events_def_table.Event{bit_on(ind(i))},'FontSize',14,'FontWeight','bold','Interpreter','none')
    else
        text(time(ind(i1_f)),bit_on(ind(i1_f)),logic_events_def_table.Event{bit_on(ind(i1_f))},'FontSize',14,'FontWeight','bold','Interpreter','none')
        text(time(ind(i2_f)),bit_on(ind(i2_f)),logic_events_def_table.Event{bit_on(ind(i2_f))},'FontSize',14,'FontWeight','bold','Interpreter','none')
        
    end
end
ylim([0 max(bit_on)+1])
set(gca,'FontSize',14,'FontWeight','bold')

grid on
end