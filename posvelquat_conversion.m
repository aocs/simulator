utc1=[2019 1 1 0 12 20];
utc2=[2019 1 1 0 14 20];
Re=1737.2*1000;
% [position_earth1,velocity_earth1] = planetEphemeris(juliandate(2019,1,1,0,12,20),'Moon','Earth');
% 
% [position_earth2,velocity_earth2] = planetEphemeris(juliandate(2019,1,1,0,14,20),'Moon','Earth');

lat1=0.6785*180/pi;
lon1=0.9046*180/pi;
alt_surf1=4118;
lat2=0.6713*180/pi;
lon2=0.9245*180/pi;
alt_surf2=1383;
qb2ned1=[0.2239;0.7678;-0.487;0.3508]';
qb2ned2=[0.2362;0.7563;-0.4771;0.3803]';
vned1=[-159;340.6;36.88];
vned2=[-49.64;103.1;18.27];

hdtm1 = F_dtm2(lat1,lon1);
hdtm2 = F_dtm2(lat2,lon2);

plla_ref1=[lat1,lon1,alt_surf1+hdtm1];
plla_ref2=[lat2,lon2,alt_surf2+hdtm2];
plla_lclf1=lla2ecef(plla_ref1, 0, Re)';
plla_lclf2=lla2ecef(plla_ref2, 0, Re)';



% dcmi2ecef1 = dcmeci2ecef('IAU-2000/2006',utc1);
% dcmi2ecef2 = dcmeci2ecef('IAU-2000/2006',utc2);
[init_Lib_angles1, q_i2lclf_1] = spiceLibrations(utc1);
[init_Lib_angles2, q_i2lclf_2] = spiceLibrations(utc2);
dcmi2lclf1=quat2dcm(q_i2lclf_1);
dcmi2lclf2=quat2dcm(q_i2lclf_2);

dcmlclf2ned1=dcmecef2ned(lat1, lon1);
dcmlclf2ned2=dcmecef2ned(lat2, lon2);

position1_mci = dcmi2lclf1'*plla_lclf1;%+position_earth1;
position2_mci = dcmi2lclf2'*plla_lclf1;%+position_earth2;

v1_mci=(dcmi2lclf1')*(dcmlclf2ned1'*vned1);%+velocity_earth1';
v2_mci=(dcmi2lclf2')*(dcmlclf2ned2'*vned2);%+velocity_earth2';

qb2i1=dcm2quat(dcmi2lclf1'*((dcmlclf2ned1')*quat2dcm(qb2ned1)));
qb2i2=dcm2quat(dcmi2lclf2'*((dcmlclf2ned2')*quat2dcm(qb2ned2)));