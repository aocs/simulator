Xinertial               =    2606558.0858;
Yinertial               =   -13282754.4454;
Zinertial               =   -5758778.8409;
XI0=[Xinertial;Yinertial;Zinertial];
Vx_inertial             =   1224.37;  % [m/sec]
Vy_inertial             =   0;  % [m/sec]
Vz_inertial             =   9971.73;  % [m/sec]

q_i2b_0 = [1;0;0;0];
w_i2b_b_0 = [0;0;1*pi/180];
VI0=[Vx_inertial;Vy_inertial;Vz_inertial];
init_mission_time=[2018,1,1,0,0,0];
final_mission_time=[2018,1,2,0,0,0];

JD_init=juliandate(init_mission_time);
JD_final=juliandate(final_mission_time);

eph_model='421';

init_Lib_angles= moonLibration(juliandate(init_mission_time),eph_model);
final_Lib_angles= moonLibration(juliandate(final_mission_time),eph_model);
position_Moon = planetEphemeris(JD_init,'Earth','Moon',eph_model)*1000; % m
position_Sun = planetEphemeris(JD_init,'Earth','Sun',eph_model)*1000; % m
sun_uvec_i = position_Sun/norm(position_Sun);

SIM_MODE=SIM_MODES.DS; % SIM_MODES - Deep Space
sim_duration=5; % typical duration of a deltav simulation

Surface_start_time= single(10e6);
Cruise_start_time = single(3); % [sec] (simulation time)
Calib_start_time=single(10e6);
Hop_start_time = single(10e6); % [sec] (simulation time)
SP_start_time = single(10e6); %single(7); % [sec]
DV_start_time = single(10); % [sec]
Landing_start_time = single(10e6); %10;

